/* global UndoFlag_NoSwapFile, Dialog, StdButton_No, StdIcon_Question, StdButton_Cancel, StdButton_Yes */

// Version 1.0 (c) John Murphy 12th-Aug-2020
//
// ======== #license ===============================================================
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =================================================================================
//"use strict";

/**
 * Create a dialog that displays a graph.
 * The Graph object returned from the supplied createZoomedGradientGraph(Number zoomFactor) 
 * function must include the methods:
 * Bitmap Graph.getGraphBitmap()
 * String Graph.screenToWorld(Number x, Number y)
 * The GraphDialog is initialised with the Graph returned from createZoomedGradientGraph, 
 * with a zoom factor of 1
 * @param {String} title Window title
 * @param {PhotometricMosaicData} data
 * @param {Boolean} isColor
 * @param {Graph function({Number} zoomFactor, {Number} width, {Number} height, {Number} channel)} createZoomedGradientGraph
 * Callback function used to create a zoomed graph
 * @param {PhotometricMosaicDialog} photometricMosaicDialog
 * @param {Boolean} isAdjustScaleDialog If true, specialise dialog for adjust scale
 * @returns {GradientGraphDialog}
 */
function GradientGraphDialog(title, data, isColor, createZoomedGradientGraph, 
        photometricMosaicDialog, isAdjustScaleDialog)
{
    this.__base__ = Dialog;
    this.__base__();
    let self = this;
    let zoom_ = 1;
    let selectedChannel_ = 3;
    let createZoomedGradientGraph_ = createZoomedGradientGraph;
    let graphHeight = data.smallScreen ? data.graphHeight - 300 : data.graphHeight;
    let width = this.logicalPixelsToPhysical(data.graphWidth);
    let height = this.logicalPixelsToPhysical(graphHeight);
    let graph_ = createZoomedGradientGraph_(zoom_, width, height, selectedChannel_);
    
    /**
     * Converts bitmap (x,y) into graph coordinates.
     * @param {Number} x Bitmap x coordinate
     * @param {Number} y Bitmap y coordinate
     * @returns {String} Output string in format "( x, y )"
     */
    function displayXY(x, y){
        self.windowTitle = title + getZoomString() + "  " + graph_.screenToWorld(x, y);
    };
    
    // Draw bitmap into this component
    let bitmapControl = new Control(this);
    
    bitmapControl.onPaint = function (){
        let g;
        try {
            g = new Graphics(this);
            g.clipRect = new Rect(0, 0, this.width, this.height);
            g.drawBitmap(0, 0, graph_.getGraphBitmap());
        } catch (e) {
            console.criticalln("GradientGraphDialog bitmapControl.onPaint() error: " + e);
        } finally {
            g.end();
        }
    };
    
    bitmapControl.onMousePress = function ( x, y, button, buttonState, modifiers ){
        // Display graph coordinates in title bar
        displayXY(x, y);
    };
    
    bitmapControl.onMouseMove = function ( x, y, buttonState, modifiers ){
        // When dragging mouse, display graph coordinates in title bar
        displayXY(x, y);
        // TODO create pan mode using space bar (modifiers = 8)
    };
    
    bitmapControl.onMouseWheel = function ( x, y, delta, buttonState, modifiers ){
        if (delta < 0){
            updateZoom( zoom_ + 1);
        } else {
            updateZoom( zoom_ - 1);
        }
    };
    
    bitmapControl.onResize = function (wNew, hNew, wOld, hOld) {
        update(wNew, hNew);
    };
    
    /**
     * @param {Number} zoom
     */
    function updateZoom (zoom) {
        if (zoom < 101 && zoom > -99){
            zoom_ = zoom;
            update(bitmapControl.width, bitmapControl.height);
            self.windowTitle = title + getZoomString();   // display zoom factor in title bar
        }
    }
    
    /**
     * @param {Number} width Graph bitmap width (
     * @param {Number} height Graph bitmap height
     */
    function update(width, height){
        try {
            graph_ = createZoomedGradientGraph_(getZoomFactor(), width, height, selectedChannel_);
            bitmapControl.repaint();    // display the zoomed graph bitmap
        } catch (e) {
            console.criticalln("Graph update error: " + e);
        }
    }
    
    /**
     * If zoom_ is positive, return zoom_ (1 to 100)
     * If zoom_ is zero or negative, then:
     * 0 -> 1/2
     * -1 -> 1/3
     * -2 -> 1/4
     * -98 -> 1/100
     * @returns {Number} Zoom factor
     */
    function getZoomFactor(){
        return zoom_ > 0 ? zoom_ : 1 / (2 - zoom_);
    }
    
    /**
     * @returns {String} Zoom string (e.g. " 1:2")
     */
    function getZoomString(){
        let zoomFactor = getZoomFactor();
        if (zoomFactor < 1){
            return " 1:" + Math.round(1/zoomFactor);
        } else {
            return " " + zoomFactor + ":1";
        }
    }
    
    bitmapControl.toolTip = 
            "Mouse wheel: Zoom" +
            "\nLeft click: Display (x,y) in title bar";
    
    /**
     * When a slider is dragged, only fast draw operations are performed.
     * When the drag has finished (or after the user has finished editing in the textbox)
     * this method is called to perform all calculations.
     */
    function finalUpdateFunction(){
        self.enabled = false;
        processEvents();
        update(bitmapControl.width, bitmapControl.height);
        self.enabled = true;
    }
    
    let smoothnessControl;
    if (!isAdjustScaleDialog){
        // Gradient controls
        let gradientControls = new GradientControls();
        if (data.viewFlag === DISPLAY_TARGET_GRADIENT_GRAPH()){
            smoothnessControl = gradientControls.createTargetGradientSmoothnessControl(this, data, 0);
            smoothnessControl.onValueUpdated = function (value) {
                data.targetGradientSmoothness = value;
                photometricMosaicDialog.targetGradientSmoothness_Control.setValue(value);
            };
        } else {
            smoothnessControl = gradientControls.createOverlapGradientSmoothnessControl(this, data, 0);
            smoothnessControl.onValueUpdated = function (value) {
                data.overlapGradientSmoothness = value;
                photometricMosaicDialog.overlapGradientSmoothness_Control.setValue(value);
            }; 
        }
        smoothnessControl.slider.minWidth = 280;
        addFinalUpdateListener(smoothnessControl, finalUpdateFunction);
    }
    
    // ===========================
    // Color toggles
    // ===========================
    let redRadioButton = new RadioButton(this);
    redRadioButton.text = "Red";
    redRadioButton.toolTip = "<p>Display the red channel gradient</p>" + 
            "<p>This is only used to declutter the display. " +
            "The 'Smoothness' setting will be applied to all color channels.</p>";
    redRadioButton.checked = false;
    redRadioButton.onClick = function (checked) {
        selectedChannel_ = 0;
        enableControls();
        self.enabled = false;
        processEvents();
        update(bitmapControl.width, bitmapControl.height);
        self.enabled = true;
    };
    
    let greenRadioButton = new RadioButton(this);
    greenRadioButton.text = "Green";
    greenRadioButton.toolTip = "<p>Display the green channel gradient</p>" + 
            "<p>This is only used to declutter the display. " +
            "The 'Smoothness' setting will be applied to all color channels.</p>";
    greenRadioButton.checked = false;
    greenRadioButton.onClick = function (checked) {
        selectedChannel_ = 1;
        enableControls();
        self.enabled = false;
        processEvents();
        update(bitmapControl.width, bitmapControl.height);
        self.enabled = true;
    };
    
    let blueRadioButton = new RadioButton(this);
    blueRadioButton.text = "Blue";
    blueRadioButton.toolTip = "<p>Display the blue channel gradient</p>" + 
            "<p>This is only used to declutter the display. " +
            "The 'Smoothness' setting will be applied to all color channels.</p>";
    blueRadioButton.checked = false;
    blueRadioButton.onClick = function (checked) {
        selectedChannel_ = 2;
        enableControls();
        self.enabled = false;
        processEvents();
        update(bitmapControl.width, bitmapControl.height);
        self.enabled = true;
    };
    
    let allRadioButton = new RadioButton(this);
    allRadioButton.text = "All";
    allRadioButton.toolTip = "Display the gradient for all channels";
    allRadioButton.checked = true;
    allRadioButton.onClick = function (checked) {
        selectedChannel_ = 3;
        enableControls();
        self.enabled = false;
        processEvents();
        update(bitmapControl.width, bitmapControl.height);
        self.enabled = true;
    };
    
    if (!isColor){
        redRadioButton.enabled = false;
        greenRadioButton.enabled = false;
        blueRadioButton.enabled = false;
    }

    let optionsSizer = new HorizontalSizer(this);
    optionsSizer.margin = 0;
    optionsSizer.spacing = 10;
    optionsSizer.addSpacing(4);
    if (!isAdjustScaleDialog){
        optionsSizer.add(smoothnessControl);
        optionsSizer.addSpacing(20);
    }
    optionsSizer.add(redRadioButton);
    optionsSizer.add(greenRadioButton);
    optionsSizer.add(blueRadioButton);
    optionsSizer.add(allRadioButton);
    optionsSizer.addStretch();
    
    // ===========================
    // Adjust scale controls
    // ===========================
    let controlsHeight = 0;
    let minHeight = bitmapControl.minHeight;
    const GREEN_STRLEN = this.font.width("Green: ");
    
    this.onToggleSection = function(bar, beginToggle){
        if (beginToggle){
            if (bar.isExpanded()){
                bitmapControl.setMinHeight(bitmapControl.height + bar.section.height + 2);
            } else {
                bitmapControl.setMinHeight(bitmapControl.height - bar.section.height - 2);
            }
            this.adjustToContents();
        }  else {
            bitmapControl.setMinHeight(minHeight);
            let maxDialogHeight = self.logicalPixelsToPhysical(1150);
            if (self.height > maxDialogHeight)
                self.resize(self.width, maxDialogHeight);
        }
    };
    
    let adjustRedScale_Control;
    let adjustGreenScale_Control;
    let adjustBlueScale_Control;
    
    function createScaleSection(dialog){
        let adjustScaleControls = new AdjustScaleControls();
        adjustRedScale_Control = adjustScaleControls.createAdjustRedControl(
                dialog, data, GREEN_STRLEN);
        adjustRedScale_Control.onValueUpdated = function (value) {
            data.adjustScale[0] = value;
            photometricMosaicDialog.adjustRedScale_Control.setValue(value);
            update(bitmapControl.width, bitmapControl.height);
        };
        addFinalUpdateListener(adjustRedScale_Control, finalUpdateFunction);
        controlsHeight += adjustRedScale_Control.height;

        adjustGreenScale_Control = adjustScaleControls.createAdjustGreenControl(
                dialog, data, GREEN_STRLEN);
        adjustGreenScale_Control.enabled = isColor;
        adjustGreenScale_Control.onValueUpdated = function (value) {
            data.adjustScale[1] = value;
            photometricMosaicDialog.adjustGreenScale_Control.setValue(value);
            update(bitmapControl.width, bitmapControl.height);
        };
        addFinalUpdateListener(adjustGreenScale_Control, finalUpdateFunction);
        controlsHeight += adjustGreenScale_Control.height;

        adjustBlueScale_Control = adjustScaleControls.createAdjustBlueControl(
                dialog, data, GREEN_STRLEN);
        adjustBlueScale_Control.enabled = isColor;
        adjustBlueScale_Control.onValueUpdated = function (value) {
            data.adjustScale[2] = value;
            photometricMosaicDialog.adjustBlueScale_Control.setValue(value);
            update(bitmapControl.width, bitmapControl.height);
        };
        addFinalUpdateListener(adjustBlueScale_Control, finalUpdateFunction);
        controlsHeight += adjustBlueScale_Control.height;
        
        let scaleSection = new Control(dialog);
        scaleSection.sizer = new VerticalSizer;
        scaleSection.sizer.spacing = 2;
        scaleSection.sizer.add(adjustRedScale_Control);
        scaleSection.sizer.add(adjustGreenScale_Control);
        scaleSection.sizer.add(adjustBlueScale_Control);
        scaleSection.sizer.addSpacing(5);
        return scaleSection;
    }
    
    function createScaleBar(dialog, scaleSection){
        let scaleBar = new SectionBar(dialog, "Adjust Scale");
        scaleBar.setSection(scaleSection);
        scaleBar.onToggleSection = dialog.onToggleSection;
        scaleBar.toolTip = "The specified adjustments are added to the scale factors.";
        controlsHeight += scaleBar.height + scaleSection.sizer.spacing * 2 + 5;
        return scaleBar;
    }
    
    function enableControls(){
        if (isColor && isAdjustScaleDialog){
            adjustRedScale_Control.enabled = (selectedChannel_ === 0 || selectedChannel_ === 3);
            adjustGreenScale_Control.enabled = (selectedChannel_ === 1 || selectedChannel_ === 3);;
            adjustBlueScale_Control.enabled = (selectedChannel_ === 2 || selectedChannel_ === 3);;
        }
    }
    
    // ===========================
    // Zoom controls and OK button
    // ===========================
    let zoomIn_Button = new ToolButton(this);
    zoomIn_Button.icon = this.scaledResource(":/icons/zoom-in.png");
    zoomIn_Button.setScaledFixedSize(24, 24);
    zoomIn_Button.toolTip = "Zoom In";
    zoomIn_Button.onMousePress = function (){
        updateZoom( zoom_ + 1);
    };

    let zoomOut_Button = new ToolButton(this);
    zoomOut_Button.icon = this.scaledResource(":/icons/zoom-out.png");
    zoomOut_Button.setScaledFixedSize(24, 24);
    zoomOut_Button.toolTip = "Zoom Out";
    zoomOut_Button.onMousePress = function (){
        updateZoom( zoom_ - 1);
    };

    let zoom11_Button = new ToolButton(this);
    zoom11_Button.icon = this.scaledResource(":/icons/zoom-1-1.png");
    zoom11_Button.setScaledFixedSize(24, 24);
    zoom11_Button.toolTip = "Zoom 1:1";
    zoom11_Button.onMousePress = function (){
        updateZoom( 1 );
    };
    
    let ok_Button = new PushButton(this);
    ok_Button.text = "OK";
    ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
    ok_Button.onClick = function(){
        self.ok();
    };

    let zoomButton_Sizer = new HorizontalSizer(this);
    zoomButton_Sizer.margin = 0;
    zoomButton_Sizer.spacing = 4;
    zoomButton_Sizer.add(zoomIn_Button);
    zoomButton_Sizer.add(zoomOut_Button);
    zoomButton_Sizer.add(zoom11_Button);
    zoomButton_Sizer.addStretch();
    zoomButton_Sizer.add(ok_Button);
    zoomButton_Sizer.addSpacing(10);
    
    //-------------
    // Global sizer
    //-------------
    this.sizer = new VerticalSizer(this);
    this.sizer.margin = 2;
    this.sizer.spacing = 2;
    this.sizer.add(bitmapControl, 100);
    this.sizer.add(optionsSizer);
    if (isAdjustScaleDialog){
        let scaleSection = createScaleSection(this);
        let scaleBar = createScaleBar(this, scaleSection);
        this.sizer.add(scaleBar);
        this.sizer.add(scaleSection);
    }
    this.sizer.add(zoomButton_Sizer);
    enableControls();
    
    this.userResizable = true;
    let preferredWidth = width + this.sizer.margin * 2;
    let preferredHeight = height + this.sizer.margin * 2 + this.sizer.spacing * 2 + 
           zoomIn_Button.height * 2 + 4;
    this.resize(preferredWidth, preferredHeight);
    
    this.setScaledMinSize(300, 300);
    this.windowTitle = title + " 1:1";
}

GradientGraphDialog.prototype = new Dialog;