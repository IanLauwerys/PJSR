/* global Dialog, StdCursor_ClosedHand, MouseButton_Left, StdCursor_UpArrow, StdCursor_Checkmark */

// Version 1.0 (c) John Murphy 30th-July-2020
//
// ======== #license ===============================================================
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =================================================================================
//"use strict";

/**
 * Display the Join Size and Position in a Dialog that contains a scrolled window and 
 * controls to adjust the join size and position parameters.
 * @param {String} title Window title
 * @param {Bitmap} refBitmap Background image of the reference overlap area at 1:1 scale
 * @param {Bitmap} tgtBitmap Background image of the target overlap area at 1:1 scale
 * @param {PhotometricMosaicData} data Values from user interface
 * @param {Point[]} joinPath The path of the reference image - target image join
 * @param {Point[]} targetSide The target side envelope of the overlapping pixels
 * @param {PhotometricMosaicDialog} photometricMosaicDialog
 * @returns {JoinDialog}
 */
function JoinDialog(title, refBitmap, tgtBitmap, data, joinPath, targetSide, photometricMosaicDialog)
{
    this.__base__ = Dialog;
    this.__base__();
    
    const REF = 10;
    const TGT = 20;
    let self = this;
    let zoomText = "1:1";
    let coordText;
    setCoordText(null);
    let selectedBitmap = REF;
    let bitmap = getBitmap(selectedBitmap);
    let bitmapOffset = getBitmapOffset(data);
    
    /**
     * Return bitmap of the reference or target image
     * @param {Number} refOrTgt Set to REF or TGT
     * @returns {Bitmap}
     */
    function getBitmap(refOrTgt){
        return refOrTgt === REF ? refBitmap : tgtBitmap;
    }
    
    /**
     * The offset between the full mosaic image and the bounding box of the overlap area.
     * Note that bitmap is of the overlap area.
     * @param {PhotometricMosaicData} data
     * @returns {Point} bitmap offset
     */
    function getBitmapOffset(data){
        let overlapBox = data.cache.overlap.overlapBox;
        return new Point(overlapBox.x0, overlapBox.y0);
    }
    
    /**
     * Set dialog title, including the current zoom and cursor coordinates
     */
    function setTitle(){
        self.windowTitle = title + " " + zoomText + " " + coordText;
    };
    
    /**
     * Set coordText, the cursor coordinate text. The coordText
     * is relative to the full mosaic image's top left corner.
     * @param {Point} point cursor coordinates relative to the (1:1) bitmap
     */
    function setCoordText(point){
        if (point === null){
            coordText = "(---,---)";
        } else {
            let x = bitmapOffset.x + point.x;
            let y = bitmapOffset.y + point.y;
            coordText = format("(%8.2f,%8.2f )", x, y);
        }
    }
    
    /**
     * Draw on top of the background bitmap, within the scrolled window
     * @param {Control} viewport
     * @param {Number} translateX
     * @param {Number} translateY
     * @param {Number} scale
     * @param {Number} x0
     * @param {Number} y0
     * @param {Number} x1
     * @param {Number} y1
     */
    function drawJoin(viewport, translateX, translateY, scale, x0, y0, x1, y1){
        let graphics;
        try {
            graphics = new VectorGraphics(viewport);
            graphics.clipRect = new Rect(x0, y0, x1, y1);
            graphics.translateTransformation(translateX, translateY);
            graphics.scaleTransformation(scale, scale);
            graphics.antialiasing = false;

            if (!data.useCropTargetToReplaceRegion){
                graphics.pen = new Pen(0xff0000ff, 2.0);
                for (let i=1; i < targetSide.length; i++){
                    let x = targetSide[i-1].x - bitmapOffset.x;
                    let x2 = targetSide[i].x - bitmapOffset.x;
                    let y = targetSide[i-1].y - bitmapOffset.y;
                    let y2 = targetSide[i].y - bitmapOffset.y;
                    graphics.drawLine(x, y, x2, y2);
                }
            }

            graphics.pen = new Pen(0xff00ff00, 2.0);
            if (data.useMosaicOverlay && !data.useCropTargetToReplaceRegion){
                // Overlay mosaic mode. Draw join path
                for (let i=1; i < joinPath.length; i++){
                    let x = joinPath[i-1].x - bitmapOffset.x;
                    let x2 = joinPath[i].x - bitmapOffset.x;
                    let y = joinPath[i-1].y - bitmapOffset.y;
                    let y2 = joinPath[i].y - bitmapOffset.y;
                    graphics.drawLine(x, y, x2, y2);
                }
            } else if (!data.correctTargetFlag){
                // Random or Average mosaic mode, or replace region. Draw Join Region rectangle
                let joinRegion = new JoinRegion(data);
                let joinRect = new Rect(joinRegion.joinRect);
                joinRect.translateBy(-bitmapOffset.x, -bitmapOffset.y);
                graphics.drawRect(joinRect);
            }

        } catch (e) {
            console.criticalln("JoinDialog error: " + e);
        } finally {
            graphics.end();
        }
    }
    
    // =================================
    // Sample Generation Preview frame
    // =================================
    let previewWidth = 1800;
    let previewHeight = 955;
    if (data.smallScreen){
        previewHeight -= 300;
    }
    let previewControl = new PreviewControl(this, bitmap, previewWidth, previewHeight, null, null, false);
    previewControl.updateZoomText = function (text){
        zoomText = text;
        setTitle();
    };
    previewControl.updateCoord = function (point){
        setCoordText(point);
        setTitle();
    };
    previewControl.onCustomPaintScope = this;
    previewControl.onCustomPaint = function (viewport, translateX, translateY, scale, x0, y0, x1, y1){
        drawJoin(viewport, translateX, translateY, scale, x0, y0, x1, y1);
    };
    previewControl.ok_Button.onClick = function(){
        self.ok();
    };
    if (data.useCropTargetToReplaceRegion) {
        previewControl.toolTip = 
            "<p>The background displays the overlap region. " +
            "The green rectangle shows the mosaic area that will be replaced by the target image.</p>";
    } else {
        previewControl.toolTip =
            "<p>The background displays the overlap region. " +
            "The green line (Overlay mode) shows the path of the mosaic join. " +
            "The green rectangle (Blend or Average mode) shows the Join Region. " +
            "The blue line indicates the target side of the overlap region.</p>" +
            "<p>The region between the blue line and the green line/rectangle " +
            "will be replaced by target pixels. The other side of the green line/rectangle will " +
            "be replaced with reference pixels.</p>";
    }
    previewControl.setMinHeight(200);
    
    // ========================================
    // User controls
    // ========================================
    let controlsHeight = 0;
    let minHeight = previewControl.minHeight;
    this.onToggleSection = function(bar, beginToggle){
        if (beginToggle){
            if (bar.isExpanded()){
                previewControl.setMinHeight(previewControl.height + bar.section.height + 2);
            } else {
                previewControl.setMinHeight(previewControl.height - bar.section.height - 2);
            }
        } else {
            previewControl.setMinHeight(minHeight);
        }
    };
    
    let refCheckBox = new CheckBox(this);
    refCheckBox.text = "Reference";
    refCheckBox.toolTip = "Display either the reference or target background.";
    refCheckBox.checked = true;
    refCheckBox.onClick = function (checked) {
        selectedBitmap = checked ? REF : TGT;
        bitmap = getBitmap(selectedBitmap);
        previewControl.updateBitmap(bitmap);
        previewControl.forceRedraw();
    };
    
    let joinSize_Control;
    let joinPosition_Control;
    let sampleControls = new SampleControls();

    // ===================================================
    // SectionBar: Join Position
    // ===================================================
    /**
     * Force the joinPosition to update after the user edits the textbox directly.
     */
    function finalJoinSizeUpdateFunction(){
        self.enabled = false;
        processEvents();
        previewControl.forceRedraw();
        self.enabled = true;
        // If the join size has increased, the join position range must be reduced.
        // If data.joinPosition is out of range, it must be updated.
        setJoinPositionRange(joinPosition_Control, data, true);
        setJoinPositionRange(photometricMosaicDialog.joinPosition_Control, data, false);
        // Update the main dialog's join and position values
        // This had to be done after updating the ranges and data.joinPosition
        photometricMosaicDialog.joinSize_Control.setValue(data.joinSize);
        photometricMosaicDialog.joinPosition_Control.setValue(data.joinPosition);
    }
    /**
     * Force the joinPosition to update after the user edits the textbox directly.
     */
    function finalJoinPositionUpdateFunction(){
        self.enabled = false;
        processEvents();
        previewControl.forceRedraw();
        self.enabled = true;
        // Update the main dialog's position value
        photometricMosaicDialog.joinPosition_Control.setValue(data.joinPosition);
    }
    
    joinSize_Control = sampleControls.createJoinSizeControl(this, data, 0);
    joinSize_Control.onValueUpdated = function (value) {
        data.joinSize = value;
        let joinRegion = new JoinRegion(data);
        // Update data.joinPosition. Necessary if the joinSize has increased
        joinRegion.updateJoinPosition();
        // Update the join position control.
        // The main dialog's control will be update at the end of the drag
        joinPosition_Control.setValue(data.joinPosition);
        // Draw the rectangle
        previewControl.forceRedraw();
    };
    addFinalUpdateListener(joinSize_Control, finalJoinSizeUpdateFunction);
    joinSize_Control.enabled = 
            !data.useMosaicOverlay && !data.correctTargetFlag &&
            !data.useCropTargetToReplaceRegion;
    
    joinPosition_Control = sampleControls.createJoinPositionControl(this, data, 0);
    joinPosition_Control.onValueUpdated = function (value) {
        data.joinPosition = value;
        let joinRegion = new JoinRegion(data);
        let joinRect = joinRegion.joinRect;
        let isHorizontal = joinRegion.isJoinHorizontal();
        joinPath = createMidJoinPathLimittedByOverlap(data.targetView.image,
                data.cache.overlap, joinRect, isHorizontal, data);
        previewControl.forceRedraw();
    };
    addFinalUpdateListener(joinPosition_Control, finalJoinPositionUpdateFunction);
    joinPosition_Control.enabled = !data.correctTargetFlag && !data.useCropTargetToReplaceRegion;
    
    let joinPositionSection = new Control(this);
    joinPositionSection.sizer = new VerticalSizer;
    joinPositionSection.sizer.add(joinSize_Control);
    joinPositionSection.sizer.add(joinPosition_Control);
    let joinPositionBar = new SectionBar(this, "Join");
    joinPositionBar.setSection(joinPositionSection);
    joinPositionBar.onToggleSection = this.onToggleSection;
    controlsHeight += joinPositionBar.height + 5;
    if (!joinPosition_Control.enabled){
        joinPositionSection.hide();
    } else {
        controlsHeight += joinSize_Control.height;
        controlsHeight += joinPosition_Control.height;
    }
    
    let optionsSizer = new HorizontalSizer(this);
    optionsSizer.margin = 0;
    optionsSizer.addSpacing(4);
    optionsSizer.add(refCheckBox);
    optionsSizer.addStretch();
    
    controlsHeight += refCheckBox.height;

    // Global sizer
    this.sizer = new VerticalSizer(this);
    this.sizer.margin = 2;
    this.sizer.spacing = 2;
    this.sizer.add(previewControl);
    this.sizer.add(optionsSizer);
    this.sizer.add(joinPositionBar);
    this.sizer.add(joinPositionSection);
    this.sizer.add(previewControl.getButtonSizer());

    controlsHeight += this.sizer.spacing * 3 + this.sizer.margin * 2;

    // The PreviewControl size is determined by the size of the bitmap
    // The dialog must also leave enough room for the extra controls we are adding
    this.userResizable = true;
    let preferredWidth = previewControl.width + this.sizer.margin * 2 + this.logicalPixelsToPhysical(20);
    let preferredHeight = previewControl.height + previewControl.getButtonSizerHeight() +
            controlsHeight + this.logicalPixelsToPhysical(20);
    this.resize(preferredWidth, preferredHeight);
    setTitle();
}

JoinDialog.prototype = new Dialog;
