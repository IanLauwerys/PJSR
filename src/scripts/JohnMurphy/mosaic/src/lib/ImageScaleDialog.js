/* global Dialog, DataType_Float, Settings, KEYPREFIX, DataType_Int32, FrameStyle_Sunken */

// Version 1.0 (c) John Murphy 31st-Dec-2020
//
// ======== #license ===============================================================
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =================================================================================
//"use strict";

/**
 * @param {View} view Check FIT headers in this view
 * @param {String} KEYPREFIX Save 'Settings' /pixelSize and /focalLength using this prefix
 * @returns {ImageScaleDialog}
 */
function ImageScaleDialog( view, KEYPREFIX ){
    this.__base__ = Dialog;
    this.__base__();

    let titleLabel = new Label();
    titleLabel.frameStyle = FrameStyle_Sunken;
    titleLabel.margin = 4;
    titleLabel.wordWrapping = false;
    titleLabel.useRichText = true;
    titleLabel.text = "<p>PhotometricMosaic requires the following FITS headers:<br />" +
        "<b>XPIXSZ</b> (binned pixel size in microns)<br />" +
        "<b>FOCALLEN</b> (focal length in mm)</p>";

    let pixelSize = getPixelSize(view, 0);
    let unknownPixelSize = false;
    if (pixelSize === 0){
        unknownPixelSize = true;
        let keyValue = Settings.read( KEYPREFIX+"/pixelSize", DataType_Float );
        if ( Settings.lastReadOK ){
            pixelSize = keyValue;
        } else {
            pixelSize = 6.0;
        }
    }
    
    let focalLength = getFocalLength(view, 0);
    let unknownFocalLength = false;
    if (focalLength === 0){
        unknownFocalLength = true;
        let keyValue = Settings.read( KEYPREFIX+"/focalLength", DataType_Int32 );
        if ( Settings.lastReadOK ){
            focalLength = keyValue;
        } else {
            focalLength = 1000;
        }
    }
    
    let ok_Button = new PushButton(this);
    ok_Button.defaultButton = true;
    ok_Button.text = "OK";
    ok_Button.icon = this.scaledResource(":/icons/ok.png");
    ok_Button.onClick = function () {
        this.dialog.ok();
    };
    
    let ignore_Button = new PushButton(this);
    ignore_Button.defaultButton = false;
    ignore_Button.text = "Ignore";
    ignore_Button.toolTip = "Leave FITS headers unchanged";
    ignore_Button.icon = this.scaledResource(":/icons/cancel.png");
    ignore_Button.onClick = function () {
        this.dialog.cancel();
    };

    let buttons_Sizer = new HorizontalSizer;
    buttons_Sizer.spacing = 6;
    buttons_Sizer.addStretch();
    buttons_Sizer.add(ok_Button);
    buttons_Sizer.add(ignore_Button);

    let pixelSizeEdit = new NumericEdit();
    pixelSizeEdit.label.text = "Pixel size including binning (microns)";
    pixelSizeEdit.setReal(true);
    pixelSizeEdit.setPrecision(2);
    pixelSizeEdit.setRange(0.1, 100);
    pixelSizeEdit.setValue(pixelSize);
    pixelSizeEdit.enabled = unknownPixelSize;
    pixelSizeEdit.toolTip = "<p>Pixel size including binning in microns</p>" +
            "<p>For example, if the image has been resized to half size, or " +
            "uses 2x binning, the pixel size is 2x the sensor pixel size.</p>";
    pixelSizeEdit.onValueUpdated = function (value){
        pixelSize = value;
        Settings.write( KEYPREFIX+"/pixelSize", DataType_Float, pixelSize );
    };
    
    let focalLengthEdit = new NumericEdit();
    focalLengthEdit.label.text = "Focal Length (mm)";
    focalLengthEdit.setReal(false);
    focalLengthEdit.setRange(1, 10000);
    focalLengthEdit.setValue(focalLength);
    focalLengthEdit.enabled = unknownFocalLength;
    focalLengthEdit.toolTip = "Focal length in millimeters";
    focalLengthEdit.onValueUpdated = function (value){
        focalLength = value;
        Settings.write( KEYPREFIX+"/focalLength", DataType_Int32, focalLength );      
    };

    // Global sizer
    this.sizer = new VerticalSizer();
    this.sizer.margin = 10;
    this.sizer.spacing = 10;
    
    this.sizer.add(titleLabel);
    this.sizer.add(pixelSizeEdit);
    this.sizer.add(focalLengthEdit);
    this.sizer.add(buttons_Sizer);

    this.windowTitle = "Enter FITS headers";
    this.adjustToContents();
    
    /**
     * @returns {Number} Pixel size in microns
     */
    this.getPixelSize = function(){
        return pixelSize;
    };
    
    /**
     * @returns {Number} Focal length in mm
     */
    this.getFocalLength = function(){
        return focalLength;
    };
}

ImageScaleDialog.prototype = new Dialog;
