// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-engine.js - Released 2021-04-11T09:14:46Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.1.0
//
// Copyright (c) 2019-2021 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2021 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include <pjsr/UndoFlag.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Compression.jsh>
#include <pjsr/StarDetector.jsh>
#include <pjsr/LinearDefectDetection.jsh>
#include <pjsr/LinearPatternSubtraction.jsh>
#include "WeightedBatchPreprocessing-processLogger.js"
/* beautify ignore:end */

// ----------------------------------------------------------------------------

/**
 * Keyword constructor: defines a keyword by name and mode mask.
 *
 * @param {String} name
 * @param {WBPPKeywordMode} mode
 */
function Keyword( name, mode )
{
   this.name = name;
   this.mode = mode;
}

// ----------------------------------------------------------------------------

/**
 * Manages the list of keywords and provides helpers for
 * adding/replacing/removing/sorting and searching keywords.
 * Duplicates and empty keywords are now allowed.
 */
function Keywords()
{
   this.list = [];

   /**
    * Safely add a new keyword and checks for duplicates and empty keyword.
    *
    * @param {String} name
    * @param {String?} mode keyword mode, pre-processing only as default
    * @returns undefined on success, error message in case of error
    */
   this.add = ( name, mode ) =>
   {
      // default mode is pre-processing only
      mode = mode || WBPPKeywordMode.PRE;
      if ( name.length == 0 )
         return "Unable to add an empty keyword."
      if ( this.contains( name ) )
         return "Keyword \"" + name + "\" is already in the list."
      this.list.push( new Keyword( name, mode ) );
      return undefined;
   };

   /**
    * Replace an exsistent keyword name.
    * If the new name to be assigned collides with an existing keywords then
    * the replacement does not occur and the function silently returns.
    *
    * @param {String} oldName
    * @param {String} newName
    */
   this.replace = ( oldName, newName ) =>
   {
      if ( this.contains( newName ) )
         return;
      this.list.forEach( kw =>
      {
         if ( kw.name == oldName )
            kw.name = newName
      } )
   };

   /**
    * Removes a keyword witht the given name.
    *
    * @param {String} name
    */
   this.remove = ( name ) =>
   {
      this.list = this.list.filter( k => k.name != name );
   };

   /**
    * Moves a keyword up or down in the list.
    *
    * @param {*} name
    * @param {*} up true to move keyword in the previous position, false to move it in the next
    * @return the new index of the keyword
    */
   this.move = ( name, up ) =>
   {
      let indx = this.list.reduce( ( obj, keyword, index ) =>
      {
         return keyword.name == name ? index : obj;
      }, -1 );
      if ( indx == -1 )
         return undefined;

      // do nothing if keyword is not found or if the keyword would move out of bounds
      if ( indx == -1 || ( indx == 0 && up ) || ( indx == this.list.length - 1 && !up ) )
         return undefined;
      let dstIndx = indx + ( up ? -1 : 1 );
      let tmp = this.list[ dstIndx ];
      this.list[ dstIndx ] = this.list[ indx ];
      this.list[ indx ] = tmp;
      return dstIndx;
   }

   /**
    * Checks if a keyword with the given name exists.
    *
    * @param {string} name
    * @returns Boolean
    */
   this.contains = ( name ) =>
   {
      return this.list.map( k => k.name ).indexOf( name ) > -1;
   };

   /**
    * Filters the given keywords object accordingly to the current keywords configuration.
    * Always returns an empty object if keywords are globally disabled.
    *
    * @param {{String:String}} keywords keywords to be filtered
    * @param {WBPPKeywordMode} mode filtering mode
    * @returns {String:Stting} filtered keywords
    */
   this.filterKeywordsForMode = ( keywords, mode ) =>
   {
      if ( !engine.groupingKeywordsEnabled || !keywords )
      {
         return {};
      }
      let namesForMode = this.list.reduce( ( obj, keyword ) =>
      {
         if ( keyword.mode & mode )
            obj.push( keyword.name );
         return obj;
      }, [] );
      // return the keywords enabled for the given mode
      return Object
         .keys( keywords )
         .filter( name => ( namesForMode.indexOf( name ) != -1 ) )
         .reduce( ( obj, name ) =>
         {
            obj[ name ] = keywords[ name ];
            return obj;
         },
         {} );
   }

   /**
    * Returns the kewyords object containing only the keywords enabled for the given mode.
    *
    * @param {WBPPGroupingMode} mode
    */
   this.keywordsForMode = ( mode ) =>
   {
      return this.list.filter( kw => kw.mode & mode );
   }

   /**
    * Switches the mode of the given keyword.
    *
    * @param {String} name
    */
   this.switchMode = ( name ) =>
   {
      let keyword = this.list.filter( kw => kw.name == name );
      if ( keyword.length == 0 )
         return;
      keyword[ 0 ].mode = ( keyword[ 0 ].mode + 1 ) % Object.keys( WBPPKeywordMode ).length;
   }

   /**
    * Returns a flat list of all keyword names
    */
   this.names = () =>
   {
      return this.list.map( k => k.name );
   }
}

Keywords.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * File item object constructor.
 *
 * @param {String} filePath the file path
 * @param {ImageType} imageType type of image
 * @param {String} filter filter name
 * @param {Number} binning image's binning
 * @param {Number} exposureTime image's exposure time
 * @param {Boolean} isCFA true if the file contains a CFA image
 * @param {Boolean} isMaster true if the image is a master file
 * @param {{String:String}?} keywords the keywords key-value map
 */
function FileItem( filePath, imageType, filter, binning, exposureTime, isCFA, isMaster, keywords )
{
   this.__base__ = Object;
   this.__base__();

   this.filePath = filePath;
   this.imageType = imageType;
   this.binning = binning;
   this.filter = filter;
   this.exposureTime = exposureTime;
   this.enabled = true;
   this.isCFA = isCFA;
   this.isMaster = isMaster;
   this.keywords = keywords ||
   {};

   /**
    * Initializes the refences to the processed versions of the file.
    */
   this.initForProcessing = () =>
   {
      this.processed = {};
      this.current = this.filePath;
      this.descriptor = undefined;
   }

   /**
    * Invoked when a processing step has been performed successfully.
    *
    * @param {WBPPFrameProcessingStep} step step executed
    * @param {String} filePath processed output file
    */
   this.processingSucceeded = ( step, filePath ) =>
   {
      this.processed[ step ] = filePath;
      this.current = filePath;
   }

   /**
    * Mark the file as failed to process. Indicates that an error occurred.
    *
    */
   this.processingFailed = () =>
   {
      this.current = undefined;
   }

   /**
    * Checks if the file processing is failed.
    *
    */
   this.failed = () =>
   {
      return this.current == undefined;
   }

   this.initForProcessing();
}

FileItem.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Frame group object constructor.
 *
 * @param {ImageType} imageType the type of images in this group
 * @param {String} filter the filter name associated to this group
 * @param {Number} binning the binning of images in this group
 * @param {Number} exposureTime the initial exposure time set for this group
 * @param {Boolean} isCFA true if this group contains CFA images
 * @param {FileItem} firstItem optional, initial first item to be added
 * @param {Boolean} hasMaster must set to true if the item provided is to be considered a master file
 * @param {{String:String}?} keywords the keywords key-value map
 * @param {WBPPGroupingMode} mode the group mode
 */
function FrameGroup( imageType, filter, binning, exposureTime, isCFA, firstItem, hasMaster, keywords, mode )
{
   this.__base__ = Object;
   this.__base__();

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // commons
   this.imageType = imageType;
   this.binning = binning;
   this.hasMaster = ( imageType != ImageType.LIGHT ) && hasMaster;

   // reference exposure time for the group
   this.exposureTime = ( imageType == ImageType.BIAS ) ? 0 : exposureTime;
   // filter name of the group, used by flat and light frame groups
   this.filter = ( imageType == ImageType.BIAS || imageType == ImageType.DARK ) ? "" : filter;
   // list of all exposure times of the frames in the group
   this.exposureTimes = [ this.exposureTime ];
   // used by Dark groups
   this.containsBias = DEFAULT_DARK_INCLUDE_BIAS;
   // used by flat and light frames
   this.optimizeMasterDark = DEFAULT_OPTIMIZE_DARKS;
   // group CFA
   this.isCFA = isCFA;
   // used by light frames to define the debayer Pattern and Method
   this.CFAPattern = DEFAULT_CFA_PATTERN;
   this.debayerMethod = DEFAULT_DEBAYER_METHOD;
   // used by LIGHT frames when a Master Flat is involved during calibration
   this.separateCFAFlatScalingFactors = isCFA;
   // group keywords, use only the keywords enabled for the given mode
   this.keywords = engine.keywords.filterKeywordsForMode( keywords, mode );
   // the group mode
   this.mode = mode;
   // list of frames
   this.fileItems = new Array;
   // output pedestal used for light frames
   this.lightOutputPedestal = DEFAULT_LIGHT_OUTPUT_PEDESTAL;

   // internally stores the current header descriptor length, used to pretty print the
   // group header in diagnostic messages
   this.footerLengthForCurrentHeader = 0;

   //

   /**
    * Compute the total integration time for the group.
    *
    * @param {Boolean} onlyActiveFrames true if total time takes into account only active frames
    * @returns total exposure as numerica value in seconds.
    */
   this.totalExposureTime = ( onlyActiveFrames ) =>
   {
      return ( onlyActiveFrames ? this.activeFrames() : this.fileItems ).reduce( ( acc, fileItem ) => ( acc + fileItem.exposureTime ), 0 );
   }

   /**
    * Merge the keywords object into the group's keywords object filtering only
    * the keywords enabled for the group's mode.
    *
    * @param {{String:String}} keywords the keywords key-value map
    */
   this.mergeKeywords = function( keywords )
   {
      Object.keys( keywords ).forEach( k =>
      {
         this.keywords[ k ] = keywords[ k ];
      } );
      this.keywords = engine.keywords.filterKeywordsForMode( this.keywords, mode );
   };

   // MANUAL OVERRIDES

   this.forceNoDark = false;
   this.forceNoFlat = false;
   this.overrideDark = undefined;
   this.overrideFlat = undefined;

   if ( firstItem )
   { // we pass null from importParameters()
      this.fileItems.push( firstItem );
      this.mergeKeywords( firstItem.keywords );
   }

   //

   /**
    * Generates the group ID.
    *
    * @returns
    */
   this.generateID = () =>
   {
      // WBPP 2.0.2 - do not change!
      let pre_id_items = [
         this.imageType,
         this.binning,
         this.filter,
         this.exposureTime,
         this.isCFA ? "CFA" : "gray"
      ];

      // WBPP 2.1 and above
      // NB: **** migration to be update in case of changes! ****
      let post_id_items = [
         this.mode
      ];

      let ID = pre_id_items.join( "_" )
      ID = ID + Object.keys( keywords ).map( k => k + ":" + keywords[ k ] ).join( "_" );
      ID = ID + "_" + post_id_items.join( "_" )

      return ID;
   }

   //

   this.id = this.generateID();

   /**
    * Returns the list of active items. An active item is a frame that
    * has not yet been processed or has been successfully processed so far.
    *
    * @returns
    */
   this.activeFrames = () =>
   {
      return this.fileItems.filter( item => item.current != undefined );
   }

   /**
    * Determines if the group matches the provided parameters.
    *
    * @param {ImageType} imageType
    * @param {String} filter
    * @param {Number} binning
    * @param {Number} exposureTime
    * @param {Boolean} isCFA
    * @param {Number} exposureTolerance
    * @param {Number} lightExposureTolerance
    * @param {WBPPGroupingMode} mode
    * @returns
    */
   this.sameParameters = ( imageType, filter, binning, exposureTime, isCFA, exposureTolerance, lightExposureTolerance, mode ) =>
   {
      if ( this.imageType != imageType || this.binning != binning || this.mode != mode )
         return false;

      switch ( imageType )
      {
         case ImageType.BIAS:
            return true
         case ImageType.DARK:
            return ( Math.abs( this.exposureTime - exposureTime ) <= Math.max( CONST_MIN_EXPOSURE_TOLERANCE, exposureTolerance ) );
         case ImageType.FLAT:
         case ImageType.UNKNOWN:
            return this.filter == filter;
         case ImageType.LIGHT:
            return this.isCFA == isCFA && this.filter == filter && Math.abs( this.exposureTime - exposureTime ) <= lightExposureTolerance;
      }
      return false;
   };

   /**
    * Returns the number of matches between the values of the current group keywords and
    * the values of the keywords object provided. If a mismatch occurs( values are different )
    * then - 1 is returned otherwise the count of the matches is returend.
    * If keywordsMatchCount is true then return -1 if the group contains a keyword is not in the
    * matching keywords OR if keywords contain a keyword that is not in the gropu
    *
    * @param {{String:String}} keywords the keywords key-value map
    * @param {Boolean} strictKeywordsMatching
    *
    * @returns the number of matching keywords
    */
   this.keywordsMatchCount = function( keywords, strictKeywordsMatching )
   {
      // scan the list of group's keywords and cound the number of keywords
      let keys = Object.keys( this.keywords );
      let count = 0;
      for ( let i = 0; i < keys.length; i++ )
      {
         let key = keys[ i ];
         if ( keywords[ key ] )
         {
            if ( this.keywords[ key ] == keywords[ key ] )
               count++;
            else
               return -1;
         }
         else if ( strictKeywordsMatching )
         {
            // the group contains a keyword that is not in the provided keywords, in this case
            // the group does not strict match
            return -1;
         }
      }

      if ( strictKeywordsMatching )
      {
         // do the inverse check i.e. check if the list of provided keywords contain a keyword
         // that is not in the group, if not the group is not strictly matching
         let kw = Object.keys( keywords );
         for ( let i = 0; i < kw.length; i++ )
         {
            let key = kw[ i ];
            if ( !this.keywords[ key ] )
            {
               return -1;
            }
         }

      }
      return count;
   };

   /**
    * Adds a new file item to the group.
    *
    * @param {FileItem} item
    * @param {Boolean} ignoreCFAMatching
    * @returns on success { success: true }, {sucecss: false, message: string} otherwise
    */
   this.addFileItem = ( item, ignoreCFAMatching ) =>
   {
      // image type and group CFA must be compatible
      if ( this.imageType == ImageType.LIGHT && !ignoreCFAMatching && item.isCFA != this.isCFA )
      {
         return {
            success: false,
            message: "File " + item.filePath + " has a different CFA with respect " +
               "the CFA of group " + this.toString() + " so it will be discarded."
         }
      }

      // add the file
      if ( item.isMaster )
      {
         this.fileItems.unshift( item );
         this.hasMaster = true;
         this.setExposureTime( item.exposureTime );
      }
      else
      {
         this.fileItems.push( item );
         this.addExposureTime( item.exposureTime );
      }
      return {
         success: true
      }
   };

   /**
    * Removes a file item at index i.
    *
    * @param {Number} i
    */
   this.removeItem = ( i ) =>
   {
      this.fileItems.splice( i, 1 );
      // if the new on-top element is a master then update the group accordingly
      if ( this.fileItems.length > 0 )
      {
         if ( this.fileItems[ 0 ] )
            this.hasMaster = this.fileItems[ 0 ].isMaster;
      }
      else
      {
         this.hasMaster = false;
      }
   };

   //
   /**
    * Checks if the provided rejection type is good for the group.
    *
    * @param {*} rejection
    * @returns
    */
   this.rejectionIsGood = function( rejection )
   {
      if ( rejection == ImageIntegration.prototype.auto )
         return [ true, "" ];

      // Invariants
      switch ( rejection )
      {
         case ImageIntegration.prototype.NoRejection:
            return [ false, "No pixel rejection algorithm has been selected" ];
         case ImageIntegration.prototype.MinMax:
            return [ false, "Min/Max rejection should not be used for production work" ];
         case ImageIntegration.prototype.CCDClip:
            return [ false, "CCD clipping rejection has been deprecated" ];
         default:
            break;
      }
      let selectedRejection = ( rejection !== ImageIntegration.prototype.auto ) ? rejection : this.bestRejectionMethod();

      // Selections dependent on the number of frames
      let n = this.fileItems.length;
      switch ( selectedRejection )
      {
         case ImageIntegration.prototype.PercentileClip:
            if ( n > 8 )
               return [ false, "Percentile clipping should only be used for small sets of eight or less images" ];
            break;
         case ImageIntegration.prototype.SigmaClip:
            if ( n < 8 )
               return [ false, "Sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping" ];
            if ( n > 15 )
               return [ false, "Winsorized sigma clipping will work better than sigma clipping for sets of 15 or more images" ];
            break;
         case ImageIntegration.prototype.WinsorizedSigmaClip:
            if ( n < 8 )
               return [ false, "Winsorized sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping" ];
            break;
         case ImageIntegration.prototype.AveragedSigmaClip:
            if ( n < 8 )
               return [ false, "Averaged sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n > 10 )
               return [ false, "Sigma clipping or Winsorized sigma clipping will work better than averaged sigma clipping for sets of 10 or more images" ];
            break;
         case ImageIntegration.prototype.LinearFit:
            if ( n < 8 )
               return [ false, "Linear fit clipping requires at least 15 images to provide reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n < 20 )
               return [ false, "Linear fit clipping may not be better than Winsorized sigma clipping for sets of less than 15-20 images" ];
            break;
         case ImageIntegration.prototype.Rejection_ESD:
            if ( n < 8 )
               return [ false, "ESD requires at least 15 images to provide reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n < 20 )
               return [ false, "ESD may not be better than Winsorized sigma clipping for sets of less than 20 images" ];
            if ( n < 25 )
               return [ false, "ESD  may not be better than linear fit clipping for sets of less than 20-25 images" ];
         default: // ?!
            break;
      }

      return [ true, "" ];
   };

   /**
    * Returns the best rejection method for the group.
    * NOTE: the best rejection is based on the current number of active frames.
    *
    * @returns
    */
   this.bestRejectionMethod = function()
   {
      let n = this.activeFrames().length;
      if ( n < 8 )
         return ImageIntegration.prototype.PercentileClip;
      if ( n <= 10 )
         return ImageIntegration.prototype.AveragedSigmaClip;
      if ( n < 20 )
         return ImageIntegration.prototype.WinsorizedSigmaClip;
      if ( n < 25 )
         return ImageIntegration.prototype.LinearFit;
      return ImageIntegration.prototype.Rejection_ESD;
   };

   /**
    * Adds a new exposure time and update the exposure data accordingly.
    *
    * @param {Number} time
    */
   this.addExposureTime = function( time )
   {
      // check exposure with tolerance
      let hasExposure = false;
      for ( let i = 0; i < this.exposureTimes.length; i++ )
      {
         if ( Math.abs( time - this.exposureTimes[ i ] ) < CONST_MIN_EXPOSURE_TOLERANCE )
         {
            hasExposure = true;
            break;
         }
      }
      if ( !hasExposure )
      {
         this.exposureTimes.push( time );
         this.exposureTimes.sort( ( a, b ) => a > b )
      }
   };

   /**
    * Assign the nominal exposure time of the group.
    *
    * @param {Number} time
    */
   this.setExposureTime = function( time )
   {
      let sanitizedValue = ( this.imageType != ImageType.BIAS ) ? time : 0;
      this.exposureTimes = [ sanitizedValue ];
      this.exposureTime = sanitizedValue;
   };

   /**
    * Returns the formatted nominal exposure string.
    *
    * @returns
    */
   this.exposureToString = function()
   {
      return format( "%.2fs", this.exposureTime );
   };

   /**
    * Returns the formatted frames exposures string.
    *
    * @returns
    */
   this.exposuresToString = function()
   {
      if ( this.exposureTimes.length > 1 )
         return '[' + this.exposureTimes.map( exposure => format( "%.2fs", exposure ) ).join( ', ' ) + ']';
      return this.exposureToString();
   };

   /**
    * Returns an extended formatted string describing the nominal exposure plus the list of all the different
    * exposures of the frames in the group.
    *
    * @returns
    */
   this.exposuresToExtendedString = function()
   {
      if ( this.exposureTimes.length > 1 )
         return format( "%.2fs", this.exposureTimes[ this.exposureTimes.length - 1 ] ) +
            ' - [' + this.exposureTimes.map( ( exposure ) => format( "%.2fs", exposure ) ).join( ', ' ) + ']';
      return format( "%.2fs", this.exposureTime );
   };

   /**
    * Logs the group information to eh console.
    */
   this.log = function()
   {
      console.noteln( 'Group of ', this.fileItems.length, ' ', StackEngine.imageTypeToString( this.imageType ), ' frames (', this.activeFrames().length, ' active)' );
      console.noteln( 'BINNING  : ', this.binning );
      console.noteln( 'Filter   : ', this.filter.length > 0 ? this.filter : 'NoFilter' );
      console.noteln( 'Exposure : ', this.exposuresToString() );
      console.noteln( 'Keywords : [', this.keywordsToString(), ']' );
      console.noteln( 'Mode     : ', this.modeToString() );
      if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
         console.noteln( 'Color   : ', this.isCFA ? 'CFA/RGB' : 'Gray' );
   };

   /**
    * Returns a rich text string to be the header of a group section in the process logged output.
    *
    * @param {String} title custom header title
    * @returns
    */
   this.logStringHeader = ( title ) =>
   {
      let activeCount = this.activeFrames().length;
      let header = '<b>********************</b> <i>' + title + '</i> <b>********************';
      this.footerLengthForCurrentHeader = header.length - '<b></b><i></i><b>'.length;
      let str = '<b>' + header + '\n';
      str += 'Group of ' + this.fileItems.length + ' ' + StackEngine.imageTypeToString( this.imageType ) + ' frames (' + activeCount + ' active)\n';
      str += 'BINNING  : ' + this.binning;
      if ( this.imageType != ImageType.BIAS && this.imageType != ImageType.DARK )
         str += '\n' + 'Filter   : ' + ( this.filter.length > 0 ? this.filter : 'NoFilter' );
      if ( this.imageType != ImageType.BIAS )
         str += '\n' + 'Exposure : ' + this.exposuresToString();
      str += '\n' + 'Grouping Keywords [' + this.keywordsToString() + ']';
      str += '\n' + 'Mode     : ' + this.modeToString();
      if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
         str += '\n' + 'Color    : ' + ( this.isCFA ? 'CFA/RGB' : 'Gray' );
      str += "</b>\n"
      return str;
   };

   /**
    * Returns a rich text string to be the footer of a group section in the process logger output.
    *
    * @returns
    */
   this.logStringFooter = function()
   {
      return '<b>' + '*'.repeat( this.footerLengthForCurrentHeader ) + '</b>\n';
   };

   /**
    * Returns a single line group information string.
    *
    * @returns
    */
   this.toString = function()
   {
      let a = [];
      if ( !isEmptyString( this.filter ) )
         a.push( "filter = " + this.filter );
      else
         a.push( "filter = NoFilter" );
      a.push( "binning = " + this.binning.toString() );
      if ( this.exposureTimes.length == 1 )
      {
         a.push( format( "exposure = %.2fs", this.exposureTime ) );
      }
      else if ( this.exposureTimes.length > 1 )
         a.push( 'exposures = ', this.exposuresToString() );
      a.push( 'grouping keywords [' + this.keywordsToString() + "]" );
      a.push( 'mode = ' + this.modeToString() );
      let activeCount = this.activeFrames().length;
      a.push( "frames = " + this.fileItems.length.toString() + " (" + activeCount + " active)" );
      let s = StackEngine.imageTypeToString( this.imageType ) + " frames (";
      s += a[ 0 ];
      for ( let i = 1; i < a.length; ++i )
         s += ", " + a[ i ];
      s += ")";
      return s;
   };

   /**
    * Returns a very short group information string to be used in a dropdown list.
    *
    * @returns
    */
   this.dropdownShortString = function()
   {
      let retVal = "";
      switch ( this.imageType )
      {
         case ImageType.BIAS:
            retVal = "BIN " + this.binning;
            break;
         case ImageType.DARK:
            retVal = "BIN " + this.binning + "   " + this.exposureToString();
            break;
         case ImageType.FLAT:
         case ImageType.LIGHT:
            retVal = this.filter + "  BIN " + this.binning + "  " + this.exposureToString();
            break;
      }
      return retVal;
   };

   /**
    * Returns the group's CFA pattern string.
    *
    * @returns
    */
   this.CFAPatternString = function()
   {
      let patterns = [ "Auto", "RGGB", "BGGR", "GBRG", "GRBG", "GRGB", "GBGR", "RGBG", "BGRG" ];
      return patterns[ this.CFAPattern ];
   };

   /**
    * Returns the group's debayer method string.
    *
    * @returns
    */
   this.debayerMethodString = function()
   {
      let methods = [ "SuperPixel", "Bilinear", "VNG" ];
      return methods[ this.debayerMethod ];
   };

   /**
    * Returns a string listing all the group's keywords and values.
    * The list follows the global keywords definition order.
    *
    * @returns
    */
   this.keywordsToString = () =>
   {
      let keywordsString = "";
      let separator = "";
      engine.keywords.names().forEach( name =>
      {
         if ( this.keywords[ name ] )
         {
            keywordsString += separator + name + ": " + this.keywords[ name ];
            separator = ", "
         }
      } );
      return keywordsString;
   };

   /**
    * Returns a string describing the group's mode.
    *
    * @returns
    */
   this.modeToString = () =>
   {
      switch ( this.mode )
      {
         case WBPPGroupingMode.PRE:
            return "pre-processing";
         case WBPPGroupingMode.POST:
            return "post-processing";
      }
      return "-";
   }

   /**
    * Performs all the group's sanity check and returns an object optionally containing the warnings or errors found.
    *
    * @returns {Object} an empty object on success, { warnings?: String, errors?: String } otherwise
    */
   this.status = function()
   {
      if ( this.hasMaster || this.mode != WBPPGroupingMode.PRE )
         return {};

      let statusString = "";

      // get the calibration groups
      let cg = engine.getCalibrationGroupsFor( this );

      // ERROR CHECK: LIGHT
      if ( this.imageType == ImageType.LIGHT )
      {
         // check if for any reason a master flat has different isCFA value than light group
         if ( cg.masterFlat && cg.masterFlat.isCFA != this.isCFA )
         {
            if ( this.isCFA )
            {
               statusString +=
                  "<p><b>Light frames are marked as CFA images but the Master Flat is not.</b><br>" +
                  "<i>Light frames mosaiced with a CFA pattern cannot be calibrated using a monochromatic Master Flat.</i><p>"
            }
            else
            {
               statusString +=
                  "<p><b>Light frames are marked as monochromatic but the Master Flat is marked as a colorized CFA image.</b><br>" +
                  "<i>Monochromatic Light frames must be calibrated with a monochromatic Master Flat.</i><p>"
            }
         }
      }

      // Returns errors immadiatly
      if ( statusString.length > 0 )
      {
         return {
            errors: statusString
         };
      }

      // CHECK: calibration files for FLAT
      if ( this.imageType == ImageType.FLAT )
         if ( !cg.masterBias && !cg.masterDark )
         {
            statusString +=
               "<p><b>Neither a Master Bias nor a Master Dark matches.</b><br>" +
               "<i>Flat frames will be integrated without being calibrated.</i><p>"
         }

      // CHECK: calibration files for LIGHT
      if ( this.imageType == ImageType.LIGHT )
         if ( !cg.masterBias && !cg.masterDark && !cg.masterFlat )
         {
            statusString +=
               "<p><b>No matching Master Bias, Master Dark and Master Flat found.</b><br>" +
               "<i>Light frames will be integrated without being calibrated.</i><p>"
         }
      else if ( !cg.masterBias && !cg.masterDark )
      {
         statusString +=
            "<p><b>Neither Bias nor Dark master file matches.</b><br>" +
            "<i>Light frames will not be bias and/or dark subtracted before being calibrated by the Master Flat. This configuration may not completely calibrate Light frames.</i><p>"
      }

      // CHECK: current group is FLAT or LIGHT
      if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
      {
         // dark doesn't contain the bias and bias is missing
         if ( !cg.masterBias && cg.masterDark && !cg.masterDark.containsBias )
            statusString +=
            "<p><b>Master Dark doesn't contain the bias and no Master Bias matches.</b><br>" +
            "<i>This configuration should be avoided since the bias will not be removed from frames during the calibration process.</i><p>"

         // dark contains the bias and is optimized but bias is missing
         if ( !cg.masterBias && cg.masterDark && cg.masterDark.containsBias && this.optimizeMasterDark )
            statusString +=
            "<p><b>Master Dark contains the bias and will be optimized but no Master Bias matches.</b><br>" +
            "<i>This configuration should be avoided since the optimization should be performed on a bias-subtracted Master Dark.</i><p>"

         // dark has different exposure time and it's not optimized
         if ( cg.masterDark )
         {
            let dT = Math.abs( cg.masterDark.exposureTime - this.exposureTime );
            if ( !this.optimizeMasterDark && dT > 5 )
            {
               // main sentence
               statusString +=
                  "<p><b>" + StackEngine.imageTypeToString( this.imageType ) + " frame's exposure differs from the Master Dark's exposure.</b><br>";
               // suggestion:
               if ( this.imageType == ImageType.FLAT )
               {
                  statusString += "<i>You can disable the use \"Dark\" checkbox and this will subtract the Master Bias only.</i><p>"
               }
               else if ( this.imageType == ImageType.LIGHT )
               {
                  if ( cg.masterBias )
                     statusString += "<i>You can optimize the Master Dark in order to achieve a better dark current estimation.</i><p>"
                  else
                     statusString += "<i>You can add a compatible Master Bias and optimize the Master Dark in order to achieve a better dark current estimation.</i><p>"
               }
            }
         }

      }

      return statusString.length > 0 ?
      {
         warnings: statusString
      } :
      {};
   }
}

FrameGroup.prototype = new Object;

// ----------------------------------------------------------------------------
/**
 * This object is responsible of managing multiple grouping startegies. In the current implementation only two
 * kind of grouping are implemented: pre-processing and post-processing grouping.
 * Any file should be added using this manager, in such way the file is added to all managed groupings in order
 * to quickly switch between the two later.
 * Any time the list of groups for a grouping strategy is needed you just need to use the groupsForMode function
 * providing the grouping mode and it will return the corresponding list of groups.
 */
function FrameGroupsManager()
{
   // groups for pre processing phase
   this.groups = [];
   this.cache = {};

   /**
    * Initialize all data for processing
    *
    */
   this.initializeProcessing = () =>
   {
      this.groups.forEach( group => group.fileItems.forEach( item => item.initForProcessing() ) )
   }

   /**
    * Adds a new group with the given properties.
    *
    * @param {ImageType} imageType
    * @param {String} filter
    * @param {Number} binning
    * @param {Number} exposureTime
    * @param {Boolean} isCFA
    * @param {String} fileItem
    * @param {Boolean} isMaster
    * @param {{String:String}?} itemKeywords
    * @param {WBPPGroupingMode} mode
    */
   this.addGroup = ( imageType, filter, binning, exposureTime, isCFA, fileItem, isMaster, keywords, mode ) =>
   {
      // append the new group
      this.groups.push( new FrameGroup(
         imageType,
         filter,
         binning,
         exposureTime,
         isCFA,
         fileItem,
         isMaster,
         keywords,
         mode ) );

      // keep groups sorted by BINNING, FILTER and EXPOSURE
      this.groups.sort( ( a, b ) =>
      {
         if ( a.binning != b.binning ) return a.binning > b.binning;
         if ( a.filter != b.filter ) return a.filter.toLowerCase() > b.filter.toLowerCase();
         return a.exposureTime < b.exposureTime;
      } );
   }

   /**
    * Adds a new file item. customKeywoeds and customModes are used by Linear
    * Pattern Subtraction in order to regroup frames by binning, exposure and filter name
    * ignoring the keywords and keep the new generated groups separated form the pre and post
    * groups.
    *
    * @param {FileItem} fileItem
    * @param {*} customKeywords
    * @param {*} customModes
    * @param {*} customCFA
    * @returns
    */
   this.addFileItem = ( fileItem, customKeywords, customModes, customCFA ) =>
   {
      // only light frames are added to both pre and post process modes. Bias, Dark and Flat are added only to pre-process mode.
      let modes = customModes || ( fileItem.imageType == ImageType.LIGHT ? [ WBPPGroupingMode.PRE, WBPPGroupingMode.POST ] : [ WBPPGroupingMode.PRE ] );
      // for all modes iterate and add the file
      modes.forEach( mode =>
      {

         let group = engine.findGroup(
            fileItem.imageType,
            fileItem.filter,
            fileItem.binning,
            fileItem.exposureTime,
            fileItem.isMaster,
            customCFA || fileItem.isCFA,
            engine.darkExposureTolerance,
            engine.lightExposureTolerance,
            customKeywords || fileItem.keywords,
            true, /* strictKeywordsMatching */
            mode
         );

         // add the item if a group has been found, create a new group otherwise
         if ( group )
         {
            group.addFileItem( fileItem, true /* ignoreCFAMatching */ );
         }
         else
         {
            this.addGroup(
               fileItem.imageType,
               fileItem.filter,
               fileItem.binning,
               fileItem.exposureTime,
               customCFA || fileItem.isCFA,
               fileItem,
               fileItem.isMaster,
               customKeywords || fileItem.keywords,
               mode );
         }
      } );

      return {
         success: true
      };
   }

   /**
    * Returns the group with the provided id.
    *
    * @param {String} id the id that the returned group has
    * @returns the group with the provided id
    */
   this.getGroupByID = ( id ) =>
   {
      let matching = this.groups.filter( g => g.id == id );
      return matching.length == 1 ? matching[ 0 ] : undefined
   };

   /**
    * Returns the list of group matching the given mode
    *
    * @param {WBPPKeywordMode} mode
    * @returns
    */
   this.groupsForMode = ( mode ) =>
   {
      return this.groups.filter( group =>
      {
         return group && group.mode == mode
      } )
   }

   /**
    * Removes a group given its index and mode.
    *
    * @param {Numeric} i group index
    */
   this.removeGroupAtIndex = ( i ) =>
   {

      // sanitize overrides i.e. clear any override that references the group
      if ( this.groups[ i ] && this.groups[ i ].mode == WBPPGroupingMode.PRE )
      {
         let id = this.groups[ i ].id;
         this.groups.forEach( group =>
         {
            if ( !group.overrideDark || group.overrideDark.id == id )
            {
               group.overrideDark = undefined;
            }
            if ( !group.overrideFlat || group.overrideFlat.id == id )
            {
               group.overrideFlat = undefined;
            }
         } );
      }

      // remove the group from the list
      this.groups.splice( i, 1 );
   }

   /**
    * Delete all groups of the given type and mode
    *
    * @param {ImageType} imageType
    */
   this.deleteFrameSet = ( imageType, mode ) =>
   {
      for ( let i = 0; i < this.groups.length; ++i )
         // use mode only if defined
         if ( this.groups[ i ].imageType == imageType && this.groups[ i ].mode == ( mode || this.groups[ i ].mode ) )
            this.removeGroupAtIndex( i-- );
   };

   /**
    * Removes all groups.
    *
    */
   this.clear = () =>
   {
      this.groups = [];
   }

   /**
    * Returns customizable options for all groups. The object returned has the group id
    * as value and an object containing the properties as value.
    *
    * @returns
    */
   this.cacheGroupsProperties = () =>
   {
      // store all cachable properties for each group by ID
      this.groups.forEach( group =>
      {
         if ( group && group.id ) // for compatibility with WBPP 1.x
            this.cache[ group.id ] = {
               containsBias: group.containsBias,
               optimizeMasterDark: group.optimizeMasterDark,
               separateCFAFlatScalingFactors: group.separateCFAFlatScalingFactors,
               isCFA: group.isCFA,
               CFAPattern: group.CFAPattern,
               debayerMethod: group.debayerMethod,
               lightOutputPedestal: group.lightOutputPedestal,

               forceNoDark: group.forceNoDark,
               forceNoFlat: group.forceNoFlat,
               overrideDarkID: group.overrideDark && group.overrideDark.id,
               overrideFlatID: group.overrideFlat && group.overrideFlat.id
            }
      } );
   };

   /**
    * Restores customizable groups options from an object. The object shoule be
    * generated by the getGroupsProperties function.
    *
    * @param {*} properties
    */
   this.restoreGroupsPropertiesFromCache = () =>
   {
      this.groups.forEach( group =>
      {
         let properties = this.cache[ group.id ]
         if ( properties )
         {
            group.containsBias = properties.containsBias;
            group.optimizeMasterDark = properties.optimizeMasterDark;
            group.separateCFAFlatScalingFactors = properties.separateCFAFlatScalingFactors;
            group.isCFA = properties.isCFA;
            group.CFAPattern = properties.CFAPattern;
            group.debayerMethod = properties.debayerMethod;
            group.lightOutputPedestal = properties.lightOutputPedestal;

            group.forceNoDark = properties.forceNoDark;
            group.forceNoFlat = properties.forceNoFlat;
            group.overrideDark = this.getGroupByID( properties.overrideDarkID );
            group.overrideFlat = this.getGroupByID( properties.overrideFlatID );

            // for compatibility with WBPP 2.0 - 2.0.2
            if ( group.lightOutputPedestal == null )
               group.lightOutputPedestal = 0;
         }
      } );
   }

   /**
    * Returns the total light frames integration time.
    *
    * @returns
    */
   this.totalIntegrationTime = () =>
   {
      return this.groupsForMode( WBPPGroupingMode.POST ).reduce( ( acc, group ) =>
      {
         return acc + group.totalExposureTime();
      }, 0 );
   }

   /**
    * Returns the whole list of file items in the session.
    *
    * @returns
    */
   this.allFileItems = () =>
   {
      return this.groupsForMode( WBPPGroupingMode.POST ).reduce( ( acc, g ) =>
      {
         return acc.concat( g.fileItems );
      }, [] );
   }

   /**
    * Returns the fileItem in the session that matches the provided filePath.
    * If none or more than one item are found then returns undefined.
    *
    * @param {*} filePath
    */
   this.getReferenceFrameFileItem = ( filePath ) =>
   {
      console.noteln( "getReferenceFrameFileItem: ", filePath )
      let fileItem = this.allFileItems().filter( item => item.filePath == filePath );
      console.noteln( "fileItem.length: ", fileItem.length )
      return fileItem.length == 1 ? fileItem[ 0 ] : undefined;
   }
}
FrameGroupsManager.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Overscan region constructor
 *
 */
function OverscanRegions()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether to apply this overscan correction
   this.sourceRect = new Rect( 0 ); // source overscan region
   this.targetRect = new Rect( 0 ); // image region to be corrected

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      if ( !this.sourceRect.isNormal || !this.targetRect.isNormal )
         return false;
      if ( this.sourceRect.x0 < 0 || this.sourceRect.y0 < 0 ||
         this.targetRect.x0 < 0 || this.targetRect.y0 < 0 )
         return false;
      return true;
   };
}

OverscanRegions.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Overscan object constructor
 *
 */
function Overscan()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether overscan correction is globally enabled

   this.overscan = new Array; // four overscan source and target regions
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );

   this.imageRect = new Rect( 0 ); // image region (i.e. the cropping rectangle)

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      for ( let i = 0; i < 4; ++i )
         if ( !this.overscan[ i ].isValid() )
            return false;
      if ( !this.imageRect.isNormal )
         return false;
      if ( this.imageRect.x0 < 0 || this.imageRect.y0 < 0 )
         return false;
      return true;
   };

   this.hasOverscanRegions = function()
   {
      for ( let i = 0; i < 4; ++i )
         if ( this.overscan[ i ].enabled )
            return true;
      return false;
   };
}

Overscan.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Main StackEngine object constructor
 *
 */
function StackEngine()
{
   this.__base__ = Object;
   this.__base__();

   this.diagnosticMessages = new Array;

   // allocate structures
   this.overscan = new Overscan;
   this.combination = new Array( 4 );
   this.rejection = new Array( 4 );
   this.minMaxLow = new Array( 4 );
   this.minMaxHigh = new Array( 4 );
   this.percentileLow = new Array( 4 );
   this.percentileHigh = new Array( 4 );
   this.sigmaLow = new Array( 4 );
   this.sigmaHigh = new Array( 4 );
   this.linearFitLow = new Array( 4 );
   this.linearFitHigh = new Array( 4 );
   this.ESD_Outliers = new Array( 4 );
   this.ESD_Significance = new Array( 4 );
   this.ESD_LowRelaxation = new Array( 4 );

   this.groupsManager = new FrameGroupsManager();

   // default parameters
   setDefaultParameters.apply( this );

   // process logger
   this.processLogger = new ProcessLogger();
}

StackEngine.prototype = new Object;

var engine = new StackEngine;

// ----------------------------------------------------------------------------
// REFERENCE FRAME MANAGEMENT
// ----------------------------------------------------------------------------
StackEngine.prototype.getBestReferenceFrameModes = function()
{
   // store the current selected item
   let selections = [ "manual", "auto" ];
   let postProcessKeywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
   for ( let i = 0; i < postProcessKeywords.length; i++ )
   {
      selections.push( "auto by " + postProcessKeywords[ i ].name );
   }
   return selections;
}

StackEngine.prototype.setBestReferenceFrameMode = function( index )
{
   this.bestFrameRefernceMethod = index;
   if ( index == WBPPBestRefernenceMethod.AUTO_KEYWORD )
   {
      let options = this.getBestReferenceFrameModes();
      this.bestFrameReferenceKeyword = options[ index ].replace( "auto by ", "" );
   }
   else
   {
      this.bestFrameReferenceKeyword = "";
   }
}

// ----------------------------------------------------------------------------
// GROUPS GETTERS
// ----------------------------------------------------------------------------

//
/**
 * Returns a sorted list of groups of a given type and mode.
 *
 * @param {ImageType} type type of gorups to be retrieved
 * @param {WBPPGroupingMode} mode the grouping mode
 * @returns the sorted list of groups
 */
StackEngine.prototype.getSortedGroupsOfType = function( type, mode )
{
   let groupsByType = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; i++ )
      if ( groups[ i ].imageType == type )
         groupsByType.push( groups[ i ] );

   // sort result
   groupsByType.sort( ( a, b ) =>
   {
      // master file on bottom
      if ( a.hasMaster != b.hasMaster )
         return a.hasMaster > b.hasMaster;
      // filter by binning
      if ( a.binning != b.binning )
         return a.binning > b.binning;
      // for Flats, the filter is the only third sorting rule
      if ( type == ImageType.FLAT )
         return a.filter > b.filter;
      // filter by duration
      if ( a.exposureTime != b.exposureTime )
         return a.exposureTime > b.exposureTime;
      // filter by filter name
      if ( type != ImageType.FLAT )
         return a.filter > b.filter;
      return 0;
   } );

   return groupsByType;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that are calibrated by the provided group.
 * NB: by default this function looks the calibration groups with mode WBPPGroupingMode = .PRE
 *
 * @param {FrameGroup} group the group that calibrates all the returned groups
 * @returns the array of groups that are calibrated by the provided group
 */
StackEngine.prototype.getGroupsCalibratedBy = function( group )
{
   let calibratedBy = [];

   // Scan all pre-processing groups, search for the calibration files for each group
   // of the same type of the provided group, if the ID matches then the looped group
   // is calibrated with the provided file
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
   {
      let cg = groups[ i ];

      if ( cg == group )
         continue;

      let cf = this.getCalibrationGroupsFor( cg );

      if ( group.imageType == ImageType.BIAS && group == cf.masterBias )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.DARK && group == cf.masterDark )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.FLAT && group == cf.masterFlat )
      {
         calibratedBy.push( cg );
         continue
      }
   }
   return calibratedBy;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that calibrate the provided group.
 *
 * @param {FrameGroup} group the group for which we want to retrieve the calibration groups
 * @returns the array of groups that calibrate the provided group
 */
StackEngine.prototype.getCalibrationGroupsFor = function( group )
{
   let calibrationGroups = {
      masterBias: undefined,
      masterDark: undefined,
      masterFlat: undefined,
   };
   // a master file is not calibrated
   if ( group.hasMaster )
      return calibrationGroups;

   let mb, md, mf;
   let binning = group.binning;
   let exposureTime = group.exposureTime;
   let filter = group.filter;
   let exactDarkExposureTime = false;
   let isCFA = group.isCFA;

   switch ( group.imageType )
   {
      case ImageType.DARK:
         // look for compatible master bias
         mb = this.getMasterBiasGroup( group.binning, false /*isMaster*/ , group.keywords );
         break;
      case ImageType.FLAT:
         // look for compatible master dark and master bias
         mb = this.getMasterBiasGroup( binning, false, group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( binning, exposureTime, exactDarkExposureTime, group.keywords );
         break;
      case ImageType.LIGHT:
         mb = this.getMasterBiasGroup( binning, false, group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( binning, exposureTime, exactDarkExposureTime, group.keywords );
         if ( !group.forceNoFlat )
            mf = group.overrideFlat || this.getMasterFlatGroup( binning, filter, isCFA, false /*isMaster*/ , group.keywords );
         break;
   }

   // Advanced logic: remove master bias when it is not necessary
   // MasterBias is needed only when:
   // 1. master dark is NOT present
   // 2. if master dark is present and optimized
   // 3. master dark is present but does not contain the bias
   if ( mb )
   {
      let masterDarkISNotPResent = !md;
      let masterDarkIsOptimized = md && group.optimizeMasterDark;

      if ( masterDarkISNotPResent || masterDarkIsOptimized )
         calibrationGroups.masterBias = mb;
   }
   if ( md )
      calibrationGroups.masterDark = md;
   if ( mf )
      calibrationGroups.masterFlat = mf;

   return calibrationGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns an array of groups of the image type provided that are compatible
 * with the given keywords.
 * For each group, the number of matching keywors is counted, groups with the same
 * count are grouped together and at the end the first array of the groups with
 * the highest matching count is sorted by keyword precedence and returned.
 *
 * @param {ImageType} imageType
 * @param {{String:String}} keywords the keywords key-value map
 * @returns
 */
StackEngine.prototype.getCompatibleCalibrationGroups = function( imageType, keywords )
{
   // preselect the compatible groups
   let matchingGroups = {};
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         let count = groups[ i ].keywordsMatchCount( keywords, false /* strictKeywordsMatching */ );
         if ( count >= 0 )
         {
            if ( !matchingGroups[ count ] )
               matchingGroups[ count ] = [];
            matchingGroups[ count ].push( i );
         }
      }

   // matchingGroups is a map between the count number and the array of compatible groups
   // we select the groups with the highest matching count
   let sortedCounts = Object.keys( matchingGroups );
   sortedCounts.sort();
   sortedCounts.reverse();

   // search for the best dark within the candidates
   let compatibleGroups = [];

   // extract the candidates
   if ( sortedCounts.length > 0 )
   {
      let keywords = engine.keywords.names();
      compatibleGroups = matchingGroups[ sortedCounts[ 0 ] ].map( i => groups[ i ] );
      compatibleGroups.sort( ( a, b ) =>
      {
         // process keywords respecting the order, if a keyword is found in group A
         // but not in group B then A has precedence and viceversa. If they have the same
         // keywords then the behavior is undefined
         for ( let j = 0; j < keywords.length; j++ )
         {
            let aKeyword = a.keywords[ keywords[ j ] ];
            let bKeyword = b.keywords[ keywords[ j ] ];
            if ( aKeyword != undefined && bKeyword == undefined )
               return -1;
            else if ( ( aKeyword == undefined && bKeyword != undefined ) )
               return 1;
         }
         return 0;
      } )
   }
   return compatibleGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns the list of groups that matches the criteria provided.
 * This function accepts an object with key - value that must be matched by the returned group.
 *
 * @param {{}} properties key-value pair to be matched
 * @returns
 */
StackEngine.prototype.getCalibrationGroupsMatching = function( properties )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   return groups.reduce( ( matchingGroups, group ) =>
   {
      let keys = Object.keys( properties );
      for ( let i = 0; i < keys.length; i++ )
      {
         let key = keys[ i ];
         if ( group[ key ] != properties[ key ] )
            return matchingGroups;
      }
      let retVal = matchingGroups.slice();
      retVal.push( group );
      return retVal;
   }, [] );
};

// ----------------------------------------------------------------------------

/**
 * Search for the Master Bias Group matching the given parameters.
 *
 * @param {Numeric} binning binning to match
 * @param {Boolean} isMaster true if the group has to contain a mster file, false otherwise
 * @param {{String:String}} keywords the keywords key-value map
 * @returns the matching masterBias group on success, undefined otherwise
 */
StackEngine.prototype.getMasterBiasGroup = function( binning, isMaster, keywords )
{
   let compatibleBias = this.getCompatibleCalibrationGroups( ImageType.BIAS, keywords );

   for ( let i = 0; i < compatibleBias.length; ++i )
      if ( !isMaster || compatibleBias[ i ].hasMaster )
         if ( compatibleBias[ i ].binning == binning )
            return compatibleBias[ i ];
   return undefined;
};

// ----------------------------------------------------------------------------

/**
 * Search for the first Master Bias file matching the parameters.
 *
 * @param {Numeric} binning binning to match
 * @param {{String:String}} keywords the keywords key-value map
 * @returns the matching masterBias file name
 */
StackEngine.prototype.getMasterBiasFrame = function( binning, keywords )
{
   // search the matching master bias file
   let group = this.getMasterBiasGroup( binning, true /*isMaster*/ , keywords );
   return group ? group.fileItems[ 0 ].filePath : "";
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master dark given the parameters.
 *
 * @param {Numeric} binning binning to match
 * @param {Numeric} exposureTime exposure time to search for
 * @param {Boolean} findExactExposureTime true if exposure time must match exactly, false otherwise
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @param {Boolean} logResult true if the search result of the master dark needs to print some log
 *                      information on the console
 * @returns the matching master dark group
 */
StackEngine.prototype.getMasterDarkGroup = function( binning, exposureTime, findExactExposureTime, keywords, isMaster, logResult )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // Assume no binning when binning is unknown.
   if ( binning <= 0 )
      binning = 1;

   // Ensure we get the most exposed master dark frame when the exposure time
   // is unknown. This favors scaling down dark current during optimization.
   let knownTime = exposureTime > 0;
   if ( !knownTime )
      exposureTime = 1.0e+10;

   // By default we do not search for exact duration darks.
   if ( findExactExposureTime === undefined )
      findExactExposureTime = false;

   // search for the best dark within the candidates
   let masterDarkGroup = undefined;
   let candidateDarks = this.getCompatibleCalibrationGroups( ImageType.DARK, keywords );

   let foundTime = 0;
   let bestSoFar = 1.0e+20;
   for ( let i = 0; i < candidateDarks.length; ++i )
      if ( !isMaster || candidateDarks[ i ].hasMaster )
         if ( candidateDarks[ i ].imageType == ImageType.DARK )
            if ( candidateDarks[ i ].binning == binning )
            {
               let d = Math.abs( candidateDarks[ i ].exposureTime - exposureTime );
               if ( !findExactExposureTime && d < bestSoFar || findExactExposureTime && d < CONST_FLAT_DARK_TOLERANCE )
               {
                  masterDarkGroup = candidateDarks[ i ]
                  foundTime = candidateDarks[ i ].exposureTime;
                  if ( d == 0 ) // exact match?
                     break;
                  bestSoFar = d;
               }
            }

   if ( masterDarkGroup && logResult )
      if ( foundTime > 0 )
      {
         if ( findExactExposureTime )
            console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = " +
               exposureTime + "s -- found." );
         else if ( knownTime )
            console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
               exposureTime + "s -- best match is ", foundTime + "s" );
         else
            console.noteln( "<end><cbr><br>* Using master dark frame with exposure time = ",
               foundTime + "s to calibrate unknown exposure time frame(s)." );
      }
   else
   {
      if ( findExactExposureTime )
         console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = ",
            exposureTime + "s -- not found." );
      else if ( knownTime )
         console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
            exposureTime + "s -- best match is a master dark frame of unknown exposure time." );
      else
         console.noteln( "<end><cbr><br>* Master dark match with an unknown exposure time." );
   }

   return masterDarkGroup;
};

// ----------------------------------------------------------------------------

/**
 * Returns the path of the best matching master dark given the parameters.
 *
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Boolean} findExactExposureTime true if exposure time must match exactly, false otherwise
 * @param {{String: String}} keywords { key: value } keywords object
 * @returns the matching master dark file path
 */
StackEngine.prototype.getMasterDarkFrame = function( binning, exposureTime, findExactExposureTime, keywords )
{
   let group = this.getMasterDarkGroup( binning, exposureTime, findExactExposureTime, keywords, false /*isMaster*/ , true /*log*/ );
   return group ? group.fileItems[ 0 ].filePath : "";
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master flat given the parameters.
 *
 * @param {Numeric} binning
 * @param {String} filter
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @returns the matching master flat group
 */
StackEngine.prototype.getMasterFlatGroup = function( binning, filter, isCFA, isMaster, keywords )
{
   // search for the best dark within the candidates
   let candidateFlats = this.getCompatibleCalibrationGroups( ImageType.FLAT, keywords );

   for ( let i = 0; i < candidateFlats.length; ++i )
      if ( !isMaster || candidateFlats[ i ].hasMaster )
         if ( candidateFlats[ i ].imageType == ImageType.FLAT )
            if ( candidateFlats[ i ].binning == binning && candidateFlats[ i ].filter == filter && candidateFlats[ i ].isCFA == isCFA )
               return candidateFlats[ i ];
   return undefined;
};

// ----------------------------------------------------------------------------

/**
 * Returns the path of the best matching master flat given the parameters.
 *
 * @param {Numeric} binning
 * @param {String} filter
 * @returns the path of the master flat file
 */
StackEngine.prototype.getMasterFlatFrame = function( binning, filter, isCFA, keywords )
{
   let group = this.getMasterFlatGroup( binning, filter, isCFA, true, keywords );
   return group ? group.fileItems[ 0 ].filePath : "";
};

// ----------------------------------------------------------------------------
// FRAME GROUP CEHCKS
// ----------------------------------------------------------------------------

/**
 * Returns true if groups of the given type and mode exist.
 *
 * @param {ImageType} imageType
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.hasFrames = function( imageType, mode )
{
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if bias frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasBiasFrames = function()
{
   return this.hasFrames( ImageType.BIAS, WBPPGroupingMode.PRE );
};

/**
 * Returns true if dark frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasDarkFrames = function()
{
   return this.hasFrames( ImageType.DARK, WBPPGroupingMode.PRE );
};

/**
 * Returns true if flat frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasFlatFrames = function()
{
   return this.hasFrames( ImageType.FLAT, WBPPGroupingMode.PRE );
};

/**
 * Returns true if light frame groups with the given mode exists.
 *
 * @returns
 */
StackEngine.prototype.hasLightFrames = function( mode )
{
   return this.hasFrames( ImageType.LIGHT, mode );
};

// ----------------------------------------------------------------------------
// StackEngine Methods
// ----------------------------------------------------------------------------

/**
 * Retuns the image type given the keyword value. Invoked when IMAGETYP is found in FITS header.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns the image type, UNKNOWN if type cannot be inferred
 */
StackEngine.imageTypeFromKeyword = function( value )
{
   switch ( value.toLowerCase() )
   {
      case "bias frame":
      case "bias":
      case "master bias":
         return ImageType.BIAS;
      case "dark frame":
      case "dark":
      case "master dark":
      case "flatdark":
      case "flat dark":
      case "darkflat":
      case "dark flat":
         return ImageType.DARK;
      case "flat field":
      case "flat frame":
      case "flat":
      case "master flat":
         return ImageType.FLAT;
      case "light frame":
      case "light":
      case "science frame":
      case "science":
      case "master light":
         return ImageType.LIGHT;
      default:
         return ImageType.UNKNOWN;
   }
};

// ----------------------------------------------------------------------------

/**
 * Checks if the file is to be considered a master file from the IMAGETYP keyword's value.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns true if file is to be used as master file
 */
StackEngine.isMasterFromKeyword = function( value )
{
   return value.toLowerCase().indexOf( "master" ) >= 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToString = function( imageType )
{
   return [ "Bias", "Dark", "Flat", "Light" ][ imageType ];
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the master file associated to the providede image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToMasterKeywordValue = function( imageType )
{
   return [ "Master Bias", "Master Dark", "Master Flat", "Master Light" ][ imageType ];
};

// ----------------------------------------------------------------------------
// WARNINGS AND DIAGNOSTICS
// ----------------------------------------------------------------------------

/**
 * Diagnostic messages generator.
 */
StackEngine.prototype.runDiagnostics = function()
{
   let
   {
      cleanFilterName,
      isEmptyString
   } = WBPPUtils.shared();

   this.messages = 0;

   this.top = () =>
   {
      this.messages = this.diagnosticMessages.length;
   };

   this.errorPrefix = "<b>Error</b>: ";
   this.warningPrefix = "<b>Warning</b>: ";
   this.notePrefix = "<b>Note</b>: ";

   let preprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   let postprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   /**
    * Removes all messages that are not errors or warnings until an error or warning is retrieved or
    * the top pointer is reached.
    */
   this.cleanUp = () =>
   {
      while ( this.messages < this.diagnosticMessages.length && this.diagnosticMessages.length > 0 )
      {
         let msg = this.diagnosticMessages[ this.diagnosticMessages.length - 1 ];
         if ( !msg.startsWith( this.errorPrefix ) && !msg.startsWith( this.warningPrefix ) && !msg.startsWith( this.notePrefix ) )
            this.diagnosticMessages.pop();
         else
            return;
      }
   };

   /**
    * Pushes an error message
    *
    * @param {*} message the error message
    */
   this.error = function( message )
   {
      this.diagnosticMessages.push( this.errorPrefix + "<i>" + message + "</i>." );
   };

   /**
    * Pushes a warning message.
    *
    * @param {*} message the warning message
    */
   this.warning = function( message )
   {
      this.diagnosticMessages.push( this.warningPrefix + "<i>" + message + "</i>." );
   };

   /**
    * Pushes a note message.
    *
    * @param {*} message the note message
    */
   this.note = function( message )
   {
      this.diagnosticMessages.push( this.notePrefix + message + "." );
   };

   /**
    * Adds a generic text to the diagnostic, appending a period at the end.
    *
    * @param {*} text
    */
   this.genericText = function( text )
   {
      this.diagnosticMessages.push( text + "." );
   };

   /**
    * Adds a header
    *
    * @param {*} text the header title
    */
   this.headerText = function( text )
   {
      this.diagnosticMessages.push( "<br><br>==== " + text + "" );
   };

   // initial clean up
   this.clearDiagnosticMessages();

   // ........................................................................

   this.top();
   this.headerText( "Check .xisf writer" );
   try
   {
      let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
      if ( F == null )
         throw '';
      if ( !F.canStoreFloat )
         this.error( "The " + F.name + " format cannot store 32-bit floating point image data" );
      if ( !F.canStoreKeywords )
         this.warning( "The " + F.name + " format does not support keywords" );
      if ( !F.canStoreProperties || !F.supportsViewProperties )
         this.warning( "The " + F.name + " format does not support image properties" );
      if ( F.isDeprecated )
         this.warning( "Using a deprecated output file format: " + F.name );

   }
   catch ( x )
   {
      this.error( "No installed file format can write " + ".xisf" + " files" );
   }
   this.cleanUp();

   // ........................................................................

   this.top();
   this.headerText( "Check output directory" );

   if ( isEmptyString( this.outputDirectory ) )
      this.error( "No output directory specified" );
   else if ( !File.directoryExists( this.outputDirectory ) )
      this.error( "The specified output directory does not exist: " + this.outputDirectory );
   else
   {
      try
      {
         let f = new File;
         let n = this.outputDirectory + "/__pixinsight_checking__";
         for ( let u = 1;; ++u )
         {
            let nu = File.appendToName( n, u.toString() );
            if ( !File.exists( nu ) )
            {
               n = nu;
               break;
            }
         }
         f.createForWriting( n );
         f.close();
         File.remove( n );
      }
      catch ( x )
      {
         this.error( "Cannot access the output directory for writing: " + this.outputDirectory );
      }
   }

   this.cleanUp();

   // ........................................................................

   this.top();
   this.headerText( "Check bias/dark/flat/light groups" );

   // global configuration
   if ( preprocessGroups.length == 0 && postprocessGroups.length == 0 )
      this.error( "No input frames have been provided" );
   else
   {
      if ( !this.hasBiasFrames() )
         this.note( "No bias frames have been provided" );

      if ( !this.hasDarkFrames() )
         this.note( "No dark frames have been provided" );

      if ( !this.hasFlatFrames() )
         this.note( "No flat frames have been provided" );

      if ( !this.hasLightFrames( WBPPGroupingMode.PRE ) )
         this.note( "No light frames have been provided" );
   }

   this.cleanUp();

   // ........................................................................

   // Diagnostic pre-processes BIAS, DARK, FLAT and LIGHT frames
   let groupsOrder = [ ImageType.BIAS, ImageType.DARK, ImageType.FLAT, ImageType.LIGHT ];

   this.top();
   this.headerText( " PRE-PROCESS CONFIGURATION" );

   for ( let k = 0; k < groupsOrder.length; k++ )
   {
      for ( let i = 0; i < preprocessGroups.length; ++i )
      {
         if ( preprocessGroups[ i ].imageType != groupsOrder[ k ] )
            continue;

         this.top();
         this.headerText( preprocessGroups[ i ].toString() );

         // check file existence
         for ( let j = 0; j < preprocessGroups[ i ].fileItems.length; ++j )
            if ( !File.exists( preprocessGroups[ i ].fileItems[ j ].filePath ) )
               this.error( "Nonexistent input file: " + preprocessGroups[ i ].fileItems[ j ].filePath );

         // bias-subtracted darks
         if ( preprocessGroups[ i ].imageType == ImageType.DARK && !preprocessGroups[ i ].containsBias )
            this.warning( "Using bias-subtracted master darks is considered bad practice" );

         // filter name character set
         if ( !isEmptyString( preprocessGroups[ i ].filter ) )
            if ( cleanFilterName( preprocessGroups[ i ].filter ) != preprocessGroups[ i ].filter )
               this.warning( "Invalid file name characters will be replaced with underscores " +
                  "in filter name: \'" + preprocessGroups[ i ].filter + "\'" );

         // various checks for flat and light frames
         if ( ( preprocessGroups[ i ].imageType == ImageType.FLAT || preprocessGroups[ i ].imageType == ImageType.LIGHT ) &&
            !preprocessGroups[ i ].hasMaster )
         {
            let cf = this.getCalibrationGroupsFor( preprocessGroups[ i ] )
            let masterDarkExposureDifferenceIsHigh = cf.masterDark && preprocessGroups[ i ].optimizeMasterDark && Math.abs( cf.masterDark.exposureTime - preprocessGroups[ i ].exposureTime ) > 5;

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark && !cf.masterFlat )
            {
               this.warning( "No Master Bias, Master Dark and Maser Flat have been provided to calibrate this group. Frames will not be calibrated" );
               continue;
            }

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark )
               this.warning( "Neither Master Bias nor Master Dark will be used to calibrate the frames" );

            // check if only bias is found
            if ( cf.masterBias && !cf.masterDark )
               this.warning( "No Master Dark will be used to calibrate the frames, the Master Bias will be used instead." );

            // check if only dark is found but it does not contain the master bias
            if ( !cf.masterBias && cf.masterDark && !cf.masterDark.containsBias )
               this.warning( "Frames will be calbrated without a Master Bias; and the Master Dark does not contain the Bias. As a result, a bias level will not be subtracted from the calibrated frames" );
            if ( !cf.masterBias && cf.masterDark && cf.masterDark.containsBias && preprocessGroups[ i ].optimizeDarks )
               this.warning( "Frames will be calibrated using an optimized Master Dark that contains the Bias but no Master Bias has been found. Optimizing a Master Dark without subtracting the Master Bias could (likely) lead to improper results" );

            // check if dark exposure difference is too much
            if ( masterDarkExposureDifferenceIsHigh )
               if ( preprocessGroups[ i ].optimizeDarks )
                  this.note( 'Frames will be calibrated using an optimized Master Dark with an exposure time of ' + cf.masterDark.exposureTime + ' sec' );
               else
                  this.warning( 'Frames will be calibrated using a Master Dark with a non-matching exposure time of ' + cf.masterDark.exposureTime + ' sec' );

            // check flats for light frames
            if ( preprocessGroups[ i ].imageType == ImageType.LIGHT && !cf.masterFlat )
               this.warning( "No Master Flat will be used to calibrate the frames" );

         }

         // check rejection
         if ( !( preprocessGroups[ i ].imageType == ImageType.LIGHT && !this.integrate ) && !preprocessGroups[ i ].hasMaster )
         {
            let r = preprocessGroups[ i ].rejectionIsGood( this.rejection[ preprocessGroups[ i ].imageType ] );
            if ( !r[ 0 ] ) // if not good
               this.warning( "Integration of " + preprocessGroups[ i ].toString() + ": " + r[ 1 ] ); // reason
         }

         this.cleanUp();
      }
   }

   // ........................................................................

   this.top();
   this.headerText( " POST-PROCESS CONFIGURATION" );

   // ----------------------------------------

   if ( !engine.imageRegistration && engine.integrate )
   {
      this.warning( "You decided to integrate your light frames but registration is disabled. " +
         "Ensure that your light frames are already aligned or enable the registration to properly " +
         "align them before generating the master light frames" );
   }


   for ( let i = 0; i < postprocessGroups.length; ++i )
   {
      if ( postprocessGroups[ i ].imageType != ImageType.LIGHT )
         continue;

      this.top();
      this.headerText( postprocessGroups[ i ].toString() );

      if ( this.integrate && postprocessGroups[ i ].fileItems.length < 3 )
      {
         this.error( "Only " + postprocessGroups[ i ].fileItems.length + " frames provided. Cannot integrate less than 3 light frames" );
      }

      this.cleanUp();
   }

   // ........................................................................

   // Check overscan
   if ( this.overscan.enabled )
   {
      this.top();
      this.headerText( "Check Overscan settings" );

      if ( !this.overscan.isValid() )
         this.error( "Invalid overscan region(s) defined." );
      else if ( this.overscan.enabled && !this.overscan.hasOverscanRegions() )
         this.warning( "Overscan correction has been enabled, but no overscan regions have been defined." );

      this.cleanUp();
   }

   // ----------------------------------------

   // Cosmetic correction check
   if ( this.hasLightFrames( WBPPGroupingMode.PRE ) )
      if ( this.cosmeticCorrection )
      {
         this.top();
         this.headerText( "Check Cosmetic Correction instance reference" );

         if ( isEmptyString( this.cosmeticCorrectionTemplateId ) )
            this.error( "No cosmetic correction template instance has been specified." );
         else
         {
            let CC = ProcessInstance.fromIcon( this.cosmeticCorrectionTemplateId );
            if ( CC == null )
               this.error( "No such process process icon: " + this.cosmeticCorrectionTemplateId );
            else
            {
               if ( !( CC instanceof CosmeticCorrection ) )
                  this.error( "The specified process icon does not transport an instance " +
                     "of CosmeticCorrection: " + this.cosmeticCorrectionTemplateId );
               else
               {
                  if ( !CC.useMasterDark && !CC.useAutoDetect && !CC.useDefectList )
                     this.warning( "The specified CosmeticCorrection instance does not define " +
                        "a valid correction operation: " + this.cosmeticCorrectionTemplateId );
               }
            }
         }

         this.cleanUp();
      }

   // ----------------------------------------

   // Reference frame
   if ( this.hasLightFrames( WBPPGroupingMode.POST ) )
   {
      this.top();
      this.headerText( "Check reference frame settings" );

      // best reference frame checks
      if ( this.imageRegistration )
      {
         if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
         {
            if ( isEmptyString( this.referenceImage ) )
               this.error( "No registration reference image has been specified." );
            else if ( !File.exists( this.referenceImage ) )
               this.error( "The specified registration reference file does not exist: " + this.referenceImage );
         }
         else
         {
            let keywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
            if ( keywords.length > 0 && this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
            {
               let keywordsList = keywords.map( k => k.name ).join( ", " );
               let kwDesc = keywords.length > 1 ? "s " + keywordsList + "s <b>" : " <b>" + keywordsList + "</b>";
               this.warning( "Master Light frames will be grouped using the keyword" + kwDesc + " and the same reference frame will be used to register all of them. " );
            }
         }
      }

      this.cleanUp();
   }
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic is empty.
 *
 * @returns
 */
StackEngine.prototype.hasDiagnosticMessages = function()
{
   return this.diagnosticMessages.length > 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic contains error messages.
 *
 * @returns
 */
StackEngine.prototype.hasErrorMessages = function()
{
   for ( let i = 0; i < this.diagnosticMessages.length; ++i )
      if ( this.diagnosticMessages[ i ].startsWith( "<b>Error" ) )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Removes all diagnostic messages.
 *
 */
StackEngine.prototype.clearDiagnosticMessages = function()
{
   this.diagnosticMessages = new Array;
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the diagnostic message Dialog.
 *
 * @param {[String]} messages the messages array to be displayed
 * @param {Boolean} cancelButton true if cancel button should be displayed
 */
function DiagnosticInformationDialog( messages, cancelButton )
{
   this.__base__ = Dialog;
   this.__base__();

   let info = "";
   for ( let i = 0; i < messages.length; ++i )
      info += messages[ i ] + '\n';

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "%d message(s):", messages.length );

   this.infoBox = new TextBox( this );
   this.infoBox.readOnly = true;
   this.infoBox.styleSheet = this.scaledStyleSheet( "QWidget { font-family: Hack, DejaVu Sans Mono, monospace; font-size: 10pt; }" );
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.text = info;

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = cancelButton ? "Continue" : "OK";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   if ( cancelButton )
   {
      this.cancelButton = new PushButton( this );
      this.cancelButton.defaultButton = true;
      this.cancelButton.text = "Cancel";
      this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
      this.cancelButton.onClick = function()
      {
         this.dialog.cancel();
      };
   }

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.okButton );
   if ( cancelButton )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.cancelButton );
   }

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Diagnostic Messages";
};

DiagnosticInformationDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
/**
 * Shows the diagnostic messages dialog, optinoally with a cancel button.
 *
 * @param {Boolean} cancelButton true if a cancel button needs to be shown
 * @returns StdDialogCode_Ok if OK button is pressed, StdDialogCode_Cancel
 *          dialog is closed or cancel button is pressed
 */
StackEngine.prototype.showDiagnosticMessages = function( cancelButton )
{
   if ( this.hasDiagnosticMessages() )
   {
      if ( this.hasErrorMessages() )
      {
         ( new DiagnosticInformationDialog( this.diagnosticMessages, false /*cancelButton*/ ) ).execute();
         return StdDialogCode_Cancel;
      }

      return ( new DiagnosticInformationDialog( this.diagnosticMessages, cancelButton ) ).execute();
   }

   ( new MessageBox( "There are no errors.", TITLE + " " + VERSION, StdIcon_Information, StdButton_Ok ) ).execute();
   return StdDialogCode_Ok;
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the Process Logger dialog.
 *
 * @param {ProcessLogger} processLogger the instance of the process logger containing the messages
 */
function ProcessLogDialog( processLogger )
{
   this.__base__ = Dialog;
   this.__base__();

   let info = processLogger.toString();

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "WBPP steps:" );

   this.infoBox = new TextBox( this );
   this.infoBox.useRichText = true;
   this.infoBox.readOnly = true;
   this.infoBox.styleSheet = this.scaledStyleSheet( "QWidget { font-family: Hack, DejaVu Sans Mono, monospace; font-size: 10pt; color: #0066ff; padding: 4px;" );
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.text = info;

   this.saveButton = new PushButton( this );
   this.saveButton.defaultButton = true;
   this.saveButton.text = "Save";
   this.saveButton.icon = this.scaledResource( ":/icons/save.png" );
   this.saveButton.onClick = () =>
   {
      // save content to a text file
      var save = new SaveFileDialog;
      save.caption = "Process Dialog Output File";
      save.initialPath = "ProcessLogger.txt";
      save.overwritePrompt = true;
      save.filters = [
         [ "*.txt", "*.*" ]
      ];

      if ( save.execute() )
         processLogger.writeToFile( save.fileName );
   };

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "DONE";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.saveButton );
   this.buttonsSizer.addScaledSpacing( 8 );
   this.buttonsSizer.add( this.okButton );

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Smart Report";
}

ProcessLogDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Shows the process logger dialog.
 */
StackEngine.prototype.showProcessLogs = function()
{
   let dialog = new ProcessLogDialog( this.processLogger );
   dialog.execute();
};

// ----------------------------------------------------------------------------

/**
 * Removes all messages from the process logger.
 */
StackEngine.prototype.cleanProcessLog = function()
{
   this.processLogger.clean();
};

// ----------------------------------------------------------------------------

/**
 * The constructor of the alert dialog shown the first time WBPP is launched to
 * peroform the integration of light frames.
 */
function IntegrationWarningDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   this.infoLabel = new Label( this );
   this.infoLabel.scaledMinWidth = 550;
   this.infoLabel.useRichText = true;
   this.infoLabel.wordWrapping = true;
   this.infoLabel.styleSheet = this.scaledStyleSheet( "QWidget { font-size: 10pt; }" );
   this.infoLabel.text =
      "<p>You have selected to perform an integration of light frames with this script.</p>" +
      "<p>Please keep in mind that the light frames integration functionality of this script is just " +
      "a convenience feature, which does not include the full functionality of the ImageInegration " +
      "process. Depending in the input data the resulting image may <i>not</i> produce an optimal " +
      "result. In some cases the integrated result of this script will be " +
      "rather poor, compared with the result that can be achieved by manually selecting and optimizing " +
      "image integration parameters.</p>" +
      "<p>Image integration is a critical task that requires fine-tuning. Our ImageIntegration tool " +
      "allows you to find optimal pixel rejection parameters to maximize signal-to-noise ratio with " +
      "the appropriate rejection of spurious image data. In general, this requires some trial-error " +
      "that can't be done automatically from this script.</p>";

   this.noMoreCheckBox = new CheckBox( this );
   this.noMoreCheckBox.text = "Got it, don't show this anymore.";

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "Continue";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.cancelButton = new PushButton( this );
   this.cancelButton.defaultButton = true;
   this.cancelButton.text = "Cancel";
   this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancelButton.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.add( this.noMoreCheckBox );
   this.buttonsSizer.addSpacing( 40 );
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.okButton );
   this.buttonsSizer.addSpacing( 8 );
   this.buttonsSizer.add( this.cancelButton );

   this.sizer = new VerticalSizer;
   this.sizer.margin = 16;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 32 );
   this.sizer.add( this.buttonsSizer );

   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Light Frames Integration Warning";
}

IntegrationWarningDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Shows the integration alert dialog.
 */
StackEngine.prototype.showIntegrationWarning = function()
{
   let show = Settings.read( SETTINGS_KEY_BASE + "showIntegrationWarning", DataType_Boolean );
   if ( show == null )
      show = true;
   if ( show )
   {
      let d = new IntegrationWarningDialog;
      let result = d.execute();
      if ( d.noMoreCheckBox.checked )
         Settings.write( SETTINGS_KEY_BASE + "showIntegrationWarning", DataType_Boolean, false );
      return result;
   }
   return true;
};

// ----------------------------------------------------------------------------
// FILE ITEM MANAGEMENT
// ----------------------------------------------------------------------------

/**
 *  Finds the group matching the proveded criterias
 *
 * @param {ImageType} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Boolean} isMaster
 * @param {Boolean} isCFA
 * @param {Numeric} darkExposureTolerance
 * @param {Numeric} lightExposureTolerance
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.findGroup = function( imageType, filter, binning, exposureTime, isMaster, isCFA, darkExposureTolerance, lightExposureTolerance, keywords, strictKeywordsMatching, mode )
{
   // NOTE: since there could be more than one group matching the same parameters but a different
   // number of keywords, we lopp through all groups and we collect all groups that matches.
   // If there is more than one matching group then we do a final loop with the results and
   // we select the group that has the highest number of matching keywords
   let groupIndx = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
   {
      // in case we're searching a group for a master frame then the tolerance is reduced to the maximum
      // precision in case the current group has a master too.
      // We do this because the tolerance has a meaning only when adding dark frames or matching
      // dark frame groups that contains only dark frames.
      let tolerance = isMaster ? CONST_MIN_EXPOSURE_TOLERANCE : darkExposureTolerance;
      if ( groups[ i ].sameParameters( imageType, filter, binning, exposureTime, isCFA, tolerance, lightExposureTolerance, mode ) )
         groupIndx.push( i );
   }
   // return the group that best matches the keywords
   let keywordsForMode = engine.keywords.filterKeywordsForMode( keywords, mode );
   let bestgroupIndex = this.bestGroupMatchingKeywordsIndex( groups, groupIndx, keywordsForMode, strictKeywordsMatching );
   return bestgroupIndex == -1 ? undefined : groups[ bestgroupIndex ];
};

// ----------------------------------------------------------------------------

/**
 * Implements the strategy to select the best candidate group acconrdingly to the
 * provided keywords. If more than one group is found that matches the same number of
 * keywords then the groups are sorted following the keywords order and the
 * first gets selected.
 *
 * @param {[FrameGroup]} groups
 * @param {Numeric} groupIndx indexes of candidate groups in groups array
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching if true keywords values must match exactly, including
 *                                         the keywords that have no values
 * @returns
 */
StackEngine.prototype.bestGroupMatchingKeywordsIndex = function( groups, groupIndx, keywords, strictKeywordsMatching )
{
   // return -1 if no group matches
   if ( groupIndx.length == 0 )
   {
      return -1;
   }

   // find the groups with the highest matching count
   let maxMatch = 0;
   let matchingIndexes = [];
   groupIndx.forEach( i =>
   {
      let matchCount = groups[ i ].keywordsMatchCount( keywords, strictKeywordsMatching )
      if ( matchCount > maxMatch )
      {
         maxMatch = matchCount;
         matchingIndexes = [ i ];
      }
      else if ( matchCount == maxMatch )
      {
         matchingIndexes.push( i );
      }
   } );

   if ( matchingIndexes.length == 0 )
   {
      // no matching groups
      return -1;
   }
   else if ( matchingIndexes.length == 1 )
   {
      // one matching group
      return matchingIndexes[ 0 ];
   }
   else
   {
      // initialize an array with matching groups and index
      let groupsWithIndex = groups.map( ( group, index ) => (
      {
         priority: 0,
         keywords: Object.keys( group.keywords ),
         group: group
      } ) )
      // update the index of a group
      engine.keywords.names().forEach( ( name, index ) =>
      {
         groupsWithIndex.forEach( ( g ) =>
         {
            if ( g.keywords.indexOf( name ) != -1 )
            {
               group.priority += 1 << ( index + 1 );
            }
         } )
      } )
      // return the group with the lowest index
      groupsWithIndex.sort( ( a, b ) => a.priority - b.priority );
      return groupsWithIndex[ 0 ];
   }

   return maxIndx;
};

// ----------------------------------------------------------------------------

/**
 * Performs a sanity check on the file at the given filePath.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.checkFile = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // path must not be an empty string
   if ( isEmptyString( filePath ) )
      return {
         success: false,
         message: "Empty file path"
      };

   // file must exist
   if ( !File.exists( filePath ) )
      return {
         success: false,
         message: "File not found: " + filePath
      }

   // file must not be already added. By default the grouping mode WBPPGoupingMode = .pre is used for this check
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
         if ( groups[ i ].fileItems[ j ].filePath == filePath )
            return {
               success: false,
               message: "File " + filePath + " has already been added as " + StackEngine.imageTypeToString( groups[ i ].imageType ) + " frame"
            }

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------

/**
 * Adds a new file with the given properties.
 *
 * @param {String} filePath
 * @param {Imagetype} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Boolean} overrideCFA
 * @param {*} customModes
 * @returns
 */
StackEngine.prototype.addFile = function( filePath, imageType, filter, binning, exposureTime, overrideCFA, customModes )
{
   let
   {
      smartNaming
   } = WBPPUtils.shared();

   filePath = filePath.trim();

   let checkResult = this.checkFile( filePath );
   if ( !checkResult.success )
      return checkResult;

   let forcedType = imageType != undefined && imageType != ImageType.UNKNOWN;
   if ( !forcedType )
      imageType = ImageType.UNKNOWN;

   if ( filter == "?" )
      filter = undefined;
   let forcedFilter = filter != undefined && filter != "?"; // ### see Add Custom Frames dialog

   let forcedBinning = binning != undefined && binning > 0;
   if ( !forcedBinning )
      binning = 0;

   let forcedExposureTime = imageType == ImageType.BIAS || exposureTime != undefined && exposureTime > 0;
   if ( !forcedExposureTime || imageType == ImageType.BIAS )
      exposureTime = 0;

   // assume image is NOT CFA unless the bayer pattern is found in the header
   let isCFA = false;

   // initially assume that the file is not a master file
   let isMaster = false;

   let ext = File.extractExtension( filePath ).toLowerCase();
   let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
   if ( F.isNull ) // shouldn't happen
      return {
         success: false,
         message: "No installed file format can read \'" + ext + "\' files."
      };
   let f = new FileFormatInstance( F );
   if ( f.isNull )
      return {
         success: false,
         message: "Unable to instantiate file format: " + F.name
      };

   let info = f.open( filePath, "verbosity 0" ); // do not fill the console with useless messages
   if ( !info || ( info && info.length <= 0 ) )
      return {
         success: false,
         message: "Unable to open input file: " + filePath
      };

   let keywords = [];
   if ( F.canStoreKeywords )
      keywords = f.keywords;

   f.close();

   // initialize the keywords extracting them from the path
   let itemKeywords = {};

   for ( let i = 0; i < keywords.length; ++i )
   {
      let value = keywords[ i ].strippedValue.trim();
      let name = keywords[ i ].name;
      if ( name === "HISTORY" ) continue;

      switch ( name )
      {
         case "IMAGETYP":
            if ( !forcedType )
               imageType = StackEngine.imageTypeFromKeyword( value );
            isMaster = StackEngine.isMasterFromKeyword( value );
            break;
         case "FILTER":
         case "INSFLNAM":
            if ( !forcedFilter )
               filter = value;
            break;
         case "XBINNING":
         case "BINNING":
         case "CCDBINX":
            if ( !forcedBinning )
               binning = parseInt( value );
            break;
         case "EXPTIME":
         case "EXPOSURE":
            if ( !forcedExposureTime && imageType != ImageType.BIAS )
               exposureTime = parseFloat( value );
            break;
         case "BAYERPAT":
            isCFA = true;
            break;
      }

      // get keywords from FITS Header
      if ( engine.keywords.names().indexOf( name ) != -1 )
      {
         itemKeywords[ name ] = value;
      }
   }

   // override all keywords values from the file path
   engine.keywords.names().forEach( keywordName =>
   {
      let value = smartNaming.getCustomKeyValueFromPath( keywordName, filePath );
      if ( value )
         itemKeywords[ keywordName ] = value;
   } );

   if ( !forcedExposureTime )
      if ( exposureTime <= 0 )
         if ( typeof( info[ 0 ].exposure ) == "number" )
            if ( info[ 0 ].exposure > 0 )
               exposureTime = info[ 0 ].exposure;

   // smart naming: extract type binning, filter and duration from filePath if needed
   if ( imageType == ImageType.UNKNOWN )
      imageType = smartNaming.geImageTypeFromPath( filePath );

   if ( imageType == ImageType.UNKNOWN )
   {
      this.diagnosticMessages.push( "Unable to determine frame type: " + filePath );
      return {
         success: false,
         message: "Unable to determine frame type of file <" + filePath + ">"
      };
   }

   if ( !forcedBinning && binning == 0 )
      binning = smartNaming.getBinningFromPath( filePath );
   if ( !forcedFilter && filter == undefined )
      filter = smartNaming.getFilterFromPath( filePath ) || "NoFilter";
   if ( !forcedExposureTime && imageType !== ImageType.BIAS && exposureTime == 0 )
      exposureTime = smartNaming.getExposureTimeFromPath( filePath );

   // if master was not found in the FITS header then check if file is a master from the filePath
   if ( !isMaster )
   {
      let searchPath = this.detectMasterIncludingFullPath ? filePath : File.extractName( filePath );
      isMaster = smartNaming.isMasterFromPath( searchPath );
   }

   // check for CFA override
   if ( overrideCFA !== undefined )
      isCFA = overrideCFA;
   
   // light frames are never used as masters
   isMaster = isMaster && ( imageType != ImageType.LIGHT );

   let item = new FileItem(
      filePath,
      imageType,
      filter,
      binning,
      exposureTime,
      isCFA,
      isMaster,
      itemKeywords
   );

   this.groupsManager.addFileItem( item, undefined /* custom keywords */, customModes );

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------

/**
 * Adds a bias Frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addBiasFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.BIAS );
};

// ----------------------------------------------------------------------------

/**
 * Adds a dark frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addDarkFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.DARK );
};

// ----------------------------------------------------------------------------

/**
 * Adds a flat frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addFlatFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.FLAT );
};

// ----------------------------------------------------------------------------

/**
 * Adds a light frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addLightFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.LIGHT );
};

// ----------------------------------------------------------------------------

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconsructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 */
StackEngine.prototype.reconstructGroups = function()
{
   // clean up null values
   this.removePurgedElements();

   // save the current group properties to be restored for the unchanged groups
   this.groupsManager.cacheGroupsProperties();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = [];
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flatten files
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
         fileItems.push( groups[ i ].fileItems[ j ] );

   // remove all groups
   this.groupsManager.clear();

   // re-add files one by one
   for ( let i = 0; i < fileItems.length; ++i )
      if ( fileItems[ i ] )
      {
         let fileItem = fileItems[ i ];
         this.addFile( 
            fileItem.filePath, 
            fileItem.imageType, 
            fileItem.filter, 
            fileItem.binning, 
            fileItem.exposureTime,
            undefined, /* override CFA */
            [WBPPGroupingMode.PRE] /* customModes */
         );
      }

   // ready to restore the group properties
   this.groupsManager.restoreGroupsPropertiesFromCache();

   // reconstruct post process groups after PRE has been completed
   this.reconstructPostProcessGroups();
};

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconsructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 */
 StackEngine.prototype.reconstructPostProcessGroups = function()
 {
    // clean up null values
   this.removePurgedElements();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = [];
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   
   // flatten files and store the overridden CFA property
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.LIGHT )         {
         for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
            fileItems.push( { 
               fileItem: groups[ i ].fileItems[ j ],
               isCFA: groups[ i ].isCFA
            } );
         }
  
   // purge post-process groups
   let postGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );
   postGroups.forEach(g => {
      g.__purged__ = true;
   });
   this.removePurgedElements();

   // re-add files one by one
   for ( let i = 0; i < fileItems.length; ++i )
   {
      let fileItem = fileItems[ i ].fileItem;
      if ( fileItem )
      {
         let isCFA = fileItems[ i ].isCFA;
         this.groupsManager.addFileItem(
            fileItem, 
            undefined /* custom keywords */, 
            [WBPPGroupingMode.POST], 
            isCFA /* custom CFA */
         );
      }
   }
 };

// ----------------------------------------------------------------------------

/**
 * Apply the provided CFA settings to the pre-processing groups.
 *
 * @param {ImageType} imageType
 * @param {Boolean} isCFA
 * @param {Debayer.prototype} CFAPattern
 * @param {Debayer.prototype} debayerMethod
 * @param {Boolean} separateCFAFlatScalingFactors
 */
StackEngine.prototype.applyCFASettings = function( imageType, isCFA, CFAPattern, debayerMethod, separateCFAFlatScalingFactors )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         groups[ i ].isCFA = isCFA;
         groups[ i ].CFAPattern = CFAPattern;
         groups[ i ].debayerMethod = debayerMethod;
         if ( imageType == ImageType.FLAT )
            groups[ i ].separateCFAFlatScalingFactors = separateCFAFlatScalingFactors;
      }
};

// ----------------------------------------------------------------------------

/**
 * Provides default input file hints.
 *
 * @returns
 */
StackEngine.prototype.inputHints = function()
{
   // Input format hints:
   // * XISF: fits-keywords normalize
   // * FITS: signed-is-physical use-roworder-keywords up-bottom|bottom-up
   // * DSLR_RAW: raw cfa
   return "fits-keywords normalize raw cfa use-roworder-keywords signed-is-physical " + ( this.upBottomFITS ? "up-bottom" : "bottom-up" );
};

// ----------------------------------------------------------------------------

/**
 * Provides default output file hints.
 *
 * @returns
 */
StackEngine.prototype.outputHints = function()
{
   // Output format hints:
   // * XISF: properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution
   // * FITS: up-bottom|bottom-up
   return "properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution " +
      ( this.upBottomFITS ? "up-bottom" : "bottom-up" );
};

// ----------------------------------------------------------------------------

/**
 * Opens the image at filePath and returns the generated Window object.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.readImage = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   let ext = File.extractExtension( filePath );
   let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can read \'" + ext + "\' files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   let d = f.open( filePath, this.inputHints() );
   if ( d.length < 1 )
      throw new Error( "Unable to open file: " + filePath );
   if ( d.length > 1 )
      throw new Error( "Multi-image files are not supported by this script: " + filePath );

   let window = new ImageWindow( 1, 1, 1, /*numberOfChannels*/ 32, /*bitsPerSample*/ true /*floatSample*/ );

   let view = window.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );

   if ( !f.readImage( view.image ) )
      throw new Error( "Unable to read file: " + filePath );

   if ( F.canStoreImageProperties )
      if ( F.supportsViewProperties )
      {
         let info = view.importProperties( f );
         if ( !isEmptyString( info ) )
            console.criticalln( "<end><cbr>*** Error reading image properties:\n", info );
      }

   if ( F.canStoreKeywords )
      window.keywords = f.keywords;

   view.endProcess();

   f.close();

   return window;
};

// ----------------------------------------------------------------------------

/**
 * Writes an iamge to file.
 *
 * @param {String} filePath
 * @param {ImageWindow} imageWindow
 * @param {ImageWindow} rejectionLowWindow
 * @param {ImageWindow} rejectionHighWindow
 * @param {ImageWindow} slopeMapWindow
 * @param {Boolean} imageIdentifiers true to assign default image identifiers
 */
StackEngine.prototype.writeImage = function( filePath,
   imageWindow, rejectionLowWindow, rejectionHighWindow, slopeMapWindow, imageIdentifiers )
{
   let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can write " + ".xisf" + " files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   if ( !f.create( filePath, this.outputHints() ) )
      throw new Error( "Error creating output file: " + filePath );

   let d = new ImageDescription;
   d.bitsPerSample = 32;
   d.ieeefpSampleFormat = true;
   if ( !f.setOptions( d ) )
      throw new Error( "Unable to set output file options: " + filePath );

   if ( imageIdentifiers )
      f.setImageId( "integration" );

   if ( F.canStoreImageProperties )
      if ( F.supportsViewProperties )
         imageWindow.mainView.exportProperties( f );

   if ( F.canStoreKeywords )
      f.keywords = imageWindow.keywords;

   if ( !f.writeImage( imageWindow.mainView.image ) )
      throw new Error( "Error writing output file: " + filePath );

   if ( rejectionLowWindow && !rejectionLowWindow.isNull )
   {
      if ( imageIdentifiers )
         f.setImageId( "rejection_low" );
      f.keywords = rejectionLowWindow.keywords;
      if ( !f.writeImage( rejectionLowWindow.mainView.image ) )
         throw new Error( "Error writing output file (low rejection map): " + filePath );
   }

   if ( rejectionHighWindow && !rejectionHighWindow.isNull )
   {
      if ( imageIdentifiers )
         f.setImageId( "rejection_high" );
      f.keywords = rejectionHighWindow.keywords;
      if ( !f.writeImage( rejectionHighWindow.mainView.image ) )
         throw new Error( "Error writing output file (high rejection map): " + filePath );
   }

   if ( slopeMapWindow && !slopeMapWindow.isNull )
   {
      if ( imageIdentifiers )
         f.setImageId( "slope_map" );
      f.keywords = slopeMapWindow.keywords;
      if ( !f.writeImage( slopeMapWindow.mainView.image ) )
         throw new Error( "Error writing output file (slope map): " + filePath );
   }

   f.close();
};

// ----------------------------------------------------------------------------

StackEngine.rejectionMethods = [
{
   name: "No rejection",
   rejection: ImageIntegration.prototype.NoRejection
},
{
   name: "Min/Max",
   rejection: ImageIntegration.prototype.MinMax
},
{
   name: "Percentile Clipping",
   rejection: ImageIntegration.prototype.PercentileClip
},
{
   name: "Sigma Clipping",
   rejection: ImageIntegration.prototype.SigmaClip
},
{
   name: "Winsorized Sigma Clipping",
   rejection: ImageIntegration.prototype.WinsorizedSigmaClip
},
{
   name: "Averaged Sigma Clipping",
   rejection: ImageIntegration.prototype.AveragedSigmaClip
},
{
   name: "Linear Fit Clipping",
   rejection: ImageIntegration.prototype.LinearFit
},
{
   name: "Generalized Extreme Studentized Deviate",
   rejection: ImageIntegration.prototype.Rejection_ESD
},
{
   name: "Auto",
   rejection: ImageIntegration.prototype.auto
} ];

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionNames = function()
{
   return StackEngine.rejectionMethods.map( item => item.name );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionFromIndex = function( index )
{
   return StackEngine.rejectionMethods[ index ].rejection;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionName = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return StackEngine.rejectionMethods[ i ].name;
   return StackEngine.rejectionMethods[ StackEngine.rejectionMethods.length - 1 ].name;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionIndex = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return i;
   return StackEngine.rejectionMethods.length - 1;
};

// ----------------------------------------------------------------------------

/*
 * Most of the following routines are simplifications of some functions that
 * are implemented in the SubframeSelector script.
 */

function noiseOfImage( image )
{
   for ( let layer = 4; layer != 1; --layer )
   {
      let estimate = image.noiseMRS( layer );
      if ( estimate[ 1 ] >= 0.01 * image.bounds.area )
         return [ estimate[ 0 ], estimate[ 1 ] / image.bounds.area ];
   }
   console.writeln( "" );
   console.writeln( "** Warning: No convergence in MRS noise evaluation routine: ",
      "using k-sigma noise estimate" );
   let estimate = image.noiseKSigma();
   return [ estimate[ 0 ], estimate[ 1 ] / image.bounds.area ];
}

// ----------------------------------------------------------------------------

function numberCompare( a, b )
{
   return ( a < b ) ? -1 : ( ( a > b ) ? 1 : 0 );
}

// ----------------------------------------------------------------------------

function medianOfArray( values )
{
   if ( values.length == 0 )
      return [ 0.0, 0.0 ];

   values.sort( numberCompare );

   let i = Math.floor( 0.5 * values.length );
   let median = ( 2 * i == values.length ) ?
      0.5 * ( values[ i - 1 ] + values[ i ] ) :
      values[ i ];

   return median;
}

// ----------------------------------------------------------------------------

function starDescription( b, a, x, y, sx, sy, theta, residual )
{
   this.b = b;
   this.a = a;
   this.x = x;
   this.y = y;
   this.sx = sx;
   this.sy = sy;
   this.theta = theta;
   this.residual = residual;
}

// ----------------------------------------------------------------------------

function starDescriptionCompare( a, b )
{
   let ax = Math.round( a.x );
   let ay = Math.round( a.y );
   let bx = Math.round( b.x );
   let by = Math.round( b.y );
   return ( ax < bx ) ? -1 : ( ( ax > bx ) ? 1 : ( ( ay < by ) ? -1 : ( ( ay > by ) ? 1 : 0 ) ) );
}

// ----------------------------------------------------------------------------

function uniqueArray( values, compareFunction )
{
   if ( values.length < 2 )
      return values;

   values.sort( compareFunction );
   let j = 0;
   for ( let i = 1; i != values.length; ++i )
      if ( compareFunction( values[ j ], values[ i ] ) == -1 )
      {
         ++j;
         values[ j ] = values[ i ];
      }
   return values.slice( 0, j + 1 );
}

// ----------------------------------------------------------------------------

StackEngine.prototype.ImageDescriptor = function( imageWindow, filePath )
{
   let FWHM = null;
   let eccentricity = null;
   let noise = null;
   let SNRWeight = null;
   let stars = null;
   let numberOfStars

   let keywords = imageWindow.keywords;
   // 0. read the measurements from the FITS header, if present
   for ( let i = 0; i < keywords.length; ++i )
   {
      let value = keywords[ i ].strippedValue.trim();

      if ( keywords[ i ].name == CONST_MEASUREMENT_FWHM )
         FWHM = parseFloat( value );
      else if ( keywords[ i ].name == CONST_MEASUREMENT_ECCENTRICITY )
         eccentricity = parseFloat( value );
      else if ( keywords[ i ].name == CONST_MEASUREMENT_NOISE )
         noise = parseFloat( value );
      else if ( keywords[ i ].name == CONST_MEASUREMENT_SNRWEIGHT )
         SNRWeight = parseFloat( value );
      else if ( keywords[ i ].name == CONST_MEASUREMENT_STARS )
         stars = parseInt( value );
   }

   // if at least one measurement is missing then measure again the image
   if ( this.forceMeasurement || FWHM == null || eccentricity == null || noise == null || SNRWeight == null || stars == null )
   {
      // 1. find stars
      console.writeln();
      console.writeln( "<b>StarDetector:</b> Processing view: ", imageWindow.mainView.fullId );
      console.flush();

      let barycenters = new Array;
      let maximumDetectedStars = 100000;

      // some default values
      let starDetector = new StarDetector();
      starDetector.structureLayers = 4;
      starDetector.noiseLayers = 2;
      starDetector.hotPixelFilterRadius = 1;
      starDetector.applyHotPixelFilterToDetectionImage = 0;
      starDetector.sensitivity = -1;
      starDetector.peakResponse = 0.8;
      starDetector.maxDistortion = 0.5;
      starDetector.upperLimit = 1.0;

      stars = starDetector.stars( imageWindow.mainView.image );
      numberOfStars = stars.length;

      console.writeln( numberOfStars + " star(s) found" );
      console.flush();

      let threshold = 1.0;
      for ( ;; )
      {
         for ( let i = 0; i != stars.length && barycenters.length != maximumDetectedStars; ++i )
            if ( threshold == 1.0 || Math.random() <= threshold )
               barycenters.push(
               {
                  position: stars[ i ].pos,
                  radius: Math.max( 3, Math.ceil( Math.sqrt( stars[ i ].size ) ) )
               } );
         if ( barycenters.length != maximumDetectedStars )
            break;
         barycenters = new Array;
         threshold = 0.1 * threshold;
      }

      let maximumFittedStars = 20000;
      if ( barycenters.length > maximumFittedStars )
      {
         threshold = 0.1 * threshold;
         for ( let i = barycenters.length - 1; i != 0; --i )
         {
            let j = Math.round( Math.random() * i );
            let x = barycenters[ i ];
            barycenters[ i ] = barycenters[ j ];
            barycenters[ j ] = x;
         }
         barycenters = barycenters.slice( 0, maximumFittedStars );
      }

      let dynamicPSF = new DynamicPSF;
      dynamicPSF.autoPSF = false;
      dynamicPSF.circularPSF = false;
      dynamicPSF.gaussianPSF = false;
      dynamicPSF.moffatPSF = false;
      dynamicPSF.moffat10PSF = false;
      dynamicPSF.moffat8PSF = false;
      dynamicPSF.moffat6PSF = false;
      dynamicPSF.moffat4PSF = true;
      dynamicPSF.moffat25PSF = false;
      dynamicPSF.moffat15PSF = false;
      dynamicPSF.lorentzianPSF = false;
      dynamicPSF.regenerate = true;

      let views = new Array;
      views.push( new Array( imageWindow.mainView.id ) );
      dynamicPSF.views = views;

      stars = new Array;
      for ( let i = 0; i != barycenters.length; ++i )
      {
         stars.push( new Array(
            0, 0, DynamicPSF.prototype.Star_DetectedOk,
            barycenters[ i ].position.x - barycenters[ i ].radius,
            barycenters[ i ].position.y - barycenters[ i ].radius,
            barycenters[ i ].position.x + barycenters[ i ].radius,
            barycenters[ i ].position.y + barycenters[ i ].radius,
            barycenters[ i ].position.x,
            barycenters[ i ].position.y
         ) );
      }
      dynamicPSF.stars = stars;
      let fitted = new Array( stars.length );
      for ( let i = 0; i != fitted.length; ++i )
         fitted[ i ] = false;

      // Workaround for DynamicPSF failure on images with clipped backgrounds
      // ### TODO: Release an update to DynamicPSF with this problem fixed.
      if ( imageWindow.mainView.image.median() != 0 )
         dynamicPSF.executeGlobal();
      else
         console.writeln( "0 PSF fittings" );

      console.abortEnabled = true;

      const DYNAMICPSF_PSF_StarIndex = 0;
      const DYNAMICPSF_PSF_Status = 3;
      const DYNAMICPSF_PSF_b = 4;
      const DYNAMICPSF_PSF_a = 5;
      const DYNAMICPSF_PSF_cx = 6;
      const DYNAMICPSF_PSF_cy = 7;
      const DYNAMICPSF_PSF_sx = 8;
      const DYNAMICPSF_PSF_sy = 9;
      const DYNAMICPSF_PSF_theta = 10;
      const DYNAMICPSF_PSF_mad = 12;
      const maxPSFMAD = 0.1;

      let starDescriptions = new Array;
      let psfTable = dynamicPSF.psf;
      let starsTable = dynamicPSF.stars;
      for ( let i = 0; i != psfTable.length; ++i )
      {
         let psfRow = psfTable[ i ];
         if ( psfRow[ DYNAMICPSF_PSF_Status ] == DynamicPSF.prototype.PSF_FittedOk &&
            psfRow[ DYNAMICPSF_PSF_mad ] < maxPSFMAD &&
            !fitted[ psfRow[ DYNAMICPSF_PSF_StarIndex ] ] )
         {
            let starsRow = starsTable[ psfRow[ DYNAMICPSF_PSF_StarIndex ] ];
            starDescriptions.push( new starDescription(
               psfRow[ DYNAMICPSF_PSF_b ],
               psfRow[ DYNAMICPSF_PSF_a ],
               psfRow[ DYNAMICPSF_PSF_cx ],
               psfRow[ DYNAMICPSF_PSF_cy ],
               psfRow[ DYNAMICPSF_PSF_sx ],
               psfRow[ DYNAMICPSF_PSF_sy ],
               psfRow[ DYNAMICPSF_PSF_theta ],
               psfRow[ DYNAMICPSF_PSF_mad ]
            ) );
            fitted[ psfRow[ DYNAMICPSF_PSF_StarIndex ] ] = true;
         }
      }
      starDescriptions = uniqueArray( starDescriptions, starDescriptionCompare );

      let FWHMs = [];
      let eccentricities = [];
      let residuals = [];
      for ( let i = 0; i != starDescriptions.length; ++i )
      {
         let description = starDescriptions[ i ];
         FWHMs.push( Math.sqrt( description.sx * description.sy ) );
         eccentricities.push(
            Math.sqrt( 1.0 - Math.pow( description.sy / description.sx, 2.0 ) )
         );
         residuals.push( description.residual );
      }

      let modelScaleFactor = 2.0 * Math.sqrt( Math.pow( 2.0, 1.0 / 4.0 ) - 1.0 );

      FWHM = modelScaleFactor * medianOfArray( FWHMs );
      eccentricity = medianOfArray( eccentricities );
      let dispersion = imageWindow.mainView.image.avgDev();
      let imageNoise = noiseOfImage( imageWindow.mainView.image );
      noise = imageNoise[ 0 ];
      SNRWeight = noise != 0 ? Math.pow( dispersion, 2.0 ) / Math.pow( noise, 2.0 ) : 0;
   }
   else
   {
      console.noteln( "<b>Subframe measurement values from FITS header</b>" );
      console.flush();
   }

   return {
      filePath: filePath,
      FWHM: FWHM,
      eccentricity: eccentricity,
      SNR: SNRWeight,
      noise: noise,
      numberOfStars: numberOfStars,
      failed: isNaN( FWHM ) || isNaN( eccentricity ) || isNaN( SNRWeight )
   };
};

// ----------------------------------------------------------------------------

/*
 * min/max values ot FWHM, eccentricity and SNR are computed.
 * These values will be used to compute the final weights of the light images.
 */
StackEngine.prototype.getMinMaxDescriptorsValues = function( imagesDescriptors )
{
   let FWHM = imagesDescriptors.map( descriptor => descriptor.FWHM );
   let eccentricity = imagesDescriptors.map( descriptor => descriptor.eccentricity );
   let SNR = imagesDescriptors.map( descriptor => descriptor.SNR );
   let noise = imagesDescriptors.map( descriptor => descriptor.noise );

   let FWHM_min = Math.min.apply( null, FWHM );
   let FWHM_max = Math.max.apply( null, FWHM );
   let eccentricity_min = Math.min.apply( null, eccentricity );
   let eccentricity_max = Math.max.apply( null, eccentricity );
   let SNR_min = Math.min.apply( null, SNR );
   let SNR_max = Math.max.apply( null, SNR );
   let noise_min = Math.min.apply( null, noise );
   let noise_max = Math.max.apply( null, noise );

   return {
      FWHM_min: FWHM_min,
      FWHM_max: FWHM_max,
      eccentricity_min: eccentricity_min,
      eccentricity_max: eccentricity_max,
      SNR_min: SNR_min,
      SNR_max: SNR_max,
      noise_min: noise_min,
      noise_max: noise_max
   };
};

// ----------------------------------------------------------------------------

/*
 * Find the best frame as reference for registration across all images
 */
StackEngine.prototype.findRegistrationReferenceImage = function( groups )
{
   // extract descriptors for all active frames (double check that a descriptor exists)
   let fileItems = groups.reduce( ( acc, g ) =>
      {
         return acc.concat( g.activeFrames() );
      },
      [] ).filter( fileItem => fileItem.descriptor != undefined )
   let binningForRegistration = fileItems.reduce((acc, fileItem) => {
      return Math.min(acc, fileItem.binning);
   }, 256);
   fileItems = fileItems.filter(item => (item.binning == binningForRegistration));
   let descriptors = fileItems.map( fileItem => fileItem.descriptor )
   let maxVal = 0;
   let bestFrame = undefined;
 
   // compute the absolute mix/max ranges
   let flatDescriptorsMinMax = this.getMinMaxDescriptorsValues( descriptors );
 
   // compute all images weight and find the best
   for ( let i = 0; i < fileItems.length; ++i )
   {
      let weight = this.computeWeightForLight( fileItems[ i ].descriptor, flatDescriptorsMinMax, 50, 50, 0, 1 );
      if ( weight && isFinite( weight ) )
         if ( weight > maxVal )
         {
            maxVal = weight;
            bestFrame = fileItems[ i ];
         }
   }

   // check in case light images are all the same
   return bestFrame ? bestFrame.current : undefined;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.computeWeightForLight = function( descriptor, descriptorMinMax, FWHMWeight, eccentricityWeight, SNRWeight, pedestal )
{
   if ( descriptor == undefined )
      return undefined;

   let FWHM = descriptor.FWHM;
   let FWHM_min = descriptorMinMax.FWHM_min;
   let FWHM_max = descriptorMinMax.FWHM_max;
   let eccentricity = descriptor.eccentricity;
   let eccentricity_min = descriptorMinMax.eccentricity_min;
   let eccentricity_max = descriptorMinMax.eccentricity_max;
   let SNR = descriptor.SNR;
   let SNR_min = descriptorMinMax.SNR_min;
   let SNR_max = descriptorMinMax.SNR_max;
   let noise = descriptor.noise;
   let noise_min = descriptorMinMax.noise_min;
   let noise_max = descriptorMinMax.noise_max;

   let a = FWHM_max - FWHM_min == 0 ? 0 : 1 - ( FWHM - FWHM_min ) / ( FWHM_max - FWHM_min );
   let b = eccentricity_max - eccentricity_min == 0 ? 0 : 1 - ( eccentricity - eccentricity_min ) / ( eccentricity_max - eccentricity_min );
   let c = SNR_max - SNR_min == 0 ? 0 : ( SNR - SNR_min ) / ( SNR_max - SNR_min );
   let weight = pedestal + a * FWHMWeight + b * eccentricityWeight + c * SNRWeight;
   console.noteln( 'Weights of image: ', descriptor.filePath );
   console.noteln( "-----------------------" );
   console.noteln( 'FWHM         : ', isFinite( a ) ? format( "%.02f %%", a * 100 ) : '-' );
   console.noteln( 'eccentricity : ', isFinite( b ) ? format( "%.02f %%", b * 100 ) : '-' );
   console.noteln( 'SNR          : ', isFinite( c ) ? format( "%.02f %%", c * 100 ) : '-' );
   console.noteln( 'Image weight : ', isFinite( weight ) ? format( "%.02f", weight ) : '-' );
   console.noteln( "-----------------------" );
   console.flush();
   return weight;
};

// ----------------------------------------------------------------------------

/**
 * Writes the weights for each provided groups,
 *
 * @param {*} groups
 */
StackEngine.prototype.writeWeightsWithDescriptors = function( groups )
{
   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* Embed measurements and weights into FITS headers" );
   console.noteln( "************************************************************" );
   console.flush();

   // compute weights for all groups
   for ( let i = 0; i < groups.length; ++i )
   {
      let fileItems = groups[ i ].activeFrames();
      let descriptors = fileItems.map( fileItem => fileItem.descriptor ).filter( Boolean );
      let descriptorMinMax = this.getMinMaxDescriptorsValues( descriptors );

      for ( let j = 0; j < fileItems.length; ++j )
      {
         processEvents();
         gc();

         let descriptor = fileItems[ j ].descriptor;
         if ( descriptor )
         {
            let imageWindow = this.readImage( fileItems[ j ].current, "" );
            if ( imageWindow === null )
            {
               console.warningln( "** Warning: Unable to open file to write weight: " + fileItems[ j ].current + ", light frame will be discarded." );
               this.processLogger.addWarning( "Unable to open file to write weight: " + fileItems[ j ].current + ", light frame will be discarded." );
               // can't read the file, discard it
               fileItems[ j ].processingFailed();
            }
            else
            {

               let weight = this.computeWeightForLight( descriptor, descriptorMinMax, this.FWHMWeight, this.eccentricityWeight, this.SNRWeight, this.pedestal );
               if ( isFinite( weight ) )
               {
                  imageWindow.keywords = imageWindow.keywords.filter( keyword =>
                  {
                     return keyword.name != CONST_MEASUREMENT_FWHM &&
                        keyword.name != CONST_MEASUREMENT_ECCENTRICITY &&
                        keyword.name != CONST_MEASUREMENT_NOISE &&
                        keyword.name != CONST_MEASUREMENT_SNRWEIGHT &&
                        keyword.name != CONST_MEASUREMENT_STARS &&
                        keyword.name != WEIGHT_KEYWORD;
                  } ).concat(
                     new FITSKeyword(
                        CONST_MEASUREMENT_FWHM,
                        format( "%.5e", descriptor.FWHM ).replace( "e", "E" ),
                        "WBPP Measurement: FWHM"
                     ) ).concat(
                     new FITSKeyword(
                        CONST_MEASUREMENT_ECCENTRICITY,
                        format( "%.5e", descriptor.eccentricity ).replace( "e", "E" ),
                        "WBPP Measurement: eccentricity"
                     ) ).concat(
                     new FITSKeyword(
                        CONST_MEASUREMENT_NOISE,
                        format( "%.5e", descriptor.noise ).replace( "e", "E" ),
                        "WBPP Measurement: noise"
                     ) ).concat(
                     new FITSKeyword(
                        CONST_MEASUREMENT_SNRWEIGHT,
                        format( "%.5e", descriptor.SNR ).replace( "e", "E" ),
                        "WBPP Measurement: SNR Weight"
                     ) ).concat(
                     new FITSKeyword(
                        CONST_MEASUREMENT_STARS,
                        format( "%i", descriptor.numberOfStars ),
                        "WBPP Measurement: SNR Weight"
                     ) ).concat(
                     new FITSKeyword(
                        WEIGHT_KEYWORD,
                        format( "%.3e", weight ).replace( "e", "E" ),
                        "Subframe weight"
                     ) );
                  console.noteln( "Writing measurements and weight: " + fileItems[ j ].current );
                  imageWindow.saveAs( fileItems[ j ].current, false, false, false, false );
                  imageWindow.forceClose();
               }
               else
               {
                  console.warningln( "** Warning: Unable to open file to write weight: " + fileItems[ j ].current + ", light frame will be discarded." );
                  this.processLogger.addWarning( "Unable to open file to write weight: " + fileItems[ j ].current + ", light frame will be discarded." );
                  // can't read the file, discard it
                  fileItems[ j ].processingFailed();
               }
            }
         }
         else
         {
            console.warningln( "** Warning: Measurment not found for image: " + fileItems[ j ].current + ", light frame will be discarded." );
            this.processLogger.addWarning( "Measurment not found for image: " + fileItems[ j ].current + ", light frame will be discarded." );
            // can't find the descriptor for the file item
            fileItems[ j ].processingFailed();
         }
      }
   }

   this.processLogger.addMessage( "Embedding measurements and weight completed." );
   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* End embedding measurements and weight into FITS headers" );
   console.noteln( "************************************************************" );
   console.flush();
};

// ----------------------------------------------------------------------------

/**
 * Measure the provided file items and store the measurements into each
 * measured item. The measure is performed by means of the SubframeSelector
 * Process.
 *
 * @param {[FileItem]} fileItems
 * @returns the number of frames successfully measured
 */
StackEngine.prototype.computeDescriptors = function( fileItems )
{
   let failed = 0;

   if ( this.useSubframeSelectorProcess )
   {
      /*
       * ### N.B. Experimental use of the SubframeSelectorProcess
       */
      let
      {
         enableTargetFrames,
      } = WBPPUtils.shared();

      let subframes = fileItems.map( item => item.current );

      var SS = new SubframeSelector;
      SS.routine = SubframeSelector.prototype.MeasureSubframes;
      SS.nonInteractive = true; // ### since core version 1.8.8-8
      SS.subframes = enableTargetFrames( subframes, 2 );
      SS.cameraResolution = SubframeSelector.prototype.Bits16;
      SS.scaleUnit = SubframeSelector.prototype.ArcSeconds;
      SS.dataUnit = SubframeSelector.prototype.DataNumber;

      SS.executeGlobal();

      // NB: fixed indexes that need to be aligned with the process implementation
      let iIndex = 0;
      let iFWHM = 5;
      let iEccentricity = 6;
      let iSNRWeight = 7;
      let iNoise = 10;
      let iStars = 12;

      // create a table of (initially invalid) descriptors
      let descriptors = [];
      for ( let i = 0; i < subframes.length; ++i )
         descriptors.push( { filePath: subframes[ i ], failed: true } );

      // extract successful measurements
      for ( let i = 0; i < SS.measurements.length; ++i )
      {
         let index = SS.measurements[ i ][ iIndex ] - 1; // N.B. SS's indexes are one-based
         let FWHM = SS.measurements[ i ][ iFWHM ];
         let eccentricity = SS.measurements[ i ][ iEccentricity ];
         let SNR = SS.measurements[ i ][ iSNRWeight ];
         let noise = SS.measurements[ i ][ iNoise ];
         let numberOfStars = SS.measurements[ i ][ iStars ];

         if ( isFinite( FWHM ) )
            if ( isFinite( eccentricity ) )
               if ( isFinite( SNR ) )
                  if ( isFinite( numberOfStars ) )
                     descriptors[index] = {
                        filePath: subframes[ index ],
                        FWHM: FWHM,
                        eccentricity: eccentricity,
                        SNR: SNR,
                        noise: noise,
                        numberOfStars: numberOfStars,
                        failed: false
                     };
      }

      console.writeln();

      // gather measurements in file items
      for ( let i = 0; i < descriptors.length; ++i )
      {
         let descriptor = descriptors[i];
         console.writeln( "<end><cbr><raw>", descriptor.filePath, "</raw>" );
         if ( descriptor.failed )
         {
            fileItems[ i ].processingFailed();
            console.warningln( "** Warning: Failed to measure frame - image will be ignored." );
            this.processLogger.addWarning( "Failed to measure " + descriptor.filePath + ", image will be discarded." );
            failed++;
         }
         else
         {
            fileItems[ i ].descriptor = descriptor;
            console.noteln( "--------------------------" );
            console.noteln( "FWHM            : ", format( "%0.3f px", descriptor.FWHM ) );
            console.noteln( "Eccentricity    : ", format( "%0.3f", descriptor.eccentricity ) );
            console.noteln( "SNR             : ", format( "%0.3f", descriptor.SNR ) );
            console.noteln( "Number Of Stars : ", format( "%i", descriptor.numberOfStars ) );
            console.noteln( "--------------------------" );
         }
      }

      console.flush();
      processEvents();
      gc();
   }
   else
   {
      for ( let j = 0; j < fileItems.length; ++j )
      {
         let filePath = fileItems[ j ].current;
         console.writeln( "Computing descriptors for image ", (j+1) + " of " + fileItems.length );
         console.writeln( filePath );
         console.flush();

         let imageWindow = this.readImage( filePath, "" );
         if ( imageWindow === null )
         {
            console.warningln( "** Warning: Unable to read image: " + filePath );
            continue;
         }
         processEvents();
         gc();

         if ( imageWindow.mainView.image.colorSpace != ColorSpace_Gray )
         {
            let convertToGrayscale = new ConvertToGrayscale;
            convertToGrayscale.executeOn( imageWindow.mainView, false );
            console.abortEnabled = true;
         }

         let descriptor = this.ImageDescriptor( imageWindow, filePath );
         console.noteln( "--------------------------" );
         console.noteln( "FWHM            : ", isFinite( descriptor.FWHM ) ? format( "%0.3f px", descriptor.FWHM ) : '-' );
         console.noteln( "Eccentricity    : ", isFinite( descriptor.eccentricity ) ? format( "%0.3f", descriptor.eccentricity ) : '-' );
         console.noteln( "SNR             : ", isFinite( descriptor.SNR ) ? format( "%0.3f", descriptor.SNR ) : '-' );
         console.noteln( "Number Of Stars : ", isFinite( descriptor.numberOfStars ) ? format( "%i", descriptor.numberOfStars ) : '-' );
         console.noteln( "--------------------------" );
         console.flush();
         processEvents();
         gc();

         if ( descriptor.failed )
         {
            failed++;
            fileItems[ j ].processingFailed();
            console.warningln( "Failed to measure ", filePath );
            console.warningln( "Image will be ignored." );
            this.processLogger.addWarning( "Failed to measure " + filePath + ", image will be discarded." );
         }
         else
         {
            fileItems[ j ].descriptor = descriptor;
         }

         imageWindow.forceClose();
      }
   }

   // check measurements
   return fileItems.length - failed;
};

// ----------------------------------------------------------------------------
StackEngine.prototype.doLinearPatternSubtraction = function()
{

   let
   {
      existingDirectory,
   } = WBPPUtils.shared();

   this.processLogger.addMessage( "<b> *******************</b> <i>LINEAR PATTERN SUBTRACTION</i> <b>******************* </b>" );

   // first step: we need to generate the reference image. This image needs to be generated
   // for each binning values amongst the post-process groups.
   // For each binning we use as reference the group that has the
   // longest total exposure time (supposed to have the highest SNR once integrated).

   // retrieve the post-process groups
   let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   // separate groups by binning
   let groupsByBinning = groups.reduce( ( acc, group ) =>
   {
      if ( !acc[ group.binning ] )
      {
         acc[ group.binning ] = [ group ]
      }
      else
      {
         acc[ group.binning ].push( group );
      }

      return acc;
   },
   {} );

   // iterate through all binnings
   let binnings = Object.keys( groupsByBinning );
   for ( let i = 0; i < binnings.length; i++ )
   {
      let binning = binnings[ i ];
      let groups = groupsByBinning[ binning ];

      // sort groups by the total active duration
      let groupsSortedByDuration = groups.map( g => (
         {
            group: g,
            tet: g.totalExposureTime( true /* only active frames */ )
         } ) )
         .sort( ( a, b ) => ( b.tet - a.tet ) )
         .filter( g => ( g.group.activeFrames().length >= 3 ) )
         .map( g => ( g.group ) );

      // check if a reference group has been found
      if ( groupsSortedByDuration.length == 0 )
      {
         console.warningln( "** Warning: unable generate the Linear Defect Detection reference frame." +
            " No groups with at least 3 grayscale light frames found.Light frames with binning " + binning +
            " will not be corrected." );
         this.processLogger.addError( "Uable generate the Linear Defect Detection reference frame." +
            " No groups with at least 3 grayscale light frames found. Light frames with binning " + binning +
            " will not be corrected." );
         continue;
      }

      // reference group
      let referenceGroup = groupsSortedByDuration[ 0 ];

      // integrate the group to generate the reference frame
      let referenceFramePath = this.doIntegrate(
         referenceGroup,
         "_LDD_REFERENCE_FRAME",
         false, /* custom generate rejection maps */
         false, /* custom generate drizzle files */
         true /* forceWeightModeNoiseEvaluation */
      );

      // integration check
      if ( !referenceFramePath )
      {
         console.warningln( "** Warning: failed to generate the Linear Pattern Subtraction reference frame." +
            " Light frames with binning " + binning + " will not be corrected." );
         this.processLogger.addError( "Warning: failed to generate the Linear Pattern Subtraction reference frame for" +
            " binning " + binning + ", light frames with this binning will not be corrected." );
         continue;
      }

      // open the refrence frame
      let refrenceFrameImageWindow = ImageWindow.open( referenceFramePath );
      if ( refrenceFrameImageWindow.length > 0 )
      {
         refrenceFrameImageWindow = refrenceFrameImageWindow[ 0 ];
         refrenceFrameImageWindow.show();
      }
      else
      {
         console.warningln( "** Warning: failed to open the Linear Pattern Subtraction reference frame." +
            " Light frames with binning " + binning + " will not be corrected." );
         this.processLogger.addError( "Warning: failed to open the Linear Pattern Subtraction reference frame for" +
            " binning " + binning + ", light frames with this binning will not be corrected." );
         continue;
      }
      let W = refrenceFrameImageWindow.mainView.image.width;
      let H = refrenceFrameImageWindow.mainView.image.height;

      // configure the procedure
      console.noteln( "Perform Linear Defect Detection for binning ", binning );
      this.processLogger.addSuccess( "Linear Defect Detection reference frame", referenceFramePath );
      let LDD = new LDDEngine();
      LDD.detectionThreshold = engine.linearPatternSubtractionRejectionLimit;
      LDD.closeFormerWorkingImages = true;
      LDD.execute();
      refrenceFrameImageWindow.forceClose();
      console.noteln( "Linear Defect Detection completed" );

      // proceed applying LPS
      let LPS = new LPSEngine();
      LPS.targetIsActiveImage = false;
      LPS.backgroundReferenceWidth = W;
      LPS.backgroundReferenceHeight = H;
      LPS.outputDir = existingDirectory( this.outputDirectory + "/calibrated/light/lps" );
      LPS.rejectionLimit = engine.linearPatternSubtractionRejectionLimit;
      LPS.closeFormerWorkingImages = true;
      LPS.defectColumnOrRow = LDD.detectedColumnOrRow;
      LPS.defectStartPixel = LDD.detectedStartPixel;
      LPS.defectEndPixel = LDD.detectedEndPixel;

      let detectMode;
      switch ( engine.linearPatternSubtractionMode )
      {
         case 0: // columns
            detectMode = [
            {
               correctColumns: true,
               postfix: "_lps"
            } ];
            break;
         case 1: // rows
            detectMode = [
            {
               correctColumns: false,
               postfix: "_lps"
            } ];
            break;
         case 2: // columns and rows
            detectMode = [
            {
               correctColumns: true,
               postfix: "_lps"
            },
            {
               correctColumns: false,
               postfix: "" // on the second execution we overwrite existing files
            } ];
            break;
      }

      // list the active frames to be processed for the current binning
      let activeFrames = groups.reduce( ( acc, group ) =>
      {
         return acc.concat( group.activeFrames() )
      }, [] );

      // track the file items successfully processed
      let successfullyProcessedFileItems = {};

      // execute the correction for all selected modes
      detectMode.forEach( dm =>
      {
         // configure and run LPS
         LPS.correctColumns = dm.correctColumns;
         LPS.postfix = dm.postfix;
         LPS.inputFiles = activeFrames.map( fileItem => fileItem.current );

         console.noteln( "Executing Linear Pattern Subtraction for gray light frames with binning ", binning );
         LPS.execute();
         console.noteln( "Linear Pattern Subtraction completed" );

         // check output results
         if ( LPS.output.length == 0 )
         {
            // report the issue
            console.warningln( "** Warning: Linear Processing Subtraction failed. Light frames with binning " + binning + " will not be corrected." );
            this.processLogger.addError( "Linear Processing Subtraction failed. Light frames with binning " + binning + " will not be corrected." );
            successfullyProcessedFileItems = {};
         }
         else
         {
            // create a support for output file matching
            let LPSFilesData = LPS.output.map( filePath => (
            {
               name: File.extractName( filePath ),
               filePath: filePath
            } ) );

            // check which acive frame has been successfully processed
            for ( let c = 0; c < activeFrames.length; ++c )
            {
               let lpsFileName = File.extractName( activeFrames[ c ].current ) + dm.postfix;
               let lpsMatch = LPSFilesData.filter( lpsfd => lpsfd.name == lpsFileName );

               if ( lpsMatch.length == 1 )
               {
                  if ( File.exists( lpsMatch[ 0 ].filePath ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.LPS, lpsMatch[ 0 ].filePath );
                     successfullyProcessedFileItems[ activeFrames[ c ].filePath ] = activeFrames[ c ];
                  }
                  else
                  {
                     console.warningln( "** Warning: File does not exist after Linear Pattern Subtraction: " + activeFrames[ c ].current );
                     this.processLogger.addWarning( "File does not exist after Linear Pattern Subtraction: " + activeFrames[ c ].current );
                     successfullyProcessedFileItems[ activeFrames[ c ].filePath ] = undefined;
                  }
               }
               else
               {
                  console.warningln( "** Warning: Linear Pattern Subtraction failed for frame: " + activeFrames[ c ].current );
                  this.processLogger.addWarning( "Linear Pattern Subtraction failed for frame: " + activeFrames[ c ].current );
                  successfullyProcessedFileItems[ activeFrames[ c ].filePath ] = undefined;
               }
            }
         }
      } )

   }

   this.processLogger.addMessage( "<b> ******************************************************************************** </b>" );
   this.processLogger.newLine();
}

// ----------------------------------------------------------------------------

/**
 * Initialize the processing.
 * To be invoked before start doing bias, dark, flat and light frames.
 */
StackEngine.prototype.initializeExecution = function()
{
   this.groupsManager.initializeProcessing();
}

// ----------------------------------------------------------------------------

/**
 * Performs BIAS frames integration.
 *
 * @returns
 */
StackEngine.prototype.doBias = function()
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // process only pre-process groups
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.BIAS && !groups[ i ].hasMaster )
      {
         this.processLogger.addMessage( groups[ i ].logStringHeader( 'MASTER BIAS GENERATION' ) );

         let masterBiasPath = this.doIntegrate( groups[ i ] );
         if ( isEmptyString( masterBiasPath ) )
         {
            console.warningln( "** Warning: Master Bias file was not generated." );
            this.processLogger.addError( "Warning: Master Bias file was not generated." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            return;
         }

         this.groupsManager.addFileItem( new FileItem(
            masterBiasPath,
            ImageType.BIAS,
            groups[ i ].filter,
            groups[ i ].binning,
            groups[ i ].exposureTime,
            groups[ i ].isCFA,
            true, /* master file */
            groups[ i ].keywords
         ) );

         this.processLogger.addSuccess( "Integration completed", "master file " + groups[ i ].fileItems[ 0 ].filePath );
         this.processLogger.addMessage( groups[ i ].logStringFooter() );
         this.processLogger.newLine();
         processEvents();
         gc();
      }
};

// ----------------------------------------------------------------------------

/**
 * Performs DARK frames integration.
 *
 * @returns
 */
StackEngine.prototype.doDark = function()
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // process only pre-process groups
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.DARK && !groups[ i ].hasMaster )
      {
         this.processLogger.addMessage( groups[ i ].logStringHeader( 'MASTER DARK GENERATION' ) );

         let masterDarkPath = this.doIntegrate( groups[ i ] );
         if ( isEmptyString( masterDarkPath ) )
         {
            console.warningln( "** Warning: Master Dark file was not generated." );
            this.processLogger.addError( "Master Dark file was not generated." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            return;
         }

         this.groupsManager.addFileItem( new FileItem(
            masterDarkPath,
            ImageType.DARK,
            groups[ i ].filter,
            groups[ i ].binning,
            groups[ i ].exposureTime,
            groups[ i ].isCFA,
            true, // master file
            groups[ i ].keywords
         ) );

         this.processLogger.addSuccess( "Integration completed", "master file " + groups[ i ].fileItems[ 0 ].filePath );
         this.processLogger.addMessage( groups[ i ].logStringFooter() );
         this.processLogger.newLine();
         processEvents();
         gc();
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates all master flat frames.
 *
 */
StackEngine.prototype.doFlat = function()
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // process only pre-process groups
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.FLAT && !groups[ i ].hasMaster )
      {
         let activeFrames = groups[ i ].activeFrames();

         this.processLogger.addMessage( groups[ i ].logStringHeader( 'MASTER FLAT GENERATION' ) );

         let calibratedFiles = this.doCalibrate( groups[ i ] );
         if ( !calibratedFiles || calibratedFiles.length < 1 )
         {
            // mark all processed frames as failed
            activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
            // report the issue
            console.warningln( "** Warning: Master Flat file was not generated." );
            this.processLogger.addError( "Master Flat file was not generated." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }

         // create a support for output file matching
         let calibratedFilesData = calibratedFiles.map( filePath => (
         {
            name: File.extractName( filePath ),
            filePath: filePath
         } ) );

         // check which acive frame has been successfully processed
         for ( let c = 0; c < activeFrames.length; ++c )
         {
            let calibratedFileName = File.extractName( activeFrames[ c ].current ) + "_c";
            let calibratedMatch = calibratedFilesData.filter( cfd => cfd.name == ( calibratedFileName ) );

            if ( calibratedMatch.length == 1 )
            {
               if ( File.exists( calibratedMatch[ 0 ].filePath ) )
               {
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CALIBRATION, calibratedMatch[ 0 ].filePath );
               }
               else
               {
                  console.warningln( "** Warning: File does not exist after image calibration: " + activeFrames[ c ].current );
                  this.processLogger.addWarning( "File does not exist after image calibration: " + activeFrames[ c ].current );
                  activeFrames[ c ].processingFailed();
               }
            }
            else
            {
               console.warningln( "** Warning: Calibration failed for frame: " + activeFrames[ c ].current );
               this.processLogger.addWarning( "Calibration failed for frame: " + activeFrames[ c ].current );
               activeFrames[ c ].processingFailed();
            }
         }

         // report how many flat frames exist after calibration
         let calibratedActiveFrames = groups[ i ].activeFrames();
         if ( calibratedActiveFrames.length < 1 )
         {
            console.warningln( "** Warning: No flat frames found after calibration." );
            this.processLogger.addError( "No flat frames found after calibration." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }
         else
         {
            this.processLogger.addSuccess( "Calibration completed", calibratedActiveFrames.length + " flat frame" + ( calibratedActiveFrames.length == 1 ? "" : "s" ) + " calibrated." );
         }
         processEvents();
         gc();

         // CALIBRATION COMPLETED

         // do integrate
         let masterFlatPath = this.doIntegrate( groups[ i ] );

         // check the result
         if ( isEmptyString( masterFlatPath ) || !File.exists( masterFlatPath ) )
         {
            console.warningln( "** Warning: Master Flat file was not be generated." );
            this.processLogger.addError( "Master Flat file was not be generated." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }

         // add the created master flat
         groups[ i ].addFileItem( new FileItem(
            masterFlatPath,
            ImageType.FLAT,
            groups[ i ].filter,
            groups[ i ].binning,
            groups[ i ].exposureTime,
            groups[ i ].isCFA,
            true, /* master file */
            groups[ i ].keywords
         ) );

         this.processLogger.addSuccess( "Integration completed", "master flat saved at path " + masterFlatPath );
         this.processLogger.addMessage( groups[ i ].logStringFooter() );
         this.processLogger.newLine();
         processEvents();
         gc();
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates all master light frames.
 *
 * @returns
 */
StackEngine.prototype.doLight = function()
{
   /*
    * ### N.B. The option to use as registration reference the light with the best weight
    *          requires to:
    *
    *          1. apply calibration/cosmeticCorrection/debayer/SubframeWeighting to
    *             all pre-processing groups
    *
    *          2. if the automatic registration reference image selection is enabled then the light
    *             with the highest FWHM+eccentricity across all groups will be set as reference frame
    *             otherwise replace it with its calibrated / cosmetized / debayered
    *             counterpart. See also doCalibrate().
    *
    *          3. registration/integration will continue for all post-processing
    *             groups
    */

   // determine the lowest binning available
   // the reference frame is selected among the frames with that binning
   let
   {
      cleanFilterName,
      enableTargetFrames,
      existingDirectory,
      isEmptyString,
   } = WBPPUtils.shared();

   // use pre-process groups only
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // CALBRATE ALL GROUPS

   for ( let i = 0; i < groups.length; ++i )
   {
      if ( groups[ i ].imageType == ImageType.LIGHT )
      {
         let activeFrames = groups[ i ].activeFrames();

         this.processLogger.addMessage( groups[ i ].logStringHeader( 'LIGHT FRAMES CALIBRATION' ) );

         let calibratedFiles = this.doCalibrate( groups[ i ] );
         if ( !calibratedFiles )
         {
            // calibration skipped
            console.warningln( "** Warning: Calibration skipped, no master files found." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }
         else if ( calibratedFiles.length < 1 )
         {
            // mark all active frames as failed
            activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
            // report the issue
            console.warningln( "** Warning: Light frames calibration failed." );
            this.processLogger.addError( "Light frames calibration failed." );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }
         else
         {
            // create a support for output file matching
            let calibratedFilesData = calibratedFiles.map( filePath => (
            {
               name: File.extractName( filePath ),
               filePath: filePath
            } ) );

            // check which active frame has been successfully processed
            for ( let c = 0; c < activeFrames.length; ++c )
            {
               let calibratedFileName = File.extractName( activeFrames[ c ].current ) + "_c";
               let calibratedMatch = calibratedFilesData.filter( cfd => cfd.name == calibratedFileName );

               if ( calibratedMatch.length == 1 )
               {
                  if ( File.exists( calibratedMatch[ 0 ].filePath ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CALIBRATION, calibratedMatch[ 0 ].filePath );
                  }
                  else
                  {
                     activeFrames[ c ].processingFailed();
                     console.warningln( "** Warning: File does not exist after image calibration: " + calibratedMatch[ 0 ].filePath );
                     this.processLogger.addWarning( "File does not exist after image calibration: " + calibratedMatch[ 0 ].filePath );
                  }
               }
               else
               {
                  console.warningln( "** Warning: Calibration failed for frame: " + activeFrames[ c ].current );
                  this.processLogger.addWarning( "Calibration failed for frame: " + activeFrames[ c ].current );
                  activeFrames[ c ].processingFailed();
               }
            }

            // report how many flat frames exist after calibration
            let calibratedActiveFrames = groups[ i ].activeFrames();
            if ( calibratedActiveFrames.length < 1 )
            {
               console.warningln( "** Warning: No light frames found after calibration." );
               this.processLogger.addError( "No light frames found after calibration." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }
            else
            {
               this.processLogger.addSuccess( "Calibration completed", calibratedActiveFrames.length + " light frame" + ( calibratedActiveFrames.length == 1 ? "" : "s" ) + " calibrated." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
            }
         }
         processEvents();
         gc();

         // GROUP CALIBRATION COMPLETED
      }
   } // for each group

   // LINEAR PATTERN SUBTRACTION

   if ( this.linearPatternSubtraction )
   {
      console.noteln( "<end><cbr><br>",
         "************************************************************" );
      console.noteln( "* Apply linear pattern subtraction to light frames" );
      console.noteln( "************************************************************" );

      this.doLinearPatternSubtraction();

      console.noteln( "<end><cbr><br>",
         "************************************************************" );
      console.noteln( "* End of linear pattern subtraction process" );
      console.noteln( "************************************************************" );
   }

   // COSMETIC CORRECTION / DEBAYER

   for ( let i = 0; i < groups.length; ++i )
   {
      if ( groups[ i ].imageType == ImageType.LIGHT )
      {

         this.processLogger.addMessage( groups[ i ].logStringHeader( 'COSMETIZATION / DEBAYERING' ) );

         if ( this.cosmeticCorrection )
         {
            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.noteln( "* Begin cosmetic correction of light frames" );
            console.noteln( "************************************************************" );

            groups[ i ].log();

            let activeFrames = groups[ i ].activeFrames();

            let CC = ProcessInstance.fromIcon( this.cosmeticCorrectionTemplateId );
            if ( CC == null )
            {
               console.warningln( "** Warning: No such process icon: " + this.cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
               this.processLogger.addWarning( "No such process icon: " + this.cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
            }
            else if ( !( CC instanceof CosmeticCorrection ) )
            {
               console.warningln( "** Warning: The specified icon does not transport an instance " +
                  "of CosmeticCorrection: " + this.cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
               this.processLogger.addWarning( "The specified icon does not transport an instance " +
                  "of CosmeticCorrection: " + this.cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
            }
            else
            {
               let cosmetizedDirectory = existingDirectory( this.outputDirectory + "/calibrated/light/cosmetized" );

               let activeFramesFilePaths = activeFrames.map( activeFrame => activeFrame.current );
               CC.targetFrames = enableTargetFrames( activeFramesFilePaths, 2 );
               CC.outputDir = cosmetizedDirectory;
               CC.outputExtension = ".xisf";
               CC.prefix = "";
               CC.postfix = "_cc";
               CC.overwrite = true;
               CC.cfa = groups[ i ].isCFA;

               CC.executeGlobal();

               /*
                * ### FIXME: CosmeticCorrection should provide read-only output
                * data, including the full file path of each output image.
                */
               let countFailed = 0;
               images = new Array;
               for ( let c = 0; c < activeFramesFilePaths.length; ++c )
               {
                  let filePath = activeFramesFilePaths[ c ];
                  let ccFilePath = cosmetizedDirectory + '/' + File.extractName( filePath ) + "_cc" + ".xisf";

                  // we mark as successful the frames that succeeded but we don't mark as failed frames for which CC failed such that wa can continue
                  // using the uncosmetized versions
                  if ( File.exists( ccFilePath ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CC, ccFilePath );
                  }
                  else
                  {
                     countFailed++;
                     console.warningln( "** Warning: File does not exist after cosmetic correction: " + ccFilePath + ", the uncosmetized frame " + activeFrames[ c ].current + "will be used." );
                     this.processLogger.addWarning( "File does not exist after cosmetic correction: " + ccFilePath + ", the uncosmetized frame " + activeFrames[ c ].current + "will be used." );
                  }
               }
               if ( countFailed == activeFrames.length )
               {
                  console.warningln( "** Warning: All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
                  this.processLogger.addWarning( "All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
               }
               else if ( countFailed == 0 )
               {
                  this.processLogger.addSuccess( "Cosmetic correction completed", activeFrames.length + " light frame" + ( activeFrames.length > 1 ? "s have" : "has" ) + " been calibrated." );
               }
               else
               {
                  let successCount = activeFrames.length - countFailed;
                  this.processLogger.addSuccess( "Cosmetic correction completed", successCount + " light frame" + ( successCount > 1 ? "s have" : "has" ) + " been cosmetized." );
               }

               console.noteln( "<end><cbr><br>",
                  "************************************************************" );
               console.noteln( "* End cosmetic correction of light frames" );
               console.noteln( "************************************************************" );
            }
         }
         else 
         {
            this.processLogger.addSuccess( "Cosmetic Correction", "disabled." );
         }

         if ( groups[ i ].isCFA )
         {
            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.noteln( "* Begin demosaicing of light frames" );
            console.noteln( "************************************************************" );
            this.processLogger.addSuccess( "Demosaicing with pattern", groups[ i ].CFAPatternString() );
            groups[ i ].log();
            console.writeln( "CFA Pattern: ", groups[ i ].CFAPatternString() );
            console.writeln( "Debayer Method: ", groups[ i ].debayerMethodString() );

            activeFrames = groups[ i ].activeFrames();

            if ( activeFrames.length == 0 )
            {
               // report the issue
               console.warningln( "** Warning: No active light frames in the group." );
               this.processLogger.addError( "No active light frames in the group." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }

            let DB = new Debayer;

            let debayerDirectory = existingDirectory( this.outputDirectory + "/calibrated/light/debayered" );

            let activeFramesFilePaths = activeFrames.map( activeFrame => activeFrame.current );
            DB.cfaPattern = groups[ i ].CFAPattern;
            DB.debayerMethod = groups[ i ].debayerMethod;
            DB.evaluateNoise = this.evaluateNoise;
            DB.targetItems = enableTargetFrames( activeFramesFilePaths, 2 );
            DB.outputDirectory = debayerDirectory;
            DB.outputExtension = ".xisf";
            DB.outputPostfix = "_d";
            DB.overwriteExistingFiles = true;

            DB.executeGlobal();

            if ( !DB.outputFileData || DB.outputFileData.length == 0 )
            {
               // mark all active frames as failed
               activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
               // report the issue
               console.warningln( "** Warning: Light frames debayering failed." );
               this.processLogger.addError( "Light frames debayering failed." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }

            let debayeredFileNames = DB.outputFileData.map( file => file[ 0 ] );

            // create a support for output file matching
            let debayeredFilesData = debayeredFileNames.map( filePath => (
            {
               name: File.extractName( filePath ),
               filePath: filePath
            } ) );

            // check which acive frame has been successfully processed
            for ( let c = 0; c < activeFrames.length; ++c )
            {
               let debayeredFileName = File.extractName( activeFrames[ c ].current ) + "_d";
               let debayeredMatch = debayeredFilesData.filter( dfd => dfd.name == debayeredFileName );

               if ( debayeredMatch.length == 1 )
               {
                  if ( File.exists( debayeredMatch[ 0 ].filePath ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.DEBAYER, debayeredMatch[ 0 ].filePath );
                  }
                  else
                  {
                     console.warningln( "** Warning: File does not exist after image debayering: " + activeFrames[ c ].current );
                     this.processLogger.addWarning( "File does not exist after image debayering: " + activeFrames[ c ].current );
                     activeFrames[ c ].processingFailed();
                  }
               }
               else
               {
                  console.warningln( "** Warning: Debayer failed for frame: " + activeFrames[ c ].current );
                  this.processLogger.addWarning( "Debayer failed for frame: " + activeFrames[ c ].current );
                  activeFrames[ c ].processingFailed();
               }
            }

            // report how many flat frames exist after calibration
            let debayeredActiveFrames = groups[ i ].activeFrames();
            if ( debayeredActiveFrames.length < 1 )
            {
               console.warningln( "** Warning: No light frames found after debayering." );
               this.processLogger.addError( "No light frames found after debayering." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }
            else
            {
               this.processLogger.addSuccess( "Debayer completed", debayeredActiveFrames.length + " Light frame" + ( debayeredActiveFrames.length == 1 ? "" : "s" ) + "  debayered." );
            }

            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.noteln( "* End demosaicing of light frames" );
            console.noteln( "************************************************************" );
         }
         else
         {
            this.processLogger.addSuccess( "Debayer", "Grayscale frames, no demosaicing is needed." );
         }
         this.processLogger.addMessage( groups[ i ].logStringFooter() );
         this.processLogger.newLine();
         processEvents();
         gc();
      }
   }

   // CALIBRATION - LINEAR PATTERN SUBTRACTION - COSMETIC CORRECTION - DEBAYER completed

   // here we continue using the post-process groups
   groups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   // ----------------------------------------------------------------------------
   // MEASURING FRAMES
   // ----------------------------------------------------------------------------

   // List of cases which requires to measure the images:
   // 1. if I choose to generate the weights
   // 2. if I do the registration and I want the best reference image to be auto-selcted
   let referenceFrameData = {};

   let measureImages = this.generateSubframesWeights || ( this.imageRegistration && this.bestFrameRefernceMethod != WBPPBestRefernenceMethod.MANUAL );
   if ( measureImages )
   {
      console.noteln( "<end><cbr><br>",
         "************************************************************" );
      console.noteln( "* Perform image measurements" );
      console.noteln( "************************************************************" );
      console.flush();
      this.processLogger.addMessage( "<b>*****************</b> <i>IMAGE MEASUREMENTS</i> <b>**************</b>" );

      if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
      {
         referenceFrameData[ "__single__" ] = {
            /* measure all groups  */
            groups: groups
         }
      }
      else
      {
         // measure the lowest binning for each keyword value
         referenceFrameData = groups.reduce( ( acc, group ) =>
         {
            let value = group.keywords[ this.bestFrameReferenceKeyword ] || "__undefined__";
            if ( acc[ value ] )
            {
               if ( acc[ value ] > group.binning )
                  acc[ value ] = {
                     binning: group.binning,
                     groups: [ group ]
                  }
               else if ( acc[ value ].binning == group.binning )
               {
                  acc[ value ].groups.push( group );
               }
            }
            else
               acc[ value ] = {
                  binning: group.binning,
                  groups: [ group ]
               }
            return acc;
         },
         {} );
      }

      // retrieve the whole list of frames to be measured
      let framesToMeasure = [];
      if ( this.generateSubframesWeights )
      {
         for ( let i = 0; i < groups.length; i++ )
            framesToMeasure = framesToMeasure.concat( groups[ i ].activeFrames() )
      }
      else
      {
         framesToMeasure = Object.keys( referenceFrameData ).reduce( ( acc, key ) =>
         {
            return acc.concat( referenceFrameData[ key ].groups );
         }, [] ).reduce( ( acc, group ) =>
         {
            return acc.concat( group.activeFrames() );
         }, [] );
      }

      // measure the frames
      let measuredCount = this.computeDescriptors( framesToMeasure );

      // report
      if ( measuredCount == framesToMeasure.length )
      {
         console.noteln( "All frame measurements completed successfully" );
         this.processLogger.addSuccess( "Frames measurement:", "completed successfully" );
      }
      else if ( measuredCount > 0 )
      {
         console.noteln( "Measurements completed successfully for " + measuredCount + " light frame" + ( measuredCount == 1 ? "" : "s" ) + " over " + framesToMeasure.length + "." );
         this.processLogger.addSuccess( "Measurements completed successfully", measuredCount + " light frame" + ( measuredCount == 1 ? "" : "s" ) + " over " + framesToMeasure.length + "." );
      }
      else
      {
         console.noteln( "Frame measurement failed for all light frames." );
         this.processLogger.addError( "Frame measurement failed for all light frames." );
         this.processLogger.newLine();
      }
      processEvents();
      gc();

      console.noteln( "<end><cbr><br>",
         "************************************************************" );
      console.noteln( "* End generation of images descriptors" );
      console.noteln( "************************************************************" );
      console.flush();
   }

   // assign the manual reference frame
   if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
   {
      // assume that the file is not added to the session
      let currentReferenceImage = this.referenceImage;
      // check if the reference frame is one frame in the session
      let referenceFrameFileItem = this.groupsManager.getReferenceFrameFileItem( this.referenceImage );
      if ( referenceFrameFileItem )
      {
         console.noteln( "referenceFrameFileItem.current: ", referenceFrameFileItem.current )
         console.noteln( "referenceFrameFileItem.filePath: ", referenceFrameFileItem.filePath )
         currentReferenceImage = referenceFrameFileItem.current || referenceFrameFileItem.filePath;
      }
      // by convention assume to configure the "auto_single" mode with the manual reference file
      referenceFrameData[ "__manual__" ] = {
         /* no groups to be measured */
         groups: [],
         referenceImage: currentReferenceImage
      }
   }

   // ----------------------------------------------------------------------------
   // GENERATE SUBFRAME WEIGHTS
   // ----------------------------------------------------------------------------

   // if frame analysis has been already performed and weights needs to be stored before the registration
   if ( this.generateSubframesWeights )
      this.writeWeightsWithDescriptors( groups );

   // close the measurements block in the smart report
   if ( measureImages )
   {
      this.processLogger.addSuccess( "************************************************************" );
      this.processLogger.newLine();
   }

   // exit if neither registration nor integration takes place
   if ( !this.imageRegistration )
      if ( !this.integrate )
         return;

   // ----------------------------------------------------------------------------
   // SELECTION OF BEST REFERENCE FRAME
   // ----------------------------------------------------------------------------

   if ( this.imageRegistration )
   {
      let criticalErrorOccurred = false;

      // initialize the reference frame per group
      for ( let i = 0; i < groups.length; i++ )
         groups[ i ].__reference_frame__ = undefined;

      this.processLogger.addMessage( "<b>**********</b> <i>BEST REFERENCE FRAME FOR REGISTRATION</i> <b>***********</b>" );

      if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
      {
         // search in all files the selected frame, in case take as reference the
         // current processed file name
         console.noteln( "Best reference frame, manually selected: " + referenceFrameData[ "__manual__" ].referenceImage );
         this.processLogger.addSuccess( "Best reference frame", " manually selected " + referenceFrameData[ "__manual__" ].referenceImage );

         // save reference frame for all groups
         for ( let i = 0; i < groups.length; i++ )
            groups[ i ].__reference_frame__ = referenceFrameData[ "__manual__" ].referenceImage;
      }
      else
      {
         console.noteln( "<end><cbr><br>",
            "************************************************************" );
         console.noteln( "* Begin selection of the best reference frame" );
         console.noteln( "************************************************************" );
         console.flush();

         if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
         {
            let actualReferenceImage = this.findRegistrationReferenceImage( referenceFrameData[ "__single__" ].groups );
            if ( actualReferenceImage )
            {
               console.noteln( "Best reference frame for registration - auto selection completed: " + actualReferenceImage );
               this.processLogger.addSuccess( "Best reference frame", " (auto selection) " + actualReferenceImage );

               // save reference frame for all groups
               for ( let i = 0; i < groups.length; i++ )
                  groups[ i ].__reference_frame__ = actualReferenceImage;
            }
            else
            {
               // mark all frames as failed
               for ( let i = 0; i < groups.length; i++ )
                  for ( let j = 0; j < groups[ i ].fileItems.length; j++ )
                     groups[ i ].fileItems[ j ].processingFailed();

               console.criticalln( "Unable to detect the best reference frame." );
               this.processLogger.addError( "Unable to detect the best reference frame." );
               criticalErrorOccurred = true;
            }
         }
         else
         {
            referenceFrameData = Object.keys( referenceFrameData ).reduce( ( acc, key ) =>
            {
               data = referenceFrameData[ key ];
               let actualReferenceImage = this.findRegistrationReferenceImage( data.groups );
               console.noteln( "<end><cbr><br>" );
               if ( actualReferenceImage )
               {
                  console.noteln( "Best reference frame for " + this.bestFrameReferenceKeyword + " = " + key + " : " + actualReferenceImage );
                  this.processLogger.addSuccess( "Best reference frame for " + this.bestFrameReferenceKeyword + " = " + key, actualReferenceImage );

                  // store the reference image in the current groups per keyword
                  for ( let i = 0; i < data.groups.length; i++ )
                     data.groups[ i ].__reference_frame__ = actualReferenceImage;
               }
               else
               {
                  // mark all frames in the current goups per keyword as failed
                  for ( let i = 0; i < data.groups.length; i++ )
                     for ( let j = 0; j < data.groups[ i ].fileItems.length; j++ )
                        data.groups[ i ].fileItems[ j ].processingFailed();

                  let msg = "Unable to detect the best reference frame for " + this.bestFrameReferenceKeyword + " = " + key +
                     ". Groups with this key/value will not be registered.";
                  console.criticalln( msg );
                  this.processLogger.addError( msg );
               }
               return acc;
            },
            {} );

            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.flush();
         }
      }

      if ( criticalErrorOccurred )
      {
         this.processLogger.addError( "Missing reference frame, the process will stop." );
         console.criticalln( "Missing reference frame, the process will stop." );
      }

      this.processLogger.addMessage( "<b>************************************************************</b>" );
      this.processLogger.newLine();

      if ( criticalErrorOccurred )
         return;
   }

   // ----------------------------------------------------------------------------
   // PROCESS POST-PROCESS GROUPS
   // ----------------------------------------------------------------------------

   if ( engine.imageRegistration || engine.integrate )
   {
      for ( let i = 0; i < groups.length; ++i )
      {
         let filter = groups[ i ].filter;

         this.processLogger.addMessage( groups[ i ].logStringHeader( "MASTER LIGHT GENERATION" ) );

         let activeFrames = groups[ i ].activeFrames();

         if ( activeFrames.length == 0 )
         {
            console.warningln( "** Warning: Group " + groups[ i ].toString() + " has no active frames and it will be skipped." );
            this.processLogger.addError( "Group " + groups[ i ].toString() + " has no active frames and it will be skipped." )
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }

         if ( engine.imageRegistration && !groups[ i ].__reference_frame__ )
         {
            console.warningln( "** Warning: No reference frame for " + groups[ i ].toString() );
            this.processLogger.addError( "No reference frame for " + groups[ i ].toString() );
            this.processLogger.addMessage( groups[ i ].logStringFooter() );
            this.processLogger.newLine();
            continue;
         }

         // proceed
         if ( engine.imageRegistration )
         {
            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.noteln( "* Begin registration of light frames" );
            console.noteln( "************************************************************" );
            groups[ i ].log();
            console.flush();

            console.noteln( 'Reference image: ', groups[ i ].__reference_frame__ );
            this.processLogger.addSuccess( 'Reference image', groups[ i ].__reference_frame__ );
            console.noteln( 'Registering ', activeFrames.length, ' light images' );
            let SA = new StarAlignment;

            let registerDirectory = this.outputDirectory + "/registered";
            if ( !isEmptyString( filter ) )
               registerDirectory += '/' + cleanFilterName( filter );
            registerDirectory = existingDirectory( registerDirectory );

            let filePaths = activeFrames.map( item => item.current );
            SA.inputHints = this.inputHints();
            SA.outputHints = this.outputHints();
            SA.referenceImage = groups[ i ].__reference_frame__;
            SA.referenceIsFile = true;
            SA.targets = enableTargetFrames( filePaths, 3 );
            SA.outputDirectory = registerDirectory;
            SA.generateDrizzleData = this.generateDrizzleData;
            SA.pixelInterpolation = this.pixelInterpolation;
            SA.clampingThreshold = this.clampingThreshold;
            SA.structureLayers = this.structureLayers;
            SA.minStructureSize = this.minStructureSize;
            SA.noiseReductionFilterRadius = this.noiseReductionFilterRadius;
            SA.sensitivity = this.sensitivity;
            SA.peakResponse = this.peakResponse;
            SA.maxStarDistortion = this.maxStarDistortion;
            SA.useTriangles = this.useTriangleSimilarity;
            SA.outputExtension = ".xisf";
            SA.outputPrefix = "";
            SA.outputPostfix = "_r";
            SA.outputSampleFormat = StarAlignment.prototype.f32;
            SA.overwriteExistingFiles = true;

            if ( this.distortionCorrection )
            {
               SA.useSurfaceSplines = true;
               SA.distortionCorrection = true;
               SA.localDistortion = true;
            }
            else
               SA.maxStars = this.maxStars;

            if ( !SA.executeGlobal() )
            {
               console.warningln( "** Warning: Error registering light frames. This group will be skipped." );
               this.processLogger.addError( "Error registering light frames. This group will be skipped." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }

            if ( !SA.outputData || SA.outputData.length < 1 )
            {
               // mark all active frames as failed
               activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
               // report the issue
               console.warningln( "** Warning: Light frames registration failed." );
               this.processLogger.addError( "Light frames registration failed." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }

            // create a support for output file matching
            let registeredFilesData = SA.outputData.map( filePath => (
            {
               name: File.extractName( filePath[ 0 ] ),
               filePath: filePath[ 0 ]
            } ) );

            // check which acive frame has been successfully processed
            for ( let c = 0; c < activeFrames.length; ++c )
            {
               let registeredFileName = File.extractName( activeFrames[ c ].current ) + "_r";
               let registeredMatch = registeredFilesData.filter( rfd => rfd.name == registeredFileName );

               if ( registeredMatch.length == 1 )
               {
                  if ( File.exists( registeredMatch[ 0 ].filePath ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.REGISTRATION, registeredMatch[ 0 ].filePath );
                  }
                  else
                  {
                     console.warningln( "** Warning: File does not exist after image registration: " + activeFrames[ c ].current );
                     this.processLogger.addWarning( "File does not exist after image registration: " + activeFrames[ c ].current );
                     activeFrames[ c ].processingFailed();
                  }
               }
               else
               {
                  console.warningln( "** Warning: Registration failed for image: " + activeFrames[ c ].current );
                  this.processLogger.addWarning( "Registration failed for image: " + activeFrames[ c ].current );
                  activeFrames[ c ].processingFailed();
               }
            }

            let registeredFrames = groups[ i ].activeFrames();
            if ( registeredFrames.length < 1 )
            {
               console.warningln( "** Warning: No light frames found after registration." );
               this.processLogger.addError( "No light frames found after registration." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }
            else if ( registeredFrames.length < 3 )
            {
               let failed = activeFrames.length - registeredFrames.length;
               console.warningln( "** Warning: Star alignment failed, to register " + failed + " images out of ", activeFrames.length, ". A minimum of 3 images must be successfully registered." );
               this.processLogger.addError( "Star alignment failed to register " + failed + " images out of " + activeFrames.length + ". A minimum of 3 images must be successfully registered." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }
            this.processLogger.addSuccess( "Registration completed", registeredFrames.length + " images out of " + activeFrames.length + " successfully registered." );

            console.noteln( "<end><cbr><br>",
               "************************************************************" );
            console.noteln( "* End registration of light frames" );
            console.noteln( "************************************************************" );
            console.flush();
            processEvents();
            gc();
         }
         else 
         {
            this.processLogger.addSuccess( "Registration", "disabled." );
         }

         if ( engine.integrate )
         {
            let masterLightPath = this.doIntegrate( groups[ i ] );
            if ( isEmptyString( masterLightPath ) || !File.exists( masterLightPath ) )
            {
               console.warningln( "** Warning: Error integrating light frames." );
               this.processLogger.addError( "Error integrating light frames." );
               this.processLogger.addMessage( groups[ i ].logStringFooter() );
               this.processLogger.newLine();
               continue;
            }

            this.processLogger.addSuccess( "Integration completed", "master light saved at path " + masterLightPath );
         }
         else
         {
            this.processLogger.addSuccess( "Integration", "disabled." );
         }

         this.processLogger.addMessage( groups[ i ].logStringFooter() );
      }
   }

   processEvents();
   gc();
}
// ----------------------------------------------------------------------------

/**
 * Integrates the provided group.
 *
 * @param {*} frameGroup
 * @param {String} customPostfix custom postfix to be added at the end of the master frame
 * @param {Boolean} customGenerateRejectionMaps optionally overrides the rejection maps setting
 * @param {Boolean} customGenerateDrizzle optionally override the drizzle files generation
 * @param {Boolean} forceWeightModeNoiseEvaluation force the weight mode to noise evaluation
 * @returns
 */
StackEngine.prototype.doIntegrate = function( frameGroup, customPostfix, customGenerateRejectionMaps, customGenerateDrizzle, forceWeightModeNoiseEvaluation )
{
   let
   {
      cleanFilterName,
      enableTargetFrames,
      existingDirectory,
   } = WBPPUtils.shared();

   let filePath = "";
   let imageType = frameGroup.imageType;

   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* Begin integration of ", StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( "************************************************************" );
   frameGroup.log();

   let frameSet = frameGroup.activeFrames().map( item => item.current );
   if ( frameSet.length < 3 )
   {
      console.warningln( "** Warning: Cannot integrate less than three frames." );
      this.processLogger.addWarning( "Cannot integrate less than three frames." );
   }
   else
   {

      let selectedRejection = ( this.rejection[ imageType ] == ImageIntegration.prototype.auto ) ?
         frameGroup.bestRejectionMethod() : this.rejection[ imageType ];

      if ( this.rejection[ imageType ] == ImageIntegration.prototype.auto )
      {
         console.noteln( 'Rejection method auto-selected: ', engine.rejectionName( selectedRejection ) );
         this.processLogger.addMessage( '<b>Rejection method auto-selected:</b> ' + engine.rejectionName( selectedRejection ) );
      }
      else
      {
         console.noteln( '<b>Rejection method:</b> ', engine.rejectionName( selectedRejection ) );
      }

      // Drizzle is generated only for Light frames
      let generateDrizzle = imageType == ImageType.LIGHT && this.imageRegistration && this.generateDrizzleData;

      let II = new ImageIntegration;

      II.inputHints = this.inputHints();
      II.bufferSizeMB = 16;
      II.stackSizeMB = 1024;
      II.autoMemorySize = true;
      II.autoMemoryLimit = 0.75;
      II.images = enableTargetFrames( frameSet, 2, generateDrizzle );
      II.combination = this.combination[ imageType ];
      II.rejection = selectedRejection;
      II.generateRejectionMaps = customGenerateRejectionMaps != undefined ? customGenerateRejectionMaps : this.generateRejectionMaps;
      II.generateDrizzleData = customGenerateDrizzle != undefined ? customGenerateDrizzle : generateDrizzle;
      II.minMaxLow = this.minMaxLow[ imageType ];
      II.minMaxHigh = this.minMaxHigh[ imageType ];
      II.pcClipLow = this.percentileLow[ imageType ];
      II.pcClipHigh = this.percentileHigh[ imageType ];
      II.sigmaLow = this.sigmaLow[ imageType ];
      II.sigmaHigh = this.sigmaHigh[ imageType ];
      II.winsorizationCutoff = 5.0;
      II.linearFitLow = this.linearFitLow[ imageType ];
      II.linearFitHigh = this.linearFitHigh[ imageType ];
      II.esdOutliersFraction = this.ESD_Outliers[ imageType ];
      II.esdAlpha = this.ESD_Significance[ imageType ];
      II.esdLowRelaxation = this.ESD_LowRelaxation[ imageType ];
      II.clipLow = true;
      II.clipHigh = true;
      II.largeScaleClipLow = false;
      II.largeScaleClipHigh = false;
      II.subtractPedestals = false;
      II.truncateOnOutOfRange = true;
      II.generate64BitResult = false;
      II.showImages = false; // since core 1.8.8-6
      II.useFileThreads = true;
      II.fileThreadOverload = 1.00;

      switch ( imageType )
      {
         case ImageType.LIGHT:
            II.normalization = ImageIntegration.prototype.AdditiveWithScaling;
            II.rejectionNormalization = ImageIntegration.prototype.Scale;
            II.weightScale = ImageIntegration.prototype.WeightScale_IKSS;
            II.largeScaleClipHigh = this.lightsLargeScaleRejectionHigh;
            II.largeScaleClipHighProtectedLayers = this.lightsLargeScaleRejectionLayersHigh;
            II.largeScaleClipHighGrowth = this.lightsLargeScaleRejectionGrowthHigh;
            II.largeScaleClipLow = this.lightsLargeScaleRejectionLow;
            II.largeScaleClipLowProtectedLayers = this.lightsLargeScaleRejectionLayersLow;
            II.largeScaleClipLowGrowth = this.lightsLargeScaleRejectionGrowthLow;
            break;
         case ImageType.FLAT:
            II.normalization = ImageIntegration.prototype.Multiplicative;
            II.rejectionNormalization = ImageIntegration.prototype.EqualizeFluxes;
            II.weightScale = ImageIntegration.prototype.WeightScale_IKSS;
            II.largeScaleClipHigh = this.flatsLargeScaleRejection;
            II.largeScaleClipHighProtectedLayers = this.flatsLargeScaleRejectionLayers;
            II.largeScaleClipHighGrowth = this.flatsLargeScaleRejectionGrowth;
            break;
         default:
            II.normalization = ImageIntegration.prototype.NoNormalization;
            II.rejectionNormalization = ImageIntegration.prototype.NoRejectionNormalization;
            II.weightScale = ImageIntegration.prototype.WeightScale_MAD;
            break;
      }

      switch ( imageType )
      {
         case ImageType.LIGHT:
            II.weightMode = (!forceWeightModeNoiseEvaluation && this.generateSubframesWeights) ? ImageIntegration.prototype.KeywordWeight : ImageIntegration.prototype.NoiseEvaluation;
            II.weightKeyword = this.generateSubframesWeights ? WEIGHT_KEYWORD : "";
            II.evaluateNoise = this.evaluateNoise;
            II.rangeClipLow = true;
            II.rangeLow = 0;
            II.rangeClipHigh = false;
            II.truncateOnOutOfRange = false;
            II.useCache = true;
            break;
         default:
            II.weightMode = ImageIntegration.prototype.DontCare;
            II.evaluateNoise = false;
            II.rangeClipLow = false;
            II.rangeClipHigh = false;
            II.useCache = false;
            break;
      }

      let ok = II.executeGlobal();

      if ( !ok )
      {
         console.warningln( "** Warning: ImageIntegration failed." );
         this.processLogger.addWarning( "ImageIntegration failed." );
      }
      else
      {
         // Write master frame FITS keywords
         // Build the file name postfix

         let keywords = new Array;

         keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
         keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + TITLE + " v" + VERSION ) );

         keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( imageType ), "Type of image" ) );

         let postfix = ""

         keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
         keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );
         postfix += format( "_BINNING-%d", frameGroup.binning );

         // Make sure the filter postfix includes only valid file name characters.
         keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );
         if ( imageType !== ImageType.BIAS && imageType !== ImageType.DARK )
            postfix += "_FILTER-" + cleanFilterName( frameGroup.filter );

         keywords.push( new FITSKeyword( "EXPTIME", format( "%.2f", frameGroup.exposureTime ), "Exposure time in seconds" ) );
         if ( imageType !== ImageType.BIAS && imageType !== ImageType.FLAT )
            postfix += format( "_EXPTIME-%g", frameGroup.exposureTime );

         // concatenate the image keywords filtering out the keywords already added that have to remain unique
         let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
         let window = ImageWindow.windowById( II.integrationImageId );
         window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

         // concatenate the custom group keywords in the header
         Object.keys(frameGroup.keywords).forEach( k =>
         {
            keywords.push( new FITSKeyword( k, frameGroup.keywords[ k ], "Custom grouping keyword" ) );
            postfix += "_" + k + "-" + frameGroup.keywords[ k ];
         } );

         // write the master file
         if ( !customPostfix )
            customPostfix = "";
         filePath = existingDirectory( this.outputDirectory + "/master" );
         filePath += "/master" + StackEngine.imageTypeToString( imageType ) + postfix + customPostfix + ".xisf";

         console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );

         if ( II.generateRejectionMaps )
         {
            let rejectionLowWindow = null;
            let rejectionHighWindow = null;
            let slopeMapWindow = null;

            if ( II.clipLow )
               rejectionLowWindow = ImageWindow.windowById( II.lowRejectionMapImageId );
            if ( II.clipHigh )
               rejectionHighWindow = ImageWindow.windowById( II.highRejectionMapImageId );
            if ( II.rejection == ImageIntegration.prototype.LinearFit )
               slopeMapWindow = ImageWindow.windowById( II.slopeMapImageId );

            this.writeImage( filePath, window, rejectionLowWindow, rejectionHighWindow, slopeMapWindow, true /*imageIdentifiers*/ );

            if ( rejectionLowWindow != null && !rejectionLowWindow.isNull )
               rejectionLowWindow.forceClose();
            if ( rejectionHighWindow != null && !rejectionHighWindow.isNull )
               rejectionHighWindow.forceClose();
            if ( slopeMapWindow != null && !slopeMapWindow.isNull )
               slopeMapWindow.forceClose();
         }
         else
         {
            this.writeImage( filePath, window, null, null, null, true /*imageIdentifiers*/ );
         }

         window.forceClose();
      }
   }

   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* End integration of " + StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( "************************************************************" );


   return filePath;
};

// ----------------------------------------------------------------------------

/**
 * Calibrate the provided frame group.
 *
 * @param {*} frameGroup
 * @returns
 */
StackEngine.prototype.doCalibrate = function( frameGroup )
{
   let
   {
      enableTargetFrames,
      existingDirectory,
      isEmptyString,
   } = WBPPUtils.shared();

   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* Begin calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( "************************************************************" );

   frameGroup.log();

   let calibratedImages = [];
   let activeFrames = frameGroup.activeFrames();

   // -------------------------------
   // search the matching MASTER BIAS
   // -------------------------------
   let masterBias = this.getMasterBiasGroup( frameGroup.binning, true /* isMaster */ , frameGroup.keywords );
   let masterBiasPath = masterBias ? masterBias.fileItems[ 0 ].filePath : "";
   let masterBiasEnabled = !isEmptyString( masterBiasPath );

   // -------------------------------
   // search the matching MASTER DARK
   // -------------------------------
   let masterDark = undefined;
   let masterDarkPath = "";
   if ( !frameGroup.forceNoDark )
   {
      if ( frameGroup.overrideDark )
      {
         masterDarkPath = frameGroup.overrideDark.fileItems[ 0 ].filePath;
         masterDark = frameGroup.overrideDark;
         this.processLogger.addMessage( 'Master Dark manually assigned.' )
      }
      else
      {
         masterDark = this.getMasterDarkGroup(
            frameGroup.binning,
            frameGroup.exposureTime,
            false, /* exactExposureTime */
            frameGroup.keywords,
            false, /* isMaster */
            true /* logResult */
         );
         masterDarkPath = masterDark ? masterDark.fileItems[ 0 ].filePath : "";
         console.noteln( "Master Dark automatic match" );
         this.processLogger.addMessage( 'Master Dark automatic match.' )
      }
   }
   else
   {
      this.processLogger.addMessage( 'Master Dark manually disabled.' )
   }
   let masterDarkEnabled = !isEmptyString( masterDarkPath );

   // -------------------------------
   // search the matching MASTER FLAT
   // -------------------------------
   // flats are enabled only when calibrating light frames
   let masterFlat = undefined;
   let masterFlatPath = "";
   if ( !frameGroup.forceNoFlat )
   {
      if ( frameGroup.overrideFlat )
      {
         masterFlat = frameGroup.overrideFlat;
         masterFlatPath = frameGroup.overrideFlat.fileItems[ 0 ].filePath;
         console.noteln( "Master Flat manually assigned" );
         this.processLogger.addMessage( 'Master Flat manually assigned.' )
      }
      else if ( frameGroup.imageType == ImageType.LIGHT )
      {
         masterFlat = this.getMasterFlatGroup(
            frameGroup.binning,
            frameGroup.filter,
            frameGroup.isCFA,
            true, /* isMaster */
            frameGroup.keywords );
         masterFlatPath = masterFlat ? masterFlat.fileItems[ 0 ].filePath : "";
         console.noteln( "Master Flat automatic match" );
         this.processLogger.addMessage( 'Master Flat automatic match.' )
      }
   }
   else
   {
      console.noteln( "Master Flat manually disabled" );
      this.processLogger.addMessage( 'Master Flat manually disabled.' )
   }
   let masterFlatEnabled = !isEmptyString( masterFlatPath );

   // LOG
   this.processLogger.addMessage( '<ul>' )
   if ( masterBiasEnabled )
   {
      console.noteln( " Master bias: " + masterBiasPath );
      this.processLogger.addMessage( "<li> Master bias: " + masterBiasPath + '</li>' );
   }
   else
      this.processLogger.addMessage( "<li> Master bias: none</li>" );

   if ( masterDarkEnabled )
   {
      console.noteln( "* Master dark: " + masterDarkPath );
      this.processLogger.addMessage( "<li> Master dark: " + masterDarkPath + '</li>' );
   }
   else
      this.processLogger.addMessage( "<li> Master dark: none</li>" );

   if ( masterFlatEnabled )
   {
      console.noteln( "* Master flat: " + masterFlatPath );
      this.processLogger.addMessage( "<li> Master flat: " + masterFlatPath + '</li>' );
   }
   else
      this.processLogger.addMessage( "<li> Master flat: none</li>" );

   if ( !masterBiasEnabled && !masterDarkEnabled && !masterFlatEnabled )
   {
      // no master files provided, calibratio is skipped
      frameGroup.fileItems.map( item => item.filePath ).forEach( filePath => calibratedImages.push( [ filePath ] ) );
      console.warningln( " ** Warning: Image Calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      this.processLogger.addWarning( "Image Calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      return undefined; /* mark the calibration skipped by returning undefined */
   }

   if ( frameGroup.optimizeMasterDark )
   {
      this.processLogger.addMessage( "Master Dark is optimized." );
   }

   // PREPARE CALIBRATION

   let IC = new ImageCalibration;

   IC.enableCFA = frameGroup.isCFA
   if ( frameGroup.isCFA )
      IC.cfaPattern = frameGroup.CFAPattern; // ### N.B. Debayer and IC define compatible enumerated parameters for CFA patterns
   IC.inputHints = this.inputHints();
   IC.outputHints = this.outputHints();
   IC.targetFrames = enableTargetFrames( activeFrames.map( item => item.current ), 2 );
   IC.masterBiasEnabled = false;
   IC.masterDarkEnabled = false;
   IC.masterFlatEnabled = false;
   IC.calibrateBias = true; // relevant if we define overscan areas
   IC.calibrateDark = masterDark ? masterDark.containsBias : false; // ### warning - if false, this deviates from our recommended workflow
   IC.calibrateFlat = false; // assume we have calibrated each individual flat frame
   IC.optimizeDarks = frameGroup.optimizeMasterDark;
   IC.darkOptimizationLow = this.darkOptimizationLow;
   IC.darkOptimizationWindow = this.darkOptimizationWindow;
   IC.separateCFAFlatScalingFactors = masterFlat ? masterFlat.separateCFAFlatScalingFactors : false;
   IC.flatScaleClippingFactor = 0.05;
   IC.outputExtension = ".xisf";
   IC.outputPrefix = "";
   IC.outputPostfix = "_c";
   IC.evaluateNoise = this.evaluateNoise && frameGroup.imageType == ImageType.LIGHT && !frameGroup.isCFA; // for CFAs, evaluate noise after debayer
   IC.outputSampleFormat = ImageCalibration.prototype.f32;
   IC.overwriteExistingFiles = true;
   IC.onError = ImageCalibration.prototype.Continue;

   if ( frameGroup.imageType == ImageType.LIGHT )
   {
      let lightOutputPedestalLogMessage = format( "Light Output Pedestal: %.0f", frameGroup.lightOutputPedestal );
      this.processLogger.addMessage( lightOutputPedestalLogMessage );
      console.noteln( lightOutputPedestalLogMessage );
      IC.outputPedestal = frameGroup.lightOutputPedestal;
   }

   if ( this.overscan.enabled )
   {
      IC.overscanEnabled = true;
      IC.overscanImageX0 = this.overscan.imageRect.x0;
      IC.overscanImageY0 = this.overscan.imageRect.y0;
      IC.overscanImageX1 = this.overscan.imageRect.x1;
      IC.overscanImageY1 = this.overscan.imageRect.y1;
      IC.overscanRegions = [ // enabled, sourceX0, sourceY0, sourceX1, sourceY1, targetX0, targetY0, targetX1, targetY1
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ]
      ];
      for ( let i = 0; i < 4; ++i )
         if ( this.overscan.overscan[ i ].enabled )
         {
            IC.overscanRegions[ i ][ 0 ] = true;
            IC.overscanRegions[ i ][ 1 ] = this.overscan.overscan[ i ].sourceRect.x0;
            IC.overscanRegions[ i ][ 2 ] = this.overscan.overscan[ i ].sourceRect.y0;
            IC.overscanRegions[ i ][ 3 ] = this.overscan.overscan[ i ].sourceRect.x1;
            IC.overscanRegions[ i ][ 4 ] = this.overscan.overscan[ i ].sourceRect.y1;
            IC.overscanRegions[ i ][ 5 ] = this.overscan.overscan[ i ].targetRect.x0;
            IC.overscanRegions[ i ][ 6 ] = this.overscan.overscan[ i ].targetRect.y0;
            IC.overscanRegions[ i ][ 7 ] = this.overscan.overscan[ i ].targetRect.x1;
            IC.overscanRegions[ i ][ 8 ] = this.overscan.overscan[ i ].targetRect.y1;
         }
   }

   // Set master files
   IC.masterBiasEnabled = masterBiasEnabled;
   IC.masterBiasPath = masterBiasPath

   IC.masterDarkEnabled = masterDarkEnabled;
   IC.masterDarkPath = masterDarkPath;

   IC.masterFlatEnabled = masterFlatEnabled;
   IC.masterFlatPath = masterFlatPath;

   // Set output directories
   if ( frameGroup.imageType == ImageType.FLAT )
      IC.outputDirectory = existingDirectory( this.outputDirectory + "/calibrated/flat" );
   else if ( frameGroup.imageType == ImageType.LIGHT )
      IC.outputDirectory = existingDirectory( this.outputDirectory + "/calibrated/light" );

   // execute calibration
   let ok = IC.executeGlobal();

   if ( ok )
   {
      calibratedImages = IC.outputData.map( outputItem => outputItem[ 0 ] );
   }
   else
   {
      console.warningln( " ** Warning: Image Calibration failed." );
      this.processLogger.addWarning( "Image Calibration failed." );
   }

   processEvents();
   gc();

   console.noteln( "<end><cbr><br>",
      "************************************************************" );
   console.noteln( "* End calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( "************************************************************" );

   return calibratedImages;
};

// ----------------------------------------------------------------------------

/**
 * Cleans up elements that has been deallocated but that are still in the groups
 * or file items lists.
 *
 */
StackEngine.prototype.removePurgedElements = function()
{
   let groups = this.groupsManager.groups;
   for ( let i = groups.length; --i >= 0; )
   {
      if ( !groups[ i ] || groups[ i ].__purged__ )
         this.groupsManager.removeGroupAtIndex( i );
      else
      {
         for ( let j = groups[ i ].fileItems.length; --j >= 0; )
            if ( !groups[ i ].fileItems[ j ] || groups[ i ].fileItems[ j ].__purged__ )
               groups[ i ].removeItem( j );
         if ( groups[ i ].fileItems.length == 0 )
            this.groupsManager.removeGroupAtIndex( i );
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Encode the JSON string of the list of groups.
 *
 * @returns
 */
StackEngine.prototype.groupsToStringData = function()
{
   // save files structure
   return JSON.stringify( this.groupsManager.groups, null, 2 );
};

// ----------------------------------------------------------------------------

/**
 * Decode the list of groups from a JSON string.
 *
 * @param {*} data
 */
StackEngine.prototype.groupsFromStringData = function( data, version )
{
   try
   {
      let groupsData = JSON.parse( data );
      if ( groupsData )
      {
         this.removePurgedElements();
         this.groupsManager.groups = this.migrateGroupsData( groupsData, version );
         this.reconstructGroups();
      }
   }
   catch ( e )
   {
      console.noteln( "Error occurred while loading saved groups. Group list will be cleared." );
      this.groupsManager.clear();
   }
};

// ----------------------------------------------------------------------------

/**
 * Migrates old data versions to the current version
 *
 * @param {*} groupsData groups data to be migrated
 */
StackEngine.prototype.migrateGroupsData = function( groupsData, version )
{
   let migrated = groupsData;

   // ---------------------------
   // version [2.0.2], version is not saved
   if ( !version )
   {
      console.noteln( "GROUPS DATA MIGRATION FROM WBPP v2.0.2" );
      // all the existing groups that don't have the mode have to be considered
      // pre-processing groups
      migrated = migrated.map( group =>
      {
         // pre-processing mode
         group.mode = WBPPGroupingMode.PRE;
         // id needs to be updated since it contains the mode as postfix
         group.id = group.id + "_" + WBPPGroupingMode.PRE;
         return group;
      } )
   }

   return migrated;
}

// ----------------------------------------------------------------------------

StackEngine.prototype.migrateKeywords = function( keywords, version )
{

   let migrated = keywords;

   // ---------------------------
   // version [2.0.2], keywords are saved as an array of strings
   if ( version == undefined )
   {
      // assign the saved keywords to pre-process mode
      migrated = migrated.map( k => new Keyword( k, WBPPKeywordMode.PRE ) )
   }

   return migrated;
}

// ----------------------------------------------------------------------------

/**
 * Load the persisted WBPP settings.
 *
 * @returns
 */
StackEngine.prototype.loadSettings = function()
{
   function load( key, type )
   {
      return Settings.read( SETTINGS_KEY_BASE + key, type );
   }

   function loadIndexed( key, index, type )
   {
      return load( key + '_' + index.toString(), type );
   }

   let o;

   if ( ( o = load( "outputDirectory", DataType_String ) ) != null )
      this.outputDirectory = o;
   if ( ( o = load( "saveFrameGroups", DataType_Boolean ) ) != null )
      this.saveFrameGroups = o;
   if ( ( o = load( "upBottomFITS", DataType_Boolean ) ) != null )
      this.upBottomFITS = o;
   if ( ( o = load( "detectMasterIncludingFullPath", DataType_Boolean ) ) != null )
      this.detectMasterIncludingFullPath = o;
   if ( ( o = load( "saveProcessLog", DataType_Boolean ) ) != null )
      this.saveProcessLog = o;
   if ( ( o = load( "generateRejectionMaps", DataType_Boolean ) ) != null )
      this.generateRejectionMaps = o;
   if ( ( o = load( "groupingKeywordsEnabled", DataType_Boolean ) ) != null )
      this.groupingKeywordsEnabled = o;

   if ( ( o = load( "darkOptimizationThreshold", DataType_Float ) ) != null )
      this.darkOptimizationThreshold = o;
   if ( ( o = load( "darkOptimizationLow", DataType_Float ) ) != null )
      this.darkOptimizationLow = o;
   if ( ( o = load( "darkOptimizationWindow", DataType_Int32 ) ) != null )
      this.darkOptimizationWindow = o;
   if ( ( o = load( "darkExposureTolerance", DataType_Float ) ) != null )
      this.darkExposureTolerance = o;
   if ( ( o = load( "lightExposureTolerance", DataType_Float ) ) != null )
      this.lightExposureTolerance = o;
   if ( ( o = load( "evaluateNoise", DataType_Boolean ) ) != null )
      this.evaluateNoise = o;

   if ( ( o = load( "overscanEnabled", DataType_Boolean ) ) != null )
      this.overscan.enabled = o;
   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "overscanRegionEnabled", i, DataType_Boolean ) ) != null )
         this.overscan.overscan[ i ].enabled = o;
      if ( ( o = loadIndexed( "overscanSourceX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x0 = o;
      if ( ( o = loadIndexed( "overscanSourceY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y0 = o;
      if ( ( o = loadIndexed( "overscanSourceX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x1 = o;
      if ( ( o = loadIndexed( "overscanSourceY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y1 = o;
      if ( ( o = loadIndexed( "overscanTargetX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x0 = o;
      if ( ( o = loadIndexed( "overscanTargetY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y0 = o;
      if ( ( o = loadIndexed( "overscanTargetX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x1 = o;
      if ( ( o = loadIndexed( "overscanTargetY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y1 = o;
   }
   if ( ( o = load( "overscanImageX0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x0 = o;
   if ( ( o = load( "overscanImageY0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y0 = o;
   if ( ( o = load( "overscanImageX1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x1 = o;
   if ( ( o = load( "overscanImageY1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y1 = o;

   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "combination", i, DataType_Int32 ) ) != null )
         this.combination[ i ] = o;
      if ( ( o = loadIndexed( "rejection", i, DataType_Int32 ) ) != null )
         this.rejection[ i ] = o;
      // compatibility from PI 1.8.7 and above
      if ( this.rejection[ i ] == ImageIntegration.CCDClip )
         this.rejection[ i ] = ImageIntegration.auto;
      if ( ( o = loadIndexed( "minMaxLow", i, DataType_Int32 ) ) != null )
         this.minMaxLow[ i ] = o;
      if ( ( o = loadIndexed( "minMaxHigh", i, DataType_Int32 ) ) != null )
         this.minMaxHigh[ i ] = o;
      if ( ( o = loadIndexed( "percentileLow", i, DataType_Float ) ) != null )
         this.percentileLow[ i ] = o;
      if ( ( o = loadIndexed( "percentileHigh", i, DataType_Float ) ) != null )
         this.percentileHigh[ i ] = o;
      if ( ( o = loadIndexed( "sigmaLow", i, DataType_Float ) ) != null )
         this.sigmaLow[ i ] = o;
      if ( ( o = loadIndexed( "sigmaHigh", i, DataType_Float ) ) != null )
         this.sigmaHigh[ i ] = o;
      if ( ( o = loadIndexed( "linearFitLow", i, DataType_Float ) ) != null )
         this.linearFitLow[ i ] = o;
      if ( ( o = loadIndexed( "linearFitHigh", i, DataType_Float ) ) != null )
         this.linearFitHigh[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Outliers", i, DataType_Float ) ) != null )
         this.ESD_Outliers[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Significance", i, DataType_Float ) ) != null )
         this.ESD_Significance[ i ] = o;
      if ( ( o = loadIndexed( "ESD_LowRelaxation", i, DataType_Float ) ) != null )
         this.ESD_LowRelaxation[ i ] = o;
   }

   if ( ( o = load( "flatsLargeScaleRejection", DataType_Boolean ) ) != null )
      this.flatsLargeScaleRejection = o;
   if ( ( o = load( "flatsLargeScaleRejectionLayers", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionLayers = o;
   if ( ( o = load( "flatsLargeScaleRejectionGrowth", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionGrowth = o;
   if ( ( o = load( "lightsLargeScaleRejectionHigh", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLow", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthLow = o;
   if ( ( o = load( "imageRegistration", DataType_Boolean ) ) != null )
      this.imageRegistration = o;
   if ( ( o = load( "generateDrizzleData", DataType_Boolean ) ) != null )
      this.generateDrizzleData = o;

   if ( ( o = load( "linearPatternSubtraction", DataType_Boolean ) ) != null )
      this.linearPatternSubtraction = o;
   if ( ( o = load( "linearPatternSubtractionRejectionLimit", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionRejectionLimit = o;
   if ( ( o = load( "linearPatternSubtractionMode", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionMode = o;

   if ( ( o = load( "subframeWeightingPreset", DataType_Int32 ) ) != null )
      this.subframeWeightingPreset = o;
   if ( ( o = load( "generateSubframesWeights", DataType_Boolean ) ) != null )
      this.generateSubframesWeights = o;
   if ( ( o = load( "forceMeasurement", DataType_Boolean ) ) != null )
      this.forceMeasurement = o;
   if ( ( o = load( "useSubframeSelectorProcess", DataType_Boolean ) ) != null )
      this.useSubframeSelectorProcess = o;
   if ( ( o = load( "FWHMWeight", DataType_Int32 ) ) != null )
      this.FWHMWeight = o;
   if ( ( o = load( "eccentricityWeight", DataType_Int32 ) ) != null )
      this.eccentricityWeight = o;
   if ( ( o = load( "SNRWeight", DataType_Int32 ) ) != null )
      this.SNRWeight = o;
   if ( ( o = load( "pedestal", DataType_Int32 ) ) != null )
      this.pedestal = o;

   if ( ( o = load( "pixelInterpolation", DataType_Int32 ) ) != null )
      this.pixelInterpolation = o;
   if ( ( o = load( "clampingThreshold", DataType_Float ) ) != null )
      this.clampingThreshold = o;
   if ( ( o = load( "maxStars", DataType_Int32 ) ) != null )
      this.maxStars = o;
   if ( ( o = load( "distortionCorrection", DataType_Boolean ) ) != null )
      this.distortionCorrection = o;
   if ( ( o = load( "structureLayers", DataType_Int32 ) ) != null )
      this.structureLayers = o;
   if ( ( o = load( "minStructureSize", DataType_Int32 ) ) != null )
      this.minStructureSize = o;
   if ( ( o = load( "noiseReductionFilterRadius", DataType_Int32 ) ) != null )
      this.noiseReductionFilterRadius = o;
   if ( ( o = load( "sensitivity", DataType_Float ) ) != null )
      this.sensitivity = o;
   if ( ( o = load( "peakResponse", DataType_Float ) ) != null )
      this.peakResponse = o;
   if ( ( o = load( "maxStarDistortion", DataType_Float ) ) != null )
      this.maxStarDistortion = o;
   if ( ( o = load( "useTriangleSimilarity", DataType_Boolean ) ) != null )
      this.useTriangleSimilarity = o;
   if ( ( o = load( "integrate", DataType_Boolean ) ) != null )
      this.integrate = o;

   if ( ( o = load( "referenceImage", DataType_String ) ) != null )
      this.referenceImage = o;
   if ( ( o = load( "bestFrameReferenceKeyword", DataType_String ) ) != null )
      this.bestFrameReferenceKeyword = o;
   if ( ( o = load( "bestFrameRefernceMethod", DataType_Int32 ) ) != null )
      this.bestFrameRefernceMethod = o;

   let dataVersion = undefined;
   if ( ( o = load( "VERSION", DataType_String ) ) != null )
      dataVersion = o;
   if ( ( o = load( "keywords", DataType_String ) ) != null )
   {
      let keywords = JSON.parse( o );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }
   if ( this.saveFrameGroups )
   {
      if ( ( o = load( "groups", DataType_String ) ) != null )
         this.groupsFromStringData( o, dataVersion );
      else if ( ( o = load( "frameGroups", DataType_String ) ) != null ) /* WBPP 2.0.2 */
         this.groupsFromStringData( o, dataVersion );
   }
};

// ----------------------------------------------------------------------------

/**
 * Persist the WBPP settings.
 *
 */
StackEngine.prototype.saveSettings = function()
{
   function save( key, type, value )
   {
      Settings.write( SETTINGS_KEY_BASE + key, type, value );
   }

   function saveIndexed( key, index, type, value )
   {
      save( key + '_' + index.toString(), type, value );
   }

   save( "outputDirectory", DataType_String, this.outputDirectory );
   save( "saveFrameGroups", DataType_Boolean, this.saveFrameGroups );
   save( "detectMasterIncludingFullPath", DataType_Boolean, this.detectMasterIncludingFullPath );
   save( "upBottomFITS", DataType_Boolean, this.upBottomFITS );
   save( "saveProcessLog", DataType_Boolean, this.saveProcessLog );
   save( "generateRejectionMaps", DataType_Boolean, this.generateRejectionMaps );
   save( "groupingKeywordsEnabled", DataType_Boolean, this.groupingKeywordsEnabled );
   save( "darkOptimizationLow", DataType_Float, this.darkOptimizationLow );
   save( "darkOptimizationWindow", DataType_Int32, this.darkOptimizationWindow );
   save( "darkExposureTolerance", DataType_Float, this.darkExposureTolerance );
   save( "lightExposureTolerance", DataType_Float, this.lightExposureTolerance );
   save( "evaluateNoise", DataType_Boolean, this.evaluateNoise );

   save( "overscanEnabled", DataType_Boolean, this.overscan.enabled );
   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "overscanRegionEnabled", i, DataType_Boolean, this.overscan.overscan[ i ].enabled );
      saveIndexed( "overscanSourceX0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x0 );
      saveIndexed( "overscanSourceY0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y0 );
      saveIndexed( "overscanSourceX1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x1 );
      saveIndexed( "overscanSourceY1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y1 );
      saveIndexed( "overscanTargetX0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x0 );
      saveIndexed( "overscanTargetY0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y0 );
      saveIndexed( "overscanTargetX1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x1 );
      saveIndexed( "overscanTargetY1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y1 );
   }
   save( "overscanImageX0", DataType_Int32, this.overscan.imageRect.x0 );
   save( "overscanImageY0", DataType_Int32, this.overscan.imageRect.y0 );
   save( "overscanImageX1", DataType_Int32, this.overscan.imageRect.x1 );
   save( "overscanImageY1", DataType_Int32, this.overscan.imageRect.y1 );

   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "combination", i, DataType_Int32, this.combination[ i ] );
      saveIndexed( "rejection", i, DataType_Int32, this.rejection[ i ] );
      saveIndexed( "minMaxLow", i, DataType_Int32, this.minMaxLow[ i ] );
      saveIndexed( "minMaxHigh", i, DataType_Int32, this.minMaxHigh[ i ] );
      saveIndexed( "percentileLow", i, DataType_Float, this.percentileLow[ i ] );
      saveIndexed( "percentileHigh", i, DataType_Float, this.percentileHigh[ i ] );
      saveIndexed( "sigmaLow", i, DataType_Float, this.sigmaLow[ i ] );
      saveIndexed( "sigmaHigh", i, DataType_Float, this.sigmaHigh[ i ] );
      saveIndexed( "linearFitLow", i, DataType_Float, this.linearFitLow[ i ] );
      saveIndexed( "linearFitHigh", i, DataType_Float, this.linearFitHigh[ i ] );
      saveIndexed( "ESD_Outliers", i, DataType_Float, this.ESD_Outliers[ i ] );
      saveIndexed( "ESD_Significance", i, DataType_Float, this.ESD_Significance[ i ] );
      saveIndexed( "ESD_LowRelaxation", i, DataType_Float, this.ESD_LowRelaxation[ i ] );
   }

   save( "flatsLargeScaleRejection", DataType_Boolean, this.flatsLargeScaleRejection );
   save( "flatsLargeScaleRejectionLayers", DataType_Int32, this.flatsLargeScaleRejectionLayers );
   save( "flatsLargeScaleRejectionGrowth", DataType_Int32, this.flatsLargeScaleRejectionGrowth );
   save( "lightsLargeScaleRejectionHigh", DataType_Boolean, this.lightsLargeScaleRejectionHigh );
   save( "lightsLargeScaleRejectionLayersHigh", DataType_Int32, this.lightsLargeScaleRejectionLayersHigh );
   save( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32, this.lightsLargeScaleRejectionGrowthHigh );
   save( "lightsLargeScaleRejectionLow", DataType_Boolean, this.lightsLargeScaleRejectionLow );
   save( "lightsLargeScaleRejectionLayersLow", DataType_Int32, this.lightsLargeScaleRejectionLayersLow );
   save( "lightsLargeScaleRejectionGrowthLow", DataType_Int32, this.lightsLargeScaleRejectionGrowthLow );
   save( "imageRegistration", DataType_Boolean, this.imageRegistration );
   save( "generateDrizzleData", DataType_Boolean, this.generateDrizzleData );
   save( "pixelInterpolation", DataType_Int32, this.pixelInterpolation );
   save( "clampingThreshold", DataType_Float, this.clampingThreshold );
   save( "maxStars", DataType_Int32, this.maxStars );
   save( "distortionCorrection", DataType_Boolean, this.distortionCorrection );

   save( "linearPatternSubtraction", DataType_Boolean, this.linearPatternSubtraction );
   save( "linearPatternSubtractionRejectionLimit", DataType_Int32, this.linearPatternSubtractionRejectionLimit );
   save( "linearPatternSubtractionMode", DataType_Int32, this.linearPatternSubtractionMode );

   save( "subframeWeightingPreset", DataType_Int32, this.subframeWeightingPreset );
   save( "generateSubframesWeights", DataType_Boolean, this.generateSubframesWeights );
   save( "forceMeasurement", DataType_Boolean, this.forceMeasurement );
   save( "useSubframeSelectorProcess", DataType_Boolean, this.useSubframeSelectorProcess );
   save( "FWHMWeight", DataType_Int32, this.FWHMWeight );
   save( "eccentricityWeight", DataType_Int32, this.eccentricityWeight );
   save( "SNRWeight", DataType_Int32, this.SNRWeight );
   save( "pedestal", DataType_Int32, this.pedestal );
   save( "structureLayers", DataType_Int32, this.structureLayers );
   save( "minStructureSize", DataType_Int32, this.minStructureSize );
   save( "noiseReductionFilterRadius", DataType_Int32, this.noiseReductionFilterRadius );
   save( "sensitivity", DataType_Float, this.sensitivity );
   save( "peakResponse", DataType_Float, this.peakResponse );
   save( "maxStarDistortion", DataType_Float, this.maxStarDistortion );
   save( "useTriangleSimilarity", DataType_Boolean, this.useTriangleSimilarity );
   save( "integrate", DataType_Boolean, this.integrate );

   save( "referenceImage", DataType_String, this.referenceImage );
   save( "bestFrameReferenceKeyword", DataType_String, this.bestFrameReferenceKeyword );
   save( "bestFrameRefernceMethod", DataType_Int32, this.bestFrameRefernceMethod );

   save( "VERSION", DataType_String, VERSION );

   save( "keywords", DataType_String, JSON.stringify( this.keywords.list ) );
   if ( this.saveFrameGroups )
      save( "groups", DataType_String, this.groupsToStringData() );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.setDefaultParameters = function()
{
   setDefaultParameters.apply( this );
};

// ----------------------------------------------------------------------------

/**
 * Sets the WBPP's engine default parameters.
 *
 */
function setDefaultParameters()
{
   // General options
   this.detectMasterIncludingFullPath = DEFAULT_MASTER_DETECTION_USES_FULL_PATH;
   this.saveFrameGroups = DEFAULT_SAVE_FRAME_GROUPS;
   this.outputDirectory = DEFAULT_OUTPUT_DIRECTORY;
   this.upBottomFITS = DEFAULT_UP_BOTTOM_FITS;
   this.saveProcessLog = DEFAULT_SAVE_PROCESS_LOG;
   this.generateRejectionMaps = DEFAULT_GENERATE_REJECTION_MAPS;
   this.groupingKeywordsEnabled = DEFAULT_GROUPING_KEYWORDS_ACTIVE;

   // Calibration parameters
   this.darkOptimizationThreshold = 0; // ### deprecated - retained for compatibility
   this.darkOptimizationLow = DEFAULT_DARK_OPTIMIZATION_LOW; // in sigma units from the central value
   this.darkOptimizationWindow = DEFAULT_DARK_OPTIMIZATION_WINDOW;
   this.darkExposureTolerance = DEFAULT_DARK_EXPOSURE_TOLERANCE; // in seconds
   this.evaluateNoise = DEFAULT_EVALUATE_NOISE;

   // Image integration parameters
   this.flatsLargeScaleRejection = DEFAULT_LARGE_SCALE_REJECTION;
   this.flatsLargeScaleRejectionLayers = DEFAULT_LARGE_SCALE_LAYERS;
   this.flatsLargeScaleRejectionGrowth = DEFAULT_LARGE_SCALE_GROWTH;
   this.lightsLargeScaleRejectionHigh = DEFAULT_LARGE_SCALE_REJECTION;
   this.lightsLargeScaleRejectionLayersHigh = DEFAULT_LARGE_SCALE_LAYERS;
   this.lightsLargeScaleRejectionGrowthHigh = DEFAULT_LARGE_SCALE_GROWTH;
   this.lightsLargeScaleRejectionLow = DEFAULT_LARGE_SCALE_REJECTION;
   this.lightsLargeScaleRejectionLayersLow = DEFAULT_LARGE_SCALE_LAYERS;
   this.lightsLargeScaleRejectionGrowthLow = DEFAULT_LARGE_SCALE_GROWTH;

   // Overscan
   this.overscan.enabled = false;
   for ( let i = 0; i < 4; ++i )
   {
      this.overscan.overscan[ i ].enabled = false;
      this.overscan.overscan[ i ].sourceRect.assign( 0 );
      this.overscan.overscan[ i ].targetRect.assign( 0 );
   }
   this.overscan.imageRect.assign( 0 );

   for ( let i = 0; i < 4; ++i )
   {
      this.combination[ i ] = ImageIntegration.prototype.Average;
      this.rejection[ i ] = DEFAULT_REJECTION_METHOD;
      this.minMaxLow[ i ] = 1;
      this.minMaxHigh[ i ] = 1;
      this.percentileLow[ i ] = 0.2;
      this.percentileHigh[ i ] = 0.1;
      this.sigmaLow[ i ] = 4.0;
      this.sigmaHigh[ i ] = 3.0;
      this.linearFitLow[ i ] = 5.0;
      this.linearFitHigh[ i ] = 3.5;
      this.ESD_Outliers[ i ] = 0.3;
      this.ESD_Significance[ i ] = 0.05;
      this.ESD_LowRelaxation[ i ] = 1.5;
   }

   // Light
   this.lightExposureTolerance = DEFAULT_LIGHT_EXPOSURE_TOLERANCE; // in seconds
   this.imageRegistration = DEFAULT_IMAGE_REGISTRATION;
   this.generateDrizzleData = DEFAULT_GENERATE_DRIZZLE_DATA;

   // Linear Pattern Subtraction
   this.linearPatternSubtraction = DEFAULT_LINEAR_PATTERN_SUBTRACTION;
   this.linearPatternSubtractionRejectionLimit = DEFAULT_LINEAR_PATTERN_SUBTRACTION_SIGMA;
   this.linearPatternSubtractionMode = DEFAULT_LINEAR_PATTERN_SUBTRACTION_MODE;

   // Cosmetic correction
   this.cosmeticCorrection = DEFAULT_COSMETIC_CORRECTION;
   this.cosmeticCorrectionTemplateId = DEFAULT_COSMETIC_CORRECTION_TEMPLATE;

   this.subframeWeightingPreset = DEFAULT_SUBFRAMEWEIGHTING_PRESET;
   this.generateSubframesWeights = DEFAULT_SUBFRAMEWEIGHTING_GENERATE;
   this.forceMeasurement = DEFAULT_SUBFRAMEWEIGHTING_FORCE_MEASUREMENT;
   this.useSubframeSelectorProcess = DEFAULT_USE_SUBFRAMESELECTOR_PROCESS;

   this.FWHMWeight = DEFAULT_SUBFRAMEWEIGHTING_FWHM_WEIGHT;
   this.eccentricityWeight = DEFAULT_SUBFRAMEWEIGHTING_ECCENTRICITY_WEIGHT;
   this.SNRWeight = DEFAULT_SUBFRAMEWEIGHTING_SNR_WEIGHT;
   this.pedestal = DEFAULT_SUBFRAMEWEIGHTING_PEDESTAL;

   this.pixelInterpolation = DEFAULT_SA_PIXEL_INTERPOLATION;
   this.clampingThreshold = DEFAULT_SA_CLAMPING_THRESHOLD;
   this.maxStars = DEFAULT_SA_MAX_STARS;
   this.distortionCorrection = DEFAULT_SA_DISTORTION_CORRECTION;
   this.structureLayers = DEFAULT_SA_STRUCTURE_LAYERS;
   this.minStructureSize = DEFAULT_SA_MIN_STRUCTURE_SIZE;
   this.noiseReductionFilterRadius = DEFAULT_SA_NOISE_REDUCTION;
   this.sensitivity = DEFAULT_SA_SENSITIVITY;
   this.peakResponse = DEFAULT_SA_PEAK_RESPONSE;
   this.maxStarDistortion = DEFAULT_SA_MAX_STAR_DISTORTION;
   this.useTriangleSimilarity = DEFAULT_SA_USE_TRIANGLE_SIMILARITY;
   this.referenceImage = "";
   this.bestFrameReferenceKeyword = "";
   this.bestFrameRefernceMethod = DEFAULT_BEST_REFERENCE_METHOD;

   this.integrate = DEFAULT_INTEGRATE;

   this.viewMode = WBPPGroupingMode.PRE;

   this.keywords = new Keywords();
};

// ----------------------------------------------------------------------------

/**
 * Import the WBPP settings from an instance.
 *
 */
StackEngine.prototype.importParameters = function()
{
   let
   {
      parameters
   } = WBPPUtils.shared();

   this.setDefaultParameters();
   this.loadSettings();

   if ( Parameters.has( "saveFrameGroups" ) )
      this.saveFrameGroups = Parameters.getBoolean( "saveFrameGroups" );

   if ( Parameters.has( "detectMasterIncludingFullPath" ) )
      this.detectMasterIncludingFullPath = Parameters.getBoolean( "detectMasterIncludingFullPath" );

   if ( Parameters.has( "outputDirectory" ) )
      this.outputDirectory = Parameters.getString( "outputDirectory" );

   if ( Parameters.has( "upBottomFITS" ) )
      this.upBottomFITS = Parameters.getBoolean( "upBottomFITS" );

   if ( Parameters.has( "saveProcessLog" ) )
      this.saveProcessLog = Parameters.getBoolean( "saveProcessLog" );

   if ( Parameters.has( "generateRejectionMaps" ) )
      this.generateRejectionMaps = Parameters.getBoolean( "generateRejectionMaps" );

   if ( Parameters.has( "groupingKeywordsEnabled" ) )
      this.groupingKeywordsEnabled = Parameters.getBoolean( "groupingKeywordsEnabled" );

   if ( Parameters.has( "darkOptimizationThreshold" ) )
      this.darkOptimizationThreshold = Parameters.getReal( "darkOptimizationThreshold" );

   if ( Parameters.has( "darkOptimizationLow" ) )
      this.darkOptimizationLow = Parameters.getReal( "darkOptimizationLow" );

   if ( Parameters.has( "darkOptimizationWindow" ) )
      this.darkOptimizationWindow = Parameters.getInteger( "darkOptimizationWindow" );

   if ( Parameters.has( "darkExposureTolerance" ) )
      this.darkExposureTolerance = Parameters.getReal( "darkExposureTolerance" );

   if ( Parameters.has( "lightExposureTolerance" ) )
      this.lightExposureTolerance = Parameters.getReal( "lightExposureTolerance" );

   if ( Parameters.has( "evaluateNoise" ) )
      this.evaluateNoise = Parameters.getBoolean( "evaluateNoise" );

   if ( Parameters.has( "overscanEnabled" ) )
      this.overscan.enabled = Parameters.getBoolean( "overscanEnabled" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( Parameters.has( "overscanRegionEnabled" ) )
         this.overscan.overscan[ i ].enabled = parameters.getBooleanIndexed( "overscanRegionEnabled", i );

      if ( Parameters.has( "overscanSourceX0" ) )
         this.overscan.overscan[ i ].sourceRect.x0 = parameters.getIntegerIndexed( "overscanSourceX0", i );

      if ( Parameters.has( "overscanSourceY0" ) )
         this.overscan.overscan[ i ].sourceRect.y0 = parameters.getIntegerIndexed( "overscanSourceY0", i );

      if ( Parameters.has( "overscanSourceX1" ) )
         this.overscan.overscan[ i ].sourceRect.x1 = parameters.getIntegerIndexed( "overscanSourceX1", i );

      if ( Parameters.has( "overscanSourceY1" ) )
         this.overscan.overscan[ i ].sourceRect.y1 = parameters.getIntegerIndexed( "overscanSourceY1", i );

      if ( Parameters.has( "overscanTargetX0" ) )
         this.overscan.overscan[ i ].targetRect.x0 = parameters.getIntegerIndexed( "overscanTargetX0", i );

      if ( Parameters.has( "overscanTargetY0" ) )
         this.overscan.overscan[ i ].targetRect.y0 = parameters.getIntegerIndexed( "overscanTargetY0", i );

      if ( Parameters.has( "overscanTargetX1" ) )
         this.overscan.overscan[ i ].targetRect.x1 = parameters.getIntegerIndexed( "overscanTargetX1", i );

      if ( Parameters.has( "overscanTargetY1" ) )
         this.overscan.overscan[ i ].targetRect.y1 = parameters.getIntegerIndexed( "overscanTargetY1", i );
   }

   if ( Parameters.has( "overscanImageX0" ) )
      this.overscan.imageRect.x0 = Parameters.getInteger( "overscanImageX0" );

   if ( Parameters.has( "overscanImageY0" ) )
      this.overscan.imageRect.y0 = Parameters.getInteger( "overscanImageY0" );

   if ( Parameters.has( "overscanImageX1" ) )
      this.overscan.imageRect.x1 = Parameters.getInteger( "overscanImageX1" );

   if ( Parameters.has( "overscanImageY1" ) )
      this.overscan.imageRect.y1 = Parameters.getInteger( "overscanImageY1" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( parameters.hasIndexed( "combination", i ) )
         this.combination[ i ] = parameters.getIntegerIndexed( "combination", i );

      if ( parameters.hasIndexed( "rejection", i ) )
         this.rejection[ i ] = parameters.getIntegerIndexed( "rejection", i );

      if ( parameters.hasIndexed( "minMaxLow", i ) )
         this.minMaxLow[ i ] = parameters.getRealIndexed( "minMaxLow", i );

      if ( parameters.hasIndexed( "minMaxHigh", i ) )
         this.minMaxHigh[ i ] = parameters.getRealIndexed( "minMaxHigh", i );

      if ( parameters.hasIndexed( "percentileLow", i ) )
         this.percentileLow[ i ] = parameters.getRealIndexed( "percentileLow", i );

      if ( parameters.hasIndexed( "percentileHigh", i ) )
         this.percentileHigh[ i ] = parameters.getRealIndexed( "percentileHigh", i );

      if ( parameters.hasIndexed( "sigmaLow", i ) )
         this.sigmaLow[ i ] = parameters.getRealIndexed( "sigmaLow", i );

      if ( parameters.hasIndexed( "sigmaHigh", i ) )
         this.sigmaHigh[ i ] = parameters.getRealIndexed( "sigmaHigh", i );

      if ( parameters.hasIndexed( "linearFitLow", i ) )
         this.linearFitLow[ i ] = parameters.getRealIndexed( "linearFitLow", i );

      if ( parameters.hasIndexed( "linearFitHigh", i ) )
         this.linearFitHigh[ i ] = parameters.getRealIndexed( "linearFitHigh", i );

      if ( parameters.hasIndexed( "ESD_Outliers", i ) )
         this.ESD_Outliers[ i ] = parameters.getRealIndexed( "ESD_Outliers", i );

      if ( parameters.hasIndexed( "ESD_Significance", i ) )
         this.ESD_Significance[ i ] = parameters.getRealIndexed( "ESD_Significance", i );

      if ( parameters.hasIndexed( "ESD_LowRelaxation", i ) )
         this.ESD_LowRelaxation[ i ] = parameters.getRealIndexed( "ESD_LowRelaxation", i );
   }

   if ( Parameters.has( "flatsLargeScaleRejection" ) )
      this.flatsLargeScaleRejection = Parameters.getBoolean( "flatsLargeScaleRejection" );

   if ( Parameters.has( "flatsLargeScaleRejectionLayers" ) )
      this.flatsLargeScaleRejectionLayers = Parameters.getInteger( "flatsLargeScaleRejectionLayers" );

   if ( Parameters.has( "flatsLargeScaleRejectionGrowth" ) )
      this.flatsLargeScaleRejectionGrowth = Parameters.getInteger( "flatsLargeScaleRejectionGrowth" );

   if ( Parameters.has( "lightsLargeScaleRejectionHigh" ) )
      this.lightsLargeScaleRejectionHigh = Parameters.getBoolean( "lightsLargeScaleRejectionHigh" );

   if ( Parameters.has( "lightsLargeScaleRejectionLayersHigh" ) )
      this.lightsLargeScaleRejectionLayersHigh = Parameters.getInteger( "lightsLargeScaleRejectionLayersHigh" );

   if ( Parameters.has( "lightsLargeScaleRejectionGrowthHigh" ) )
      this.lightsLargeScaleRejectionGrowthHigh = Parameters.getInteger( "lightsLargeScaleRejectionGrowthHigh" );

   if ( Parameters.has( "lightsLargeScaleRejectionLow" ) )
      this.lightsLargeScaleRejectionLow = Parameters.getBoolean( "lightsLargeScaleRejectionLow" );

   if ( Parameters.has( "lightsLargeScaleRejectionLayersLow" ) )
      this.lightsLargeScaleRejectionLayersLow = Parameters.getInteger( "lightsLargeScaleRejectionLayersLow" );

   if ( Parameters.has( "lightsLargeScaleRejectionGrowthLow" ) )
      this.lightsLargeScaleRejectionGrowthLow = Parameters.getInteger( "lightsLargeScaleRejectionGrowthLow" );

   if ( Parameters.has( "imageRegistration" ) )
      this.imageRegistration = Parameters.getBoolean( "imageRegistration" );

   if ( Parameters.has( "generateDrizzleData" ) )
      this.generateDrizzleData = Parameters.getBoolean( "generateDrizzleData" );

   if ( Parameters.has( "linearPatternSubtraction" ) )
      this.linearPatternSubtraction = Parameters.getBoolean( "linearPatternSubtraction" );

   if ( Parameters.has( "linearPatternSubtractionRejectionLimit" ) )
      this.linearPatternSubtractionRejectionLimit = Parameters.getInteger( "linearPatternSubtractionRejectionLimit" );

   if ( Parameters.has( "linearPatternSubtractionMode" ) )
      this.linearPatternSubtractionMode = Parameters.getInteger( "linearPatternSubtractionMode" );

   if ( Parameters.has( "cosmeticCorrection" ) )
      this.cosmeticCorrection = Parameters.getBoolean( "cosmeticCorrection" );

   if ( Parameters.has( "cosmeticCorrectionTemplateId" ) )
      this.cosmeticCorrectionTemplateId = Parameters.getString( "cosmeticCorrectionTemplateId" );

   if ( Parameters.has( "subframeWeightingPreset" ) )
      this.subframeWeightingPreset = Parameters.getInteger( "subframeWeightingPreset" );

   if ( Parameters.has( "FWHMWeight" ) )
      this.FWHMWeight = Parameters.getInteger( "FWHMWeight" );

   if ( Parameters.has( "eccentricityWeight" ) )
      this.eccentricityWeight = Parameters.getInteger( "eccentricityWeight" );

   if ( Parameters.has( "SNRWeight" ) )
      this.SNRWeight = Parameters.getInteger( "SNRWeight" );

   if ( Parameters.has( "pedestal" ) )
      this.pedestal = Parameters.getInteger( "pedestal" );

   if ( Parameters.has( "generateSubframesWeights" ) )
      this.generateSubframesWeights = Parameters.getBoolean( "generateSubframesWeights" );

   if ( Parameters.has( "forceMeasurement" ) )
      this.forceMeasurement = Parameters.getBoolean( "forceMeasurement" );

   if ( Parameters.has( "useSubframeSelectorProcess" ) )
      this.useSubframeSelectorProcess = Parameters.getBoolean( "useSubframeSelectorProcess" );

   if ( Parameters.has( "pixelInterpolation" ) )
      this.pixelInterpolation = Parameters.getInteger( "pixelInterpolation" );

   if ( Parameters.has( "clampingThreshold" ) )
      this.clampingThreshold = Parameters.getReal( "clampingThreshold" );

   if ( Parameters.has( "maxStars" ) )
      this.maxStars = Parameters.getInteger( "maxStars" );

   if ( Parameters.has( "distortionCorrection" ) )
      this.distortionCorrection = Parameters.getBoolean( "distortionCorrection" );

   if ( Parameters.has( "structureLayers" ) )
      this.structureLayers = Parameters.getInteger( "structureLayers" );

   if ( Parameters.has( "minStructureSize" ) )
      this.minStructureSize = Parameters.getInteger( "minStructureSize" );

   if ( Parameters.has( "noiseReductionFilterRadius" ) )
      this.noiseReductionFilterRadius = Parameters.getInteger( "noiseReductionFilterRadius" );

   if ( Parameters.has( "sensitivity" ) )
      this.sensitivity = Parameters.getReal( "sensitivity" );

   if ( Parameters.has( "peakResponse" ) )
      this.peakResponse = Parameters.getReal( "peakResponse" );

   if ( Parameters.has( "maxStarDistortion" ) )
      this.maxStarDistortion = Parameters.getReal( "maxStarDistortion" );

   if ( Parameters.has( "useTriangleSimilarity" ) )
      this.useTriangleSimilarity = Parameters.getBoolean( "useTriangleSimilarity" );

   if ( Parameters.has( "referenceImage" ) )
      this.referenceImage = Parameters.getString( "referenceImage" );

   if ( Parameters.has( "bestFrameReferenceKeyword" ) )
      this.bestFrameReferenceKeyword = Parameters.getString( "bestFrameReferenceKeyword" );

   if ( Parameters.has( "bestFrameRefernceMethod" ) )
      this.bestFrameRefernceMethod = Parameters.getInteger( "bestFrameRefernceMethod" );

   if ( Parameters.has( "integrate" ) )
      this.integrate = Parameters.getBoolean( "integrate" );

   let dataVersion = undefined;
   if ( Parameters.has( "VERSION" ) )
      dataVersion = Parameters.getString( "VERSION" );

   if ( Parameters.has( "keywords" ) )
   {
      let dataStr = ByteArray.fromBase64( Parameters.getString( "keywords" ) ).toString();
      let keywords = JSON.parse( dataStr );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }

   if ( Parameters.has( "groups" ) )
      this.groupsFromStringData( ByteArray.fromBase64( Parameters.getString( "groups" ) ).toString(), dataVersion );
};

// ----------------------------------------------------------------------------

/**
 * Prepare the export of the WBPP parameters to an instance.
 *
 */
StackEngine.prototype.exportParameters = function()
{
   let
   {
      parameters
   } = WBPPUtils.shared();

   Parameters.clear();

   Parameters.set( "version", VERSION );

   Parameters.set( "saveFrameGroups", this.saveFrameGroups );
   Parameters.set( "detectMasterIncludingFullPath", this.detectMasterIncludingFullPath );
   Parameters.set( "outputDirectory", this.outputDirectory );
   Parameters.set( "upBottomFITS", this.upBottomFITS );
   Parameters.set( "saveProcessLog", this.saveProcessLog );

   Parameters.set( "generateRejectionMaps", this.generateRejectionMaps );
   Parameters.set( "groupingKeywordsEnabled", this.groupingKeywordsEnabled );

   Parameters.set( "darkOptimizationLow", this.darkOptimizationLow );
   Parameters.set( "darkOptimizationWindow", this.darkOptimizationWindow );
   Parameters.set( "darkExposureTolerance", this.darkExposureTolerance );

   Parameters.set( "evaluateNoise", this.evaluateNoise );

   Parameters.set( "overscanEnabled", this.overscan.enabled );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "overscanRegionEnabled", i, this.overscan.overscan[ i ].enabled );
      parameters.setIndexed( "overscanSourceX0", i, this.overscan.overscan[ i ].sourceRect.x0 );
      parameters.setIndexed( "overscanSourceY0", i, this.overscan.overscan[ i ].sourceRect.y0 );
      parameters.setIndexed( "overscanSourceX1", i, this.overscan.overscan[ i ].sourceRect.x1 );
      parameters.setIndexed( "overscanSourceY1", i, this.overscan.overscan[ i ].sourceRect.y1 );
      parameters.setIndexed( "overscanTargetX0", i, this.overscan.overscan[ i ].targetRect.x0 );
      parameters.setIndexed( "overscanTargetY0", i, this.overscan.overscan[ i ].targetRect.y0 );
      parameters.setIndexed( "overscanTargetX1", i, this.overscan.overscan[ i ].targetRect.x1 );
      parameters.setIndexed( "overscanTargetY1", i, this.overscan.overscan[ i ].targetRect.y1 );
   }

   Parameters.set( "overscanImageX0", this.overscan.imageRect.x0 );
   Parameters.set( "overscanImageY0", this.overscan.imageRect.y0 );
   Parameters.set( "overscanImageX1", this.overscan.imageRect.x1 );
   Parameters.set( "overscanImageY1", this.overscan.imageRect.y1 );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "combination", i, this.combination[ i ] );
      parameters.setIndexed( "rejection", i, this.rejection[ i ] );
      parameters.setIndexed( "minMaxLow", i, this.minMaxLow[ i ] );
      parameters.setIndexed( "minMaxHigh", i, this.minMaxHigh[ i ] );
      parameters.setIndexed( "percentileLow", i, this.percentileLow[ i ] );
      parameters.setIndexed( "percentileHigh", i, this.percentileHigh[ i ] );
      parameters.setIndexed( "sigmaLow", i, this.sigmaLow[ i ] );
      parameters.setIndexed( "sigmaHigh", i, this.sigmaHigh[ i ] );
      parameters.setIndexed( "linearFitLow", i, this.linearFitLow[ i ] );
      parameters.setIndexed( "linearFitHigh", i, this.linearFitHigh[ i ] );
      parameters.setIndexed( "ESD_Outliers", i, this.ESD_Outliers[ i ] );
      parameters.setIndexed( "ESD_Significance", i, this.ESD_Significance[ i ] );
      parameters.setIndexed( "ESD_LowRelaxation", i, this.ESD_LowRelaxation[ i ] );
   }

   Parameters.set( "flatsLargeScaleRejection", this.flatsLargeScaleRejection );
   Parameters.set( "flatsLargeScaleRejectionLayers", this.flatsLargeScaleRejectionLayers );
   Parameters.set( "flatsLargeScaleRejectionGrowth", this.flatsLargeScaleRejectionGrowth );

   Parameters.set( "lightsLargeScaleRejectionHigh", this.lightsLargeScaleRejectionHigh );
   Parameters.set( "lightsLargeScaleRejectionLayersHigh", this.lightsLargeScaleRejectionLayersHigh );
   Parameters.set( "lightsLargeScaleRejectionGrowthHigh", this.lightsLargeScaleRejectionGrowthHigh );

   Parameters.set( "lightsLargeScaleRejectionLow", this.lightsLargeScaleRejectionLow );
   Parameters.set( "lightsLargeScaleRejectionLayersLow", this.lightsLargeScaleRejectionLayersLow );
   Parameters.set( "lightsLargeScaleRejectionGrowthLow", this.lightsLargeScaleRejectionGrowthLow );

   Parameters.set( "imageRegistration", this.imageRegistration );
   Parameters.set( "lightExposureTolerance", this.lightExposureTolerance );
   Parameters.set( "generateDrizzleData", this.generateDrizzleData );

   Parameters.set( "linearPatternSubtraction", this.linearPatternSubtraction );
   Parameters.set( "linearPatternSubtractionRejectionLimit", this.linearPatternSubtractionRejectionLimit );
   Parameters.set( "linearPatternSubtractionMode", this.linearPatternSubtractionMode );

   Parameters.set( "cosmeticCorrection", this.cosmeticCorrection );
   Parameters.set( "cosmeticCorrectionTemplateId", this.cosmeticCorrectionTemplateId );

   Parameters.set( "pixelInterpolation", this.pixelInterpolation );
   Parameters.set( "clampingThreshold", this.clampingThreshold );
   Parameters.set( "maxStars", this.maxStars );
   Parameters.set( "distortionCorrection", this.distortionCorrection );
   Parameters.set( "structureLayers", this.structureLayers );
   Parameters.set( "minStructureSize", this.minStructureSize );
   Parameters.set( "noiseReductionFilterRadius", this.noiseReductionFilterRadius );
   Parameters.set( "sensitivity", this.sensitivity );
   Parameters.set( "peakResponse", this.peakResponse );
   Parameters.set( "maxStarDistortion", this.maxStarDistortion );
   Parameters.set( "useTriangleSimilarity", this.useTriangleSimilarity );
   Parameters.set( "referenceImage", this.referenceImage );
   Parameters.set( "bestFrameReferenceKeyword", this.bestFrameReferenceKeyword );
   Parameters.set( "bestFrameRefernceMethod", this.bestFrameRefernceMethod );

   Parameters.set( "subframeWeightingPreset", this.subframeWeightingPreset );
   Parameters.set( "generateSubframesWeights", this.generateSubframesWeights );
   Parameters.set( "forceMeasurement", this.forceMeasurement );
   Parameters.set( "useSubframeSelectorProcess", this.useSubframeSelectorProcess );
   Parameters.set( "FWHMWeight", this.FWHMWeight );
   Parameters.set( "eccentricityWeight", this.eccentricityWeight );
   Parameters.set( "SNRWeight", this.SNRWeight );
   Parameters.set( "pedestal", this.pedestal );

   Parameters.set( "integrate", this.integrate );

   Parameters.set( "keywords", new ByteArray( JSON.stringify( this.keywords.list ) ).toBase64() );

   Parameters.set( "groups", new ByteArray( this.groupsToStringData() ).toBase64() );

   Parameters.set( "VERSION", VERSION );
};

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-engine.js - Released 2021-04-11T09:14:46Z
