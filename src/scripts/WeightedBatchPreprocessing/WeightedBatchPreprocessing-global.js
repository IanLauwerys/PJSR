// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-global.js - Released 2021-04-11T09:14:46Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.1.0
//
// Copyright (c) 2019-2021 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2021 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * Global declarations and variables
 */

// ----------------------------------------------------------------------------

/* beautify ignore:start */

#define VERSION            "2.1.0"
#define TITLE              "Weighted Batch Preprocessing Script"
#define SETTINGS_KEY_BASE  "WeightedBatchPreprocessing/"

#define WEIGHT_KEYWORD                    "WBPPWGHT"
#define CONST_MEASUREMENT_FWHM            "WBPPFWHM"
#define CONST_MEASUREMENT_ECCENTRICITY    "WBPPECC"
#define CONST_MEASUREMENT_NOISE           "WBPPNOIS"
#define CONST_MEASUREMENT_SNRWEIGHT       "WBPPSNRW"
#define CONST_MEASUREMENT_STARS           "WBPPSTAR"

var ImageType = {
   UNKNOWN: -1,
   BIAS: 0,
   DARK: 1,
   FLAT: 2,
   LIGHT: 3
};

var WBPPGroupingMode = {
   PRE: 1,  /* pre processing gropuing */
   POST: 2, /* post processing gropuing */
   LPS: 3   /* linear pattern subtraction gropuing */
}

var WBPPKeywordMode = {
   NONE: 0,
   PRE: 1,
   POST: 2,
   PREPOST: 3
}

var WBPPFrameProcessingStep = {
   CALIBRATION: 0,
   LPS: 1,
   CC: 2,
   DEBAYER: 3,
   REGISTRATION: 4,
   INTEGRATION: 5
}

var WBPPBestRefernenceMethod = {
   MANUAL: 0,
   AUTO_SINGLE: 1,
   AUTO_KEYWORD: 2
}

ImageIntegration.prototype.auto = 999;

/*
 * Default parameters
 */

#define DEFAULT_MASTER_DETECTION_USES_FULL_PATH       true
#define DEFAULT_SAVE_FRAME_GROUPS                     true
#define DEFAULT_OUTPUT_DIRECTORY                      ""
#define DEFAULT_UP_BOTTOM_FITS                        true
#define DEFAULT_SAVE_PROCESS_LOG                      true
#define DEFAULT_GENERATE_REJECTION_MAPS               true
#define DEFAULT_GROUPING_KEYWORDS_ACTIVE              true

#define DEFAULT_DARK_INCLUDE_BIAS                     true
#define DEFAULT_OPTIMIZE_DARKS                        false
#define DEFAULT_DARK_OPTIMIZATION_LOW                 3.0
#define DEFAULT_DARK_OPTIMIZATION_WINDOW              1024
#define DEFAULT_DARK_EXPOSURE_TOLERANCE               10

#define DEFAULT_EVALUATE_NOISE                        true

#define DEFAULT_REJECTION_METHOD                      ImageIntegration.prototype.auto

#define DEFAULT_LARGE_SCALE_REJECTION                 false
#define DEFAULT_LARGE_SCALE_LAYERS                    2
#define DEFAULT_LARGE_SCALE_GROWTH                    2

#define DEFAULT_LIGHT_EXPOSURE_TOLERANCE              2
#define DEFAULT_LIGHT_OUTPUT_PEDESTAL                 0
#define DEFAULT_GROUP_LIGHTS_WITH_DIFFERENT_EXPOSURE  false
#define DEFAULT_IMAGE_REGISTRATION                    false
#define DEFAULT_GENERATE_DRIZZLE_DATA                 true

#define DEFAULT_LINEAR_PATTERN_SUBTRACTION            false
#define DEFAULT_LINEAR_PATTERN_SUBTRACTION_SIGMA      3
#define DEFAULT_LINEAR_PATTERN_SUBTRACTION_MODE       0


#define DEFAULT_COSMETIC_CORRECTION                   false
#define DEFAULT_COSMETIC_CORRECTION_TEMPLATE          ""

#define DEFAULT_CFA_PATTERN                           Debayer.prototype.Auto
#define DEFAULT_DEBAYER_METHOD                        Debayer.prototype.VNG

#define DEFAULT_SUBFRAMEWEIGHTING_PRESET              1
#define DEFAULT_SUBFRAMEWEIGHTING_GENERATE            false
#define DEFAULT_SUBFRAMEWEIGHTING_FORCE_MEASUREMENT   false
#define DEFAULT_USE_SUBFRAMESELECTOR_PROCESS          true

#define DEFAULT_BEST_REFERENCE_METHOD                 WBPPBestRefernenceMethod.AUTO_SINGLE

#define DEFAULT_SUBFRAMEWEIGHTING_FWHM_WEIGHT         5
#define DEFAULT_SUBFRAMEWEIGHTING_ECCENTRICITY_WEIGHT 10
#define DEFAULT_SUBFRAMEWEIGHTING_SNR_WEIGHT          20
#define DEFAULT_SUBFRAMEWEIGHTING_PEDESTAL            65

#define DEFAULT_SA_PIXEL_INTERPOLATION                StarAlignment.prototype.Auto
#define DEFAULT_SA_CLAMPING_THRESHOLD                 0.3
#define DEFAULT_SA_MAX_STARS                          0
#define DEFAULT_SA_DISTORTION_CORRECTION              false
#define DEFAULT_SA_STRUCTURE_LAYERS                   5
#define DEFAULT_SA_MIN_STRUCTURE_SIZE                 0
#define DEFAULT_SA_NOISE_REDUCTION                    0
#define DEFAULT_SA_SENSITIVITY                        0.1
#define DEFAULT_SA_PEAK_RESPONSE                      0.8
#define DEFAULT_SA_MAX_STAR_DISTORTION                0.5

#define DEFAULT_SA_USE_TRIANGLE_SIMILARITY            false

#define DEFAULT_INTEGRATE                             false

#define DEFAULT_FRAME_GROUPS                          []

/*
 * Constants
 */

#define CONST_MIN_EXPOSURE_TOLERANCE   0.01
#define CONST_FLAT_DARK_TOLERANCE      0.5

/* beautify ignore:end */

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-global.js - Released 2021-04-11T09:14:46Z
