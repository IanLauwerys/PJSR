// ****************************************************************************
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ****************************************************************************
// MureMetadata.js - Released 2020/06/05 00:00:00 UTC
// ****************************************************************************
//
// This file is part of MureDenoise Script Version 1.32
//
// Copyright (C) 2012-2020 Mike Schuster. All Rights Reserved.
// Copyright (C) 2003-2020 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ****************************************************************************

function MureMetadata(model, view) {

   this.chop = function(value, delta = 1.0e-10) {
      return Math.abs(value) < delta ? 0 : value;
   };

   this.imageCombinationCount = function() {
      var calibrationProcess = null;
      var integrationProcess = null;
      var imageCombinationCount = 0;
      var keywords = model.imageView.window.keywords;
      for (var i = 0; i != keywords.length; ++i) {
         if (keywords[i].name != "HISTORY") {
            continue;
         }

         var found = keywords[i].comment.match(
            /Calibration with ImageCalibration process/
         );
         if (found != null && found.length == 1) {
            calibrationProcess = true;
         }
         var found = keywords[i].comment.match(
            /Integration with ImageIntegration process/
         );
         if (found != null && found.length == 1) {
            integrationProcess = true;
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.numberOfImages: (\d+)/
         );
         if (found != null && found.length == 2) {
            if (!isNaN(parseInt(found[1]))) {
               imageCombinationCount = parseInt(found[1]);
            }
         }
      }

      if (
         (calibrationProcess != null && calibrationProcess) &&
         !(integrationProcess != null && integrationProcess)
      ) {
         return {error: false, imageCombinationCount: 1};
      }

      if (imageCombinationCount < model.imageCombinationCountMinimum) {
         return {error: true};
      }
      if (imageCombinationCount > model.imageCombinationCountMaximum) {
         return {error: true};
      }

      return {error: false, imageCombinationCount: imageCombinationCount};
   };

   this.combinationTransform = function() {
      if (!model.useImageMetadata) {
         return {offset: 0, scale: 1};
      }

      var pixelCombination = null;
      var outputNormalization = null;
      var weightMode = null;
      var scaleEstimates = new Array(model.imageCombinationCount);
      var locationEstimates = new Array(model.imageCombinationCount);
      var imageWeights = new Array(model.imageCombinationCount);
      var keywords = model.imageView.window.keywords;
      for (var i = 0; i != keywords.length; ++i) {
         if (keywords[i].name != "HISTORY") {
            continue;
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.pixelCombination: *([a-zA-Z]+)/
         );
         if (found != null && found.length == 2) {
            pixelCombination = found[1];
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.outputNormalization: *([a-zA-Z]+(?: [a-zA-Z+]+)*)/
         );
         if (found != null && found.length == 2) {
            outputNormalization = found[1];
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.weightMode: *([a-zA-Z']+(?: [a-zA-Z]+)*)/
         );
         if (found != null && found.length == 2) {
            weightMode = found[1];
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.scaleEstimates_(\d+): *([\-+]?\d\.\d+e[\-+]\d+)/
         );
         if (found != null && found.length == 3) {
            var index = parseInt(found[1]);
            var scaleEstimate = parseFloat(found[2]);
            if (
               !isNaN(index) && !isNaN(scaleEstimate) &&
               0 <= index && index < model.imageCombinationCount &&
               0 < scaleEstimate
            ) {
               scaleEstimates[index] = scaleEstimate;
            }
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.locationEstimates_(\d+): *([\-+]?\d\.\d+e[\-+]\d+)/
         );
         if (found != null && found.length == 3) {
            var index = parseInt(found[1]);
            var locationEstimate = parseFloat(found[2]);
            if (
               !isNaN(index) && !isNaN(locationEstimate) &&
               0 <= index && index < model.imageCombinationCount &&
               0 < locationEstimate
            ) {
               locationEstimates[index] = locationEstimate;
            }
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.imageWeights_(\d+): *([\-+]?\d\.\d+e[\-+]\d+)/
         );
         if (found != null && found.length == 3) {
            var index = parseInt(found[1]);
            var imageWeight = parseFloat(found[2]);
            if (
               !isNaN(index) && !isNaN(imageWeight) &&
               0 <= index && index < model.imageCombinationCount &&
               0 < imageWeight
            ) {
               imageWeights[index] = imageWeight;
            }
         }
      }

      if (pixelCombination == null) {
         return {offset: 0, scale: 1};
      }
      if (pixelCombination.toLowerCase() != "average") {
         return {offset: 0, scale: 1};
      }

      if (outputNormalization == null) {
         return {offset: 0, scale: 1};
      }
      if (outputNormalization.toLowerCase() == "none") {
         for (var i = 0; i != model.imageCombinationCount; ++i) {
            locationEstimates[i] = 0;
            scaleEstimates[i] = 1;
         }
      } else if (outputNormalization.toLowerCase() == "additive + scaling") {
      } else {
         return {offset: 0, scale: 1};
      }

      if (weightMode == null) {
         return {offset: 0, scale: 1};
      }
      if (weightMode.toLowerCase() == "don't care") {
         for (var i = 0; i != model.imageCombinationCount; ++i) {
            imageWeights[i] = 1;
         }
      }

      for (var i = 0; i != model.imageCombinationCount; ++i) {
         if (scaleEstimates[i] == null) {
            return {offset: 0, scale: 1};
         }
         if (locationEstimates[i] == null) {
            return {offset: 0, scale: 1};
         }
         if (imageWeights[i] == null) {
            return {offset: 0, scale: 1};
         }
      }

      if (model.useCombinationVariance) {
         var weights = 0;
         var weightScales2 = 0;
         for (var i = 0; i != model.imageCombinationCount; ++i) {
            weights += imageWeights[i];
            var scale = scaleEstimates[0] / scaleEstimates[i];
            var weightScale = imageWeights[i] * scale;
            weightScales2 += weightScale * weightScale;
         }
         var offset = 0;
         var scale = model.imageCombinationCount *
            weightScales2 / (weights * weights);
      } else {
         var weights = 0;
         var weightLocations = 0;
         var weightScales = 0;
         for (var i = 0; i != model.imageCombinationCount; ++i) {
            weights += imageWeights[i];
            var scale = scaleEstimates[0] / scaleEstimates[i];
            weightLocations += imageWeights[i] *
               (-locationEstimates[i] * scale + locationEstimates[0]);
            weightScales += imageWeights[i] * scale;
         }
         var offset = weightLocations / weights;
         var scale = weightScales / weights;
      }

      return {offset: offset, scale: scale};
   };

   this.rangeTransform = function() {
      var outputRangeLow = 0;
      var outputRangeHigh = 1;
      var outputRangeOperation = "none";
      var keywords = model.imageView.window.keywords;
      for (var i = 0; i != keywords.length; ++i) {
         if (keywords[i].name != "HISTORY") {
            continue;
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.outputRangeLow: *([\-+]?\d\.\d+e[\-+]\d+)/
         );
         if (found != null && found.length == 2) {
            var value = parseFloat(found[1]);
            if (!isNaN(value)) {
               outputRangeLow = value;
            }
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.outputRangeHigh: *([\-+]?\d\.\d+e[\-+]\d+)/
         );
         if (found != null && found.length == 2) {
            var value = parseFloat(found[1]);
            if (!isNaN(value)) {
               outputRangeHigh = value;
            }
         }

         var found = keywords[i].comment.match(
            /ImageIntegration\.outputRangeOperation: *([a-zA-Z]+)/
         );
         if (found != null && found.length == 2) {
            outputRangeOperation = found[1];
         }
      }

      if (
         (outputRangeLow < 0 || outputRangeHigh > 1) &&
         outputRangeLow < outputRangeHigh
      ) {
         if (outputRangeOperation.toLowerCase() == "normalize") {
            return {
               offset: 0,
               scale: 1 / outputRangeHigh
            };
         } else if (outputRangeOperation.toLowerCase() == "rescale") {
            return {
               offset: -outputRangeLow / (outputRangeHigh - outputRangeLow),
               scale: 1 / (outputRangeHigh - outputRangeLow)
            };
         } else {
            return {offset: 0, scale: 1};
         }
      } else {
         return {offset: 0, scale: 1};
      }
   };
};

// ****************************************************************************
// EOF MureMetadata.js - Released 2020/06/05 00:00:00 UTC
